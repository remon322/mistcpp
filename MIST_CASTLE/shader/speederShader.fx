
//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float4x4 g_mWorld;               // オブジェクトのWorld行列
float4x4 g_mRotation;            // オブジェクトのRotation行列
float4x4 g_matView;              // 3D空間のView行列
float4x4 g_matProj;              // 3D空間のprojection行列
texture  g_MeshTexture0;         // メッシュのテクスチャ
texture  g_MeshNormalMap0;       // 法線マッピングテクスチャ

float4   g_materialColor;        // 物体の色補正
float4   g_ambientLight;         // アンビエント光
float3   g_cameraPos;            // カメラの座標
float4   g_lightSource[5];      // 光源の座標
float4   g_lightColor[5];       // 光の色
float    g_lightRange[5];       // 光の届く距離
float    g_fogStartRange;        // フォグ開始地点までの距離
float    g_fogEndRange;          // フォグ終了地点までの距離

//--------------------------------------------------------------------------------------
// Texture samplers
//--------------------------------------------------------------------------------------

sampler MeshTextureSampler0 =
sampler_state
{
  Texture = <g_MeshTexture0>;
  MipFilter = LINEAR;
  MinFilter = LINEAR;
  MagFilter = LINEAR;
};

sampler NormalMapSampler0 =
sampler_state
{
  Texture = <g_MeshNormalMap0>;
  MipFilter = LINEAR;
  MinFilter = LINEAR;
  MagFilter = LINEAR;
};

//--------------------------------------------------------------------------------------
// Vertex shader output structure
//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Position  : POSITION;  // 頂点座標
  float4 TextureUV : TEXCOORD0; // float2 UV座標  +  float2 座標zw
  float4 Diffuse   : COLOR0;    // 頂点カラー
  float3 Light[5] : NORMAL;    // ライトベクトルの接空間
};

// 接空間行列の逆行列を算出
float4x4 InvTangentMatrix(float3 tangent, float3 binormal, float3 normal)
{
  float4x4 mat = {float4(tangent, 0.0f), float4(binormal, 0.0f), float4(normal, 0.0f),{0.0f, 0.0f, 0.0f, 1.0f}};
  return transpose(mat); // 転置
}

//--------------------------------------------------------------------------------------
// This shader computes standard transform and lighting
//--------------------------------------------------------------------------------------
VS_OUTPUT RenderSceneVS(float4 vPos      : POSITION,
                        float4 vWeight   : BLENDWEIGHT,
                        float2 vTexCoord : TEXCOORD0,
                        float3 vNormal   : NORMAL,
                        float3 vTangent  : TANGENT, 
                        float4 vBinormal : BINORMAL)
{
  VS_OUTPUT Output;
  float4 v4Diffuse;
  float4 worldPosition;
  float  radius;
  float4x4 mWorldViewProjection;
  int index;

  index = vBinormal.w;

  //各行列からワールドビュー行列の計算
  mWorldViewProjection = mul(g_mWorld, mul(g_matView, g_matProj));
  
  //vNormal = mul(vNormal, g_mWorld);
  vTangent = mul(vTangent, g_mWorld);
  vBinormal = mul(vBinormal, g_mWorld);

  //接空間行列の逆行列を算出
  float4x4 invTangentMat = InvTangentMatrix(vTangent, vBinormal, vNormal);

  //頂点座標のワールド変換
  worldPosition = mul(vPos, g_mWorld);

  for (int i = 0; i < 5; i++)
  {
    //頂点から光源への単位ベクトルを計算
    Output.Light[i] = normalize(g_lightSource[i] - worldPosition);

    Output.Light[i] = mul(Output.Light[i], transpose(g_mRotation));

    //ライトベクトルを接空間に移動
    Output.Light[i] = mul(Output.Light[i], invTangentMat);

    //光の届く距離から光の影響力を計算
    Output.Light[i] *= max(0.0f, (1.0f - length(worldPosition - g_lightSource[i]) / g_lightRange[i]));
  }


  //色の計算
  v4Diffuse.r = g_ambientLight.r;
  v4Diffuse.g = g_ambientLight.g;
  v4Diffuse.b = g_ambientLight.b;
  v4Diffuse.a = 1.0f;

  //出力
  Output.Position = mul(vPos, mWorldViewProjection);
  Output.Diffuse = v4Diffuse;
  Output.TextureUV.xy = vTexCoord.xy;
  Output.TextureUV.zw = Output.Position.zw;
  return Output;
}


//--------------------------------------------------------------------------------------
// Pixel shader output structure
//--------------------------------------------------------------------------------------
struct PS_OUTPUT
{
  float4 RGBColor : COLOR0;  // Pixel color    
};

float4 easingOutQuint(float t, float4 b, float4 c)
{
  t--;
  c = c - b;
  return c * (t * t * t * t * t + 1) + b;
}

//--------------------------------------------------------------------------------------
// テクスチャ付きPS
//--------------------------------------------------------------------------------------
PS_OUTPUT RenderWithTexturePS(VS_OUTPUT In)
{
  PS_OUTPUT Output;
  float4 v4Diffuse;
  float3 normalMap;
  float dotProduct;

  //テクスチャから情報を読み取る
  Output.RGBColor = tex2D(MeshTextureSampler0, In.TextureUV.xy);
  normalMap = tex2D(NormalMapSampler0, In.TextureUV.xy);

  //法線マップからUV座標上の情報を読み取る
  float3 normalVec = normalMap * 2.0f - 1.0f; // ベクトルへ変換
  normalVec = normalize(normalVec);

  float4 lightColor = 0;
  for (int i = 0; i < 5; i++)
  {
    //内積から反射光を求める
    dotProduct = dot(In.Light[i], normalVec);
    dotProduct = max(0.0f, dotProduct);
    //それぞれの光の影響力を合計する
    lightColor += g_lightColor[i] * dotProduct;
  }

  //反射を色に適応
  v4Diffuse.r = In.Diffuse.r + normalVec * 0.1f + lightColor.r * 1.0f;
  v4Diffuse.g = In.Diffuse.g + normalVec * 0.1f + lightColor.g * 1.0f;
  v4Diffuse.b = In.Diffuse.b + normalVec * 0.1f + lightColor.b * 1.0f;
  v4Diffuse.a = 1.0f;

  // Lookup mesh texture
  Output.RGBColor = Output.RGBColor * v4Diffuse;
  
  float4 fogColor;
  fogColor.r = 0.8f;
  fogColor.g = 0.8196078431372549f;
  fogColor.b = 0.8549019607843137f;
  fogColor.a = 1.0f;
  
  //fogの濃さ
  float fogLevel = (In.TextureUV.z - g_fogStartRange) / (g_fogEndRange - g_fogStartRange);
  fogLevel = clamp(fogLevel, 0, 1);
  Output.RGBColor = lerp(Output.RGBColor, fogColor, fogLevel);
  return Output;
}

//--------------------------------------------------------------------------------------
// テクスチャ無しPS
//--------------------------------------------------------------------------------------
PS_OUTPUT RenderWithMaterialPS(VS_OUTPUT In)
{
  PS_OUTPUT Output;

  //素材色を設定
  Output.RGBColor.a = g_materialColor.a;
  Output.RGBColor.r = g_materialColor.r;
  Output.RGBColor.g = g_materialColor.g;
  Output.RGBColor.b = g_materialColor.b;
  
  //霧の色設定
  float4 fogColor;
  fogColor.r = 0.8f;
  fogColor.g = 0.8196078431372549f;
  fogColor.b = 0.8549019607843137f;
  fogColor.a = 1.0f;

  //fogの濃さ
  float fogLevel = (In.TextureUV.z - g_fogStartRange) / (g_fogEndRange - g_fogStartRange);
  fogLevel = clamp(fogLevel, 0, 1);
  Output.RGBColor = lerp(Output.RGBColor, fogColor, fogLevel);
  return Output;
}

//--------------------------------------------------------------------------------------
// テクスチャ有り陰無しPS
//--------------------------------------------------------------------------------------
PS_OUTPUT RenderWithTextureNoShadePS(VS_OUTPUT In)
{
  PS_OUTPUT Output;
  float4 v4Diffuse;

  //テクスチャから情報を読み取る
  Output.RGBColor = tex2D(MeshTextureSampler0, In.TextureUV.xy);

  //反射を色に適応
  v4Diffuse.r = In.Diffuse;
  v4Diffuse.g = In.Diffuse;
  v4Diffuse.b = In.Diffuse;
  v4Diffuse.a = 1.0f;

  // Lookup mesh texture
  Output.RGBColor = Output.RGBColor * v4Diffuse;
  
  float4 fogColor;
  fogColor.r = 0.8f;
  fogColor.g = 0.8196078431372549f;
  fogColor.b = 0.8549019607843137f;
  fogColor.a = 1.0f;

  //fogの濃さ
  float fogLevel = (In.TextureUV.z - g_fogStartRange) / (g_fogEndRange - g_fogStartRange);
  Output.RGBColor = lerp(Output.RGBColor, fogColor, fogLevel);
  return Output;
}


//--------------------------------------------------------------------------------------
// Renders scene to render target
//--------------------------------------------------------------------------------------
//テクスチャあり　陰あり
technique RenderSceneWithTexture
{
  pass P0
  {
    VertexShader = compile vs_3_0 RenderSceneVS();
    PixelShader = compile ps_3_0 RenderWithTexturePS();
  }
}

//テクスチャあり　陰なし
technique RenderSceneWithTextureNoShade
{
  pass P0
  {
    VertexShader = compile vs_3_0 RenderSceneVS();
    PixelShader = compile ps_3_0 RenderWithTextureNoShadePS();
  }
}

//テクスチャなし　陰あり
technique RenderSceneWithMaterial
{
  pass P0
  {
    VertexShader = compile vs_3_0 RenderSceneVS();
    PixelShader = compile ps_3_0 RenderWithMaterialPS();
  }
}