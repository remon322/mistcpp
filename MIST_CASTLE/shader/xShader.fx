
//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float4x4 g_mWorld;                  // オブジェクトのWorld行列
float4x4 g_mRotation;               // オブジェクトのRotation行列
float4x4 g_matView;                 // 3D空間のView行列
float4x4 g_matProj;                 // 3D空間のprojection行列
texture  g_MeshTexture;             // メッシュのテクスチャ
texture  g_MeshNormalMap;           // 法線マッピングテクスチャ

float4   g_materialColor;           // 物体の色補正
float4   g_ambientLight;            // アンビエント光
float3   g_lightSource;             // 光源の座標
float3   g_cameraPos;               // カメラの座標
float    g_fogStartRange;           // フォグ開始地点までの距離
float    g_fogEndRange;             // フォグ終了地点までの距離

//--------------------------------------------------------------------------------------
// Texture samplers
//--------------------------------------------------------------------------------------
sampler MeshTextureSampler =
sampler_state
{
  Texture = <g_MeshTexture>;
  MipFilter = LINEAR;
  MinFilter = LINEAR;
  MagFilter = LINEAR;
};

sampler NormalMapSampler =
sampler_state
{
  Texture = <g_MeshNormalMap>;
  MipFilter = LINEAR;
  MinFilter = LINEAR;
  MagFilter = LINEAR;
};


//--------------------------------------------------------------------------------------
// Vertex shader output structure
//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Position  : POSITION;  // 頂点座標
  float2 TextureUV : TEXCOORD0; // メッシュ,法線マップ用UV座標
  float4 Diffuse   : COLOR0;    // 頂点カラー
  float3 Light     : NORMAL;    // ライトベクトルの接空間
  float  Fog       : FOG;       // フォグ
};

// 接空間行列の逆行列を算出
float4x4 InvTangentMatrix(float3 tangent, float3 binormal, float3 normal)
{
  float4x4 mat = {float4(tangent, 0.0f), float4(binormal, 0.0f), float4(normal, 0.0f),{0.0f, 0.0f, 0.0f, 1.0f}};
  return transpose(mat); // 転置
}

//--------------------------------------------------------------------------------------
// This shader computes standard transform and lighting
//--------------------------------------------------------------------------------------
VS_OUTPUT RenderSceneVS(float4 vPos      : POSITION,
                        float4 vWeight   : BLENDWEIGHT,
                        float2 vTexCoord : TEXCOORD0,
                        float3 vNormal   : NORMAL,
                        float3 vTangent  : TANGENT, 
                        float3 vBinormal : BINORMAL)
{
  VS_OUTPUT Output;
  float4 v4Diffuse;
  float4 worldPosition;
  float3 vLight;
  float  radius;
  float4x4 mWorldViewProjection;

  //各行列からワールドビュー行列の計算
  mWorldViewProjection = mul(g_mWorld, mul(g_matView, g_matProj));
  
  //vNormal = mul(vNormal, g_mWorld);
  vTangent = mul(vTangent, g_mWorld);
  vBinormal = mul(vBinormal, g_mWorld);

  //接空間行列の逆行列を算出
  float4x4 invTangentMat = InvTangentMatrix(vTangent, vBinormal, vNormal);

  //頂点座標のワールド変換
  worldPosition = mul(vPos, g_mWorld);

  //頂点から光源への単位ベクトルを計算
  vLight = normalize(g_lightSource - worldPosition);

  vLight = mul(vLight, transpose(g_mRotation));
  
  //ライトベクトルを接空間に移動
  vLight = mul(vLight, invTangentMat);

  //色の計算
  v4Diffuse.r = g_ambientLight.r;// + max(0.0f, dotProduct) * 0.5f;
  v4Diffuse.g = g_ambientLight.g;// + max(0.0f, dotProduct) * 0.5f;
  v4Diffuse.b = g_ambientLight.b;// + max(0.0f, dotProduct) * 0.5f;
  v4Diffuse.a = 1.0f;

  //出力
  Output.Position  = mul(vPos, mWorldViewProjection);
  Output.Light     = vLight;
  Output.Diffuse   = v4Diffuse;
  Output.TextureUV = vTexCoord;
  //Output.Index     = index;

  radius = sqrt((worldPosition.x - g_cameraPos.x) * (worldPosition.x - g_cameraPos.x)
    + (worldPosition.y - g_cameraPos.y) * (worldPosition.y - g_cameraPos.y)
    + (worldPosition.z - g_cameraPos.z) * (worldPosition.z - g_cameraPos.z));
  radius = length(worldPosition - g_cameraPos);
  
  Output.Fog = clamp((radius - g_fogStartRange) / (g_fogEndRange - g_fogStartRange), 0.0f, 0.8f);
  return Output;
}


//--------------------------------------------------------------------------------------
// Pixel shader output structure
//--------------------------------------------------------------------------------------
struct PS_OUTPUT
{
  float4 RGBColor : COLOR0;  // Pixel color    
};

float4 easingOutQuint(float t, float4 b, float4 c)
{
  t--;
  c = c - b;
  return c * (t * t * t * t * t + 1) + b;
}

//--------------------------------------------------------------------------------------
// テクスチャ付きPS
//--------------------------------------------------------------------------------------
PS_OUTPUT RenderWithTexturePS(VS_OUTPUT In)
{
  PS_OUTPUT Output;
  float4 v4Diffuse;
  float3 normalMap;
  float dotProduct;

  //法線マップからUV座標上の情報を読み取る
  normalMap = tex2D(NormalMapSampler, In.TextureUV);
  float3 normalVec = normalMap * 2.0f - 1.0f; // ベクトルへ変換
  normalVec = normalize(normalVec);

  //内積から反射光を求める
  dotProduct = dot(In.Light, normalVec);

  //反射を色に適応
  v4Diffuse.r = In.Diffuse + normalVec * 0.3 + max(0.0f, dotProduct) * 0.6f;
  v4Diffuse.g = In.Diffuse + normalVec * 0.3 + max(0.0f, dotProduct) * 0.6f;
  v4Diffuse.b = In.Diffuse + normalVec * 0.3 + max(0.0f, dotProduct) * 0.6f;
  v4Diffuse.a = 1.0f;

  // Lookup mesh texture
  Output.RGBColor = tex2D(MeshTextureSampler, In.TextureUV) * v4Diffuse;
  //Output.RGBColor = v4Diffuse;
  
  float4 fogColor;
  fogColor.r = 0.8f;
  fogColor.g = 0.8196078431372549f;
  fogColor.b = 0.8549019607843137f;
  fogColor.a = 1.0f;

  Output.RGBColor = lerp(Output.RGBColor, fogColor, In.Fog);

  //Output.RGBColor = easingOutQuint(In.Fog, Output.RGBColor, fogColor);
  return Output;
}

//--------------------------------------------------------------------------------------
// テクスチャ無しPS
//--------------------------------------------------------------------------------------
PS_OUTPUT RenderWithMaterialPS(VS_OUTPUT In)
{
  PS_OUTPUT Output;

  //素材色を設定
  Output.RGBColor.a = g_materialColor.a;
  Output.RGBColor.r = g_materialColor.r;
  Output.RGBColor.g = g_materialColor.g;
  Output.RGBColor.b = g_materialColor.b;
  
  //霧の色設定
  float4 fogColor;
  fogColor.r = 0.8f;
  fogColor.g = 0.8196078431372549f;
  fogColor.b = 0.8549019607843137f;
  fogColor.a = 1.0f;

  Output.RGBColor = lerp(Output.RGBColor, fogColor, In.Fog);
  //Output.RGBColor = easingOutQuint(In.Fog, Output.RGBColor, fogColor);
  return Output;
}

//--------------------------------------------------------------------------------------
// テクスチャ有り陰無しPS
//--------------------------------------------------------------------------------------
PS_OUTPUT RenderWithTextureNoShadePS(VS_OUTPUT In)
{
  PS_OUTPUT Output;
  float4 v4Diffuse;
  float3 normalMap;
  float dotProduct;

  //法線マップからUV座標上の情報を読み取る
  normalMap = tex2D(NormalMapSampler, In.TextureUV);
  float3 normalVec = normalMap * 2.0f - 1.0f; // ベクトルへ変換
  normalVec = normalize(normalVec);

  //内積から反射光を求める
  dotProduct = dot(In.Light, normalVec);

  //反射を色に適応
  v4Diffuse.r = In.Diffuse;
  v4Diffuse.g = In.Diffuse;
  v4Diffuse.b = In.Diffuse;
  v4Diffuse.a = 1.0f;

  // Lookup mesh texture
  Output.RGBColor = tex2D(MeshTextureSampler, In.TextureUV) * v4Diffuse;
  //Output.RGBColor = v4Diffuse;
  
  float4 fogColor;
  fogColor.r = 0.8f;
  fogColor.g = 0.8196078431372549f;
  fogColor.b = 0.8549019607843137f;
  fogColor.a = 1.0f;

  Output.RGBColor = lerp(Output.RGBColor, fogColor, In.Fog);

  //Output.RGBColor = easingOutQuint(In.Fog, Output.RGBColor, fogColor);
  return Output;
}


//--------------------------------------------------------------------------------------
// Renders scene to render target
//--------------------------------------------------------------------------------------
//テクスチャあり　陰あり
technique RenderSceneWithTexture
{
  pass P0
  {
    VertexShader = compile vs_3_0 RenderSceneVS();
    PixelShader = compile ps_3_0 RenderWithTexturePS();
  }
}

//テクスチャあり　陰なし
technique RenderSceneWithTextureNoShade
{
  pass P0
  {
    VertexShader = compile vs_3_0 RenderSceneVS();
    PixelShader = compile ps_3_0 RenderWithTextureNoShadePS();
  }
}

//テクスチャなし　陰あり
technique RenderSceneWithMaterial
{
  pass P0
  {
    VertexShader = compile vs_3_0 RenderSceneVS();
    PixelShader = compile ps_3_0 RenderWithMaterialPS();
  }
}