
//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float4x4 g_mWorld;                  // オブジェクトのWorld行列
float4x4 g_matView;                 // 3D空間のView行列
float4x4 g_matProj;                 // 3D空間のprojection行列
texture  g_MeshTexture;             // テクスチャ

float4   g_materialColor;           // 物体の色補正
float4   g_ambientLight;            // アンビエント光

//--------------------------------------------------------------------------------------
// Texture samplers
//--------------------------------------------------------------------------------------
sampler MeshTextureSampler =
sampler_state
{
  Texture = <g_MeshTexture>;
  MipFilter = LINEAR;
  MinFilter = LINEAR;
  MagFilter = LINEAR;
};

//--------------------------------------------------------------------------------------
// Vertex shader output structure
//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Position  : POSITION;  // 頂点座標
  float2 TextureUV : TEXCOORD0; // メッシュ,法線マップ用UV座標
  float4 Diffuse   : COLOR0;    // 頂点カラー
};

//--------------------------------------------------------------------------------------
// This shader computes standard transform and lighting
//--------------------------------------------------------------------------------------
VS_OUTPUT RenderSceneVS(float4 vPos      : POSITION,
                        float2 vTexCoord : TEXCOORD0)
{
  VS_OUTPUT Output;
  float4 v4Diffuse;
  float4 worldPosition;
  float3 vLight;
  float  radius;
  float4x4 mWorldViewProjection;

  //各行列からワールドビュー行列の計算
  mWorldViewProjection = mul(g_mWorld, mul(g_matView, g_matProj));

  //色の計算
  v4Diffuse.r = g_ambientLight.r;
  v4Diffuse.g = g_ambientLight.g;
  v4Diffuse.b = g_ambientLight.b;
  v4Diffuse.a = 1.0f;

  //出力
  Output.Position  = mul(vPos, mWorldViewProjection);
  Output.Diffuse   = v4Diffuse;
  Output.TextureUV = vTexCoord;

  return Output;
}


//--------------------------------------------------------------------------------------
// Pixel shader output structure
//--------------------------------------------------------------------------------------
struct PS_OUTPUT
{
  float4 RGBColor : COLOR0;  // Pixel color    
};

//--------------------------------------------------------------------------------------
// テクスチャ付きPS
//--------------------------------------------------------------------------------------
PS_OUTPUT RenderWithTexturePS(VS_OUTPUT In)
{
  PS_OUTPUT Output;
  float4 v4Diffuse;
  float3 normalMap;
  float dotProduct;

  //反射を色に適応
  v4Diffuse.r = 1.0f;
  v4Diffuse.g = 1.0f;
  v4Diffuse.b = 1.0f;
  v4Diffuse.a = 1.0f;

  // Lookup mesh texture
  Output.RGBColor = tex2D(MeshTextureSampler, In.TextureUV) * v4Diffuse;
  return Output;
}

//--------------------------------------------------------------------------------------
// Renders scene to render target
//--------------------------------------------------------------------------------------
technique RenderSceneWithTexture
{
  pass P0
  {
    VertexShader = compile vs_3_0 RenderSceneVS();
    PixelShader = compile ps_3_0 RenderWithTexturePS();
  }
}