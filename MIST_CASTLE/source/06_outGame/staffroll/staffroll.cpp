#include "../../00_application/lib/myDx9Lib.h"
#include "../../00_application/app/app.h"
#include "../../01_data/texture/loadTexture.h"
#include "../../01_data/sound/loadSound.h"
#include "../../01_data/sound/dslib.h"
#include "../../02_input/input.h"

#define STAFFROLL_ADVANCE_Y (1.218f)//スタッフロールのスクロール速度
#define STAFFROLL_HEIGHT    (6500.0f)  //スタッフロール画像の高さ
#define FADE_HEIGHT         (150.0f)

static float g_staffrollY; //スタッフロール画像のY座標

/// <summary>
/// スタッフロール画面の初期化
/// </summary>
void InitStaffroll()
{
  g_staffrollY = 100;
  DSoundPlay(g_soundHandles[STAFFROLL_BGM], FALSE);
}

/// <summary>
/// スタッフロール画面の更新
/// </summary>
void UpdateStaffroll()
{
  //タイトルに進む
  if(g_staffrollY < -STAFFROLL_HEIGHT - FADE_HEIGHT || GetTrgInKeyBoard().esc)
  {
    ChangeGameState(GAME_STATE_TITLE);
  }

  //スタッフロール画像の描画位置をスクロールさせる
  g_staffrollY -= STAFFROLL_ADVANCE_Y * GetSpeedRate();
}

/// <summary>
/// スタッフロール画面の描画
/// </summary>
void RenderStaffroll()
{
  //スタッフロールの描画
  RenderTexture(0, CLUMP(-STAFFROLL_HEIGHT + 720.0f, g_staffrollY, 0.0f), g_textureHandles[STAFFROLL_BACK], 255);

  Render2DBox(0, 0, 1280, 720, D3DXCOLOR(0.0f, 0.0f, 0.0f, max(0.0f, (-g_staffrollY - STAFFROLL_HEIGHT + 720.0f) / FADE_HEIGHT)));
  DrawPrintf(0, 0, 0xffffffff, "%f", (-g_staffrollY - STAFFROLL_HEIGHT + 720.0f) / FADE_HEIGHT);
}