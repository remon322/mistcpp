#pragma once

//スタッフロール画面の初期化
void InitStaffroll();

//スタッフロール画面の更新
void UpdateStaffroll();

//スタッフロール画面の描画
void RenderStaffroll();