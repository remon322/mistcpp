#include "../../00_application/lib/myDx9Lib.h"
#include "../../00_application/app/app.h"
#include "../../01_data/sound/dslib.h"
#include "../../01_data/texture/loadTexture.h"
#include "../../01_data/sound/loadSound.h"
#include "../../02_input/input.h"

//カーソル位置識別子
enum GAMEOVER_CURSOR
{
  CURSOR_GAME = 0,
  CURSOR_TITLE = 1,
};

int g_gameoverTime = 0;       //ゲームオーバーの経過時間
int g_gameoverCursor = 0;     //ゲームオーバーのカーソル
int gameoverUnderLineNum = 0; //アンダーラインのスプライトの現在枚数

/// <summary>
/// ゲームオーバー画面の初期化
/// </summary>
void InitGameover()
{
  g_gameoverTime = 0;
  g_gameoverCursor = 0;
  DSoundPlay(g_soundHandles[GAMEOVER_BGM], FALSE);
}

/// <summary>
/// ゲームオーバー画面の更新
/// </summary>
void UpdateGameover()
{
  KEY_BOARD keyTrg = GetTrgInKeyBoard();
  JOY_STICK joyTrg = GetTrgInJoyStickButton();
  MOUSE   mouseTrg = GetTrgInMouseButton();

  //カーソルの移動
  if(keyTrg.left || joyTrg.left)
  {
    g_gameoverTime = 0;
    gameoverUnderLineNum = 0;
    g_gameoverCursor = max(0, g_gameoverCursor - 1);
    //カーソル移動音を鳴らす
    DSoundStop(g_soundHandles[MENU_CHOOSE_SE]);
    DSoundPlay(g_soundHandles[MENU_CHOOSE_SE], false);
  }
  if(keyTrg.right || joyTrg.right)
  {
    g_gameoverTime = 0;
    gameoverUnderLineNum = 0;
    g_gameoverCursor = min(g_gameoverCursor + 1, CURSOR_TITLE);
    //カーソル移動音を鳴らす
    DSoundStop(g_soundHandles[MENU_CHOOSE_SE]);
    DSoundPlay(g_soundHandles[MENU_CHOOSE_SE], false);
  }

  //決定ボタンが押された
  if(keyTrg.rEnter || keyTrg.space || joyTrg.button[1])
  {
    //決定音を鳴らす
    DSoundPlay(MENU_DECIDE_SE, false);
    //インゲームに進む
    if(g_gameoverCursor == CURSOR_GAME)
    {
      ChangeGameState(GAME_STATE_INGAME);
    }
    //タイトルに戻る
    else if(g_gameoverCursor == CURSOR_TITLE)
    {
      ChangeGameState(GAME_STATE_TITLE);
    }
  }

  // 1/4秒で7枚分の連番を回す
  if(g_gameoverTime % (60 / (4 * 7)) == 0)
    gameoverUnderLineNum = min(gameoverUnderLineNum + 1, 6);

  g_gameoverTime++;
}

/// <summary>
/// ゲームオーバー画面の描画
/// </summary>
void RenderGameover()
{
  //黒背景を描画
  Render2DBox(0, 0, 1280, 720, D3DXCOLOR(0, 0, 0, 1));
  //タイトル背景の描画
  RenderTexture(0, 0, g_textureHandles[GAMEOVER_BACK], 255);
  //ゲームオーバーロゴの描画
  RenderTexture(0, 0, g_textureHandles[GAMEOVER], 255);

  //選択肢の描画
  RenderBlockTexture(440, 540, g_textureHandles[GAMEOVER_GAME], 255, g_gameoverCursor);
  RenderBlockTexture(840, 540, g_textureHandles[GAMEOVER_TITLE], 255, CURSOR_TITLE - g_gameoverCursor);

  //アンダーラインの描画
  if(g_gameoverCursor == CURSOR_GAME)
    RenderBlockTexture(440, 540 + 130, g_textureHandles[UNDER_SIGN], 255, gameoverUnderLineNum);
  else if(g_gameoverCursor == CURSOR_TITLE)
    RenderBlockTexture(840, 540 + 130, g_textureHandles[UNDER_SIGN], 255, gameoverUnderLineNum);
}