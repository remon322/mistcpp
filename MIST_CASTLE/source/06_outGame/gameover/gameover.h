#pragma once

//ゲームオーバー画面の初期化
void InitGameover();

//ゲームオーバー画面の更新
void UpdateGameover();

//ゲームオーバー画面の描画
void RenderGameover();