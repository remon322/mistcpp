#include "../../01_data/sound/dslib.h"
#include "../../00_application/app/app.h"
#include "../../01_data/texture/loadTexture.h"
#include "../../01_data/sound/loadSound.h"
#include "../../02_input/input.h"

int g_clearTime = 0;

/// <summary>
/// クリア画面の初期化
/// </summary>
void InitClear()
{
  g_clearTime = 0;
  DSoundPlay(g_soundHandles[GAMEOVER_BGM], FALSE);
}

/// <summary>
/// クリア画面の更新
/// </summary>
void UpdateClear()
{
  KEY_BOARD keyTrg = GetTrgInKeyBoard();
  JOY_STICK joyTrg = GetTrgInJoyStickButton();
  MOUSE   mouseTrg = GetTrgInMouseButton();

  //ボタン入力でタイトルに進む
  if(keyTrg.rEnter || joyTrg.button[1] || mouseTrg.button[0])
  {
    ChangeGameState(GAME_STATE_STAFFROLL);
  }
  //ボタン入力でゲームを終了する
  if(keyTrg.esc || joyTrg.button[10])
  {
    ChangeGameState(GAME_STATE_END);
  }

  //時間経過でタイトルに戻す
  if(g_clearTime > 60 * 5)
  {
    ChangeGameState(GAME_STATE_STAFFROLL);
  }
  g_clearTime++;
}

/// <summary>
/// クリア画面の描画
/// </summary>
void RenderClear()
{
  //クリア背景の描画
  RenderTexture(0, 0, g_textureHandles[ED_BACK], 255);
  RenderTexture(0, 0, g_textureHandles[ED_1], 255);
}