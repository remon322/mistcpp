#include "../../00_application/app/app.h"
#include "../../01_data/sound/dslib.h"
#include "../../01_data/sound/loadSound.h"
#include "../../01_data/texture/loadTexture.h"
#include "../../01_data/status/loadStatus.h"
#include "../../02_input/input.h"

static int g_openingTime; //オープニングの経過時間
static int g_nowPageNum;  //オープニングのページ数
static int g_pageAlpha;   //オープニング画像のアルファ値

/// <summary>
/// オープニングの初期化
/// </summary>
void InitOpening()
{
  g_openingTime = 0;
  g_nowPageNum = 0;
  g_pageAlpha = 255;
  DSoundPlay(g_soundHandles[OPENING_BGM], true);
}

/// <summary>
/// オープニングの更新
/// </summary>
void UpdateOpening()
{
  g_openingTime++;

  KEY_BOARD key = GetTrgInKeyBoard();
  JOY_STICK joy = GetTrgInJoyStickButton();

  //キー操作で次ページへ
  if(key.space || key.rEnter || joy.button[1])
  {
    g_nowPageNum++;
    g_openingTime = 0;
  }

  //一定ページを超えたらゲームへ
  if (g_nowPageNum >= 3)
    ChangeGameState(GAME_STATE_INGAME);
}

/// <summary>
/// オープニングの描画
/// </summary>
void RenderOpening()
{
  //背景
  RenderTexture(0, 0, g_textureHandles[OP_BACK], 255);
  //ストーリー
  RenderTexture(0,0, g_textureHandles[OP_1 + g_nowPageNum], g_pageAlpha);

  //if(g_openingTime < GetStatus().pageFlickTime_)
  //  RenderBlockTexture(0, 0, g_textureHandles[MESSAGE_OUT], 255, g_openingTime / GetStatus().pageFlickTime_ % 5);
}