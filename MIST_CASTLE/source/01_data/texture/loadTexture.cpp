#include "loadTexture.h"
#include "../../00_application/lib/myDx9Lib.h"
#include "../../00_application/app/app.h"

//テクスチャハンドル情報
void* g_textureHandles[TEXTURE_MAX] = {0};

/// <summary>
/// テクスチャハンドル情報の初期化
/// </summary>
void InitTextureHandles()
{
  g_textureHandles[TITLE_BACK]          = LoadTexture("res/texture/2D/title/titleBack.png");
  g_textureHandles[TITLE_LOGO]          = LoadTexture("res/texture/2D/title/logo.png");
  g_textureHandles[TITLE_START]         = LoadBlockTexture("res/texture/2D/title/start.png", 2, 1);
  g_textureHandles[TITLE_END]           = LoadBlockTexture("res/texture/2D/title/end.png", 2, 1);
  g_textureHandles[UNDER_SIGN]          = LoadBlockTexture("res/texture/2D/title/sing.png", 7, 1);
  g_textureHandles[GAMEOVER]            = LoadTexture("res/texture/2D/gameover/you_dead.png");
  g_textureHandles[GAMEOVER_BACK]       = LoadTexture("res/texture/2D/gameover/gameoverBack.png"); 
  g_textureHandles[GAMEOVER_RETURN_TO]  = LoadTexture("res/texture/2D/gameover/returnTo.png");
  g_textureHandles[GAMEOVER_GAME]       = LoadBlockTexture("res/texture/2D/gameover/re_game.png", 2, 1);
  g_textureHandles[GAMEOVER_TITLE]      = LoadBlockTexture("res/texture/2D/gameover/re_title.png", 2, 1);
  g_textureHandles[CLEAR_BACK]          = LoadTexture("res/texture/2D/oped/clear.png");
  g_textureHandles[STAFFROLL_BACK]      = LoadTexture("res/texture/2D/staffroll/staffroll.png");
  g_textureHandles[FOG]                 = LoadBlockTexture("res/texture/2D/effect/fog.png", 20, 1);
  g_textureHandles[NUMBERS]             = LoadBlockTexture("res/texture/2D/UI/number.png", 11, 1);
  g_textureHandles[LEFT_WINDOW]         = LoadTexture("res/texture/2D/UI/left_window.png");
  g_textureHandles[MUL_MARK]            = LoadTexture("res/texture/2D/mulMark.png");
  g_textureHandles[BULLET_ICON]         = LoadTexture("res/texture/2D/bullet.png");
  g_textureHandles[HP_FRAME]            = LoadTexture("res/texture/2D/hpFrame.png");
  g_textureHandles[HP_GAUGE]            = LoadTexture("res/texture/2D/hpGauge.png");
  g_textureHandles[HP_BACK]             = LoadTexture("res/texture/2D/lostHpGauge.png");
  g_textureHandles[CROSS_HAIR]          = LoadTexture("res/texture/2D/crossHair2.png");
  g_textureHandles[SCARECROW]           = LoadBlockTexture("res/texture/2D/scarecrow.png", 6, 1);
  g_textureHandles[DROP_ITEM_HP]        = LoadBlockTexture("res/texture/2D/effect/effect_hp.png", 5, 8);
  g_textureHandles[DROP_ITEM_SP]        = LoadBlockTexture("res/texture/2D/effect/effect_sp.png", 5, 8);
  g_textureHandles[LIGHT_SOURCE]        = LoadTexture("res/texture/2D/effect/light.png");
  g_textureHandles[MISTCOMPRESSOR_ON]   = LoadBlockTexture("res/texture/2D/effect/practice_ari.png", 16, 1);
  g_textureHandles[MISTCOMPRESSOR_OFF]  = LoadBlockTexture("res/texture/2D/effect/practice_nasi.png", 16, 1);
  g_textureHandles[TREASURE_EFFECT]     = LoadBlockTexture("res/texture/2D/effect/treasure.png", 2, 2);
  g_textureHandles[UI_BASE]             = LoadTexture("res/texture/2D/UI/player_ui.png");
  g_textureHandles[UI_HP_FRAME]         = LoadTexture("res/texture/2D/UI/player_gauge_hp.png");
  g_textureHandles[UI_HP_COLOR]         = LoadTexture("res/texture/2D/UI/hp.png");
  g_textureHandles[UI_MP_FRAME]         = LoadTexture("res/texture/2D/UI/player_gauge_mp.png");
  g_textureHandles[UI_MP_COLOR]         = LoadBlockTexture("res/texture/2D/UI/player_gauge_mp_iro.png", 1, 4);
  g_textureHandles[UI_SP_FRAME]         = LoadTexture("res/texture/2D/UI/player_gauge_sp.png");
  g_textureHandles[UI_SP_COLOR]         = LoadTexture("res/texture/2D/UI/sp.png");
  g_textureHandles[UI_SKILL_MINUS]      = LoadBlockTexture("res/texture/2D/UI/skillMinus.png", 1, 2);
  g_textureHandles[UI_SKILL_PLUS]       = LoadBlockTexture("res/texture/2D/UI/skillPlus.png", 1, 2);
  g_textureHandles[UI_MIST_LEVEL]       = LoadBlockTexture("res/texture/2D/UI/mist_level.png", 5, 1);
  g_textureHandles[UI_ITEM_CHOOSE_BASE] = LoadTexture("res/texture/2D/UI/item_choose_base.png");
  g_textureHandles[UI_ITEM_CHOOSE_UP]   = LoadTexture("res/texture/2D/UI/item_choose_ue.png");
  g_textureHandles[UI_ITEM_CHOOSE_LEFT] = LoadBlockTexture("res/texture/2D/UI/item_choose_left.png", 2, 1);
  g_textureHandles[UI_ITEM_CHOOSE_RIGHT]= LoadBlockTexture("res/texture/2D/UI/item_choose_right.png", 2, 1);
  g_textureHandles[UI_ARROW]            = LoadBlockTexture("res/texture/2D/UI/arrow.png", 5, 4);
  g_textureHandles[UI_TREASURE_ARROW]   = LoadBlockTexture("res/texture/2D/UI/arrowTreasure.png", 5, 4);
  g_textureHandles[MESSAGE_OUT]         = LoadBlockTexture("res/texture/2D/oped/messege_off.png", 5, 1);
  g_textureHandles[OP_BACK]             = LoadTexture("res/texture/2D/oped/op_back.png");
  g_textureHandles[OP_1]                = LoadTexture("res/texture/2D/oped/op_1.png");
  g_textureHandles[OP_2]                = LoadTexture("res/texture/2D/oped/op_2.png");
  g_textureHandles[OP_3]                = LoadTexture("res/texture/2D/oped/op_3.png");
  g_textureHandles[ED_BACK]             = LoadTexture("res/texture/2D/oped/ed_back.png");
  g_textureHandles[ED_1]                = LoadTexture("res/texture/2D/oped/ed_1.png");
  g_textureHandles[UI_ITEM_CHOOSE_RIGHT]= LoadBlockTexture("res/texture/2D/UI/item_choose_right.png", 2, 1);
}