#pragma once

#include <d3dx9.h>
#include "../../00_application/lib/myDx9Lib.h"

//モデルデータ管理用列挙
enum XFILE_NAME
{
  PLAYER,
  FOG_TUBE,
  MAP,
  TERRAIN,
  SKELETON,
  SKELETON_HALF,
  SKELETON_ARMOR,
  MIST_COMPRESSOR,
  LAUNCHER,
  ARROW,
  SPIDERWEB,
  AUTO_SPEAR,
  TRAP_SPEAR,
  SPEAR,
  DROP_ITEMS,
  ALTER,
  TREASURE,

  XFILE_MAX
};

//メッシュハンドル管理用配列
extern void*       g_meshHandles[];
extern D3DXVECTOR3 g_meshScaleTable[];
//アニメーションハンドル管理用配列
extern char*       g_animNameTable[];

//全モデルデータの読み込み
void InitMeshHandles();

//アニメーション管理用
enum ANIMATION_NAME
{
  ANIM_PLAYER_IDLE,
  ANIM_PLAYER_MOVE,
  ANIM_PLAYER_KNOCKBACK,
  ANIM_PLAYER_DEAD,

  ANIM_SKELETON_MOVE,
  ANIM_SKELETON_ATTACK,

  ANIM_HALF_SKELETON_IDLE,
  ANIM_HALF_SKELETON_MOVE,

  ANIM_ARMOR_SKELETON_MOVE,
  ANIM_ARMOR_SKELETON_ATTACK,

  ANIM_MAX,
};