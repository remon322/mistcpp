#include "loadModel.h"
#include "../../00_application/lib/myDx9Lib.h"
#include "../../00_application/app/app.h"

//メッシュ情報
void* g_meshHandles[XFILE_MAX] = { 0 };
D3DXVECTOR3 g_meshScaleTable[XFILE_MAX] = { D3DXVECTOR3(0.0f, 0.0f, 0.0f) };
char* g_animNameTable[ANIM_MAX] = {"\0"};

//プロトタイプ宣言
void LoadFbxAnimation();

/// <summary>
/// メッシュ情報の初期化
/// </summary>
void InitMeshHandles()
{
  void* texHandle = LoadBlockTexture("res/texture/2D/load.png", 4, 1);
  //ロード画面の描画1
  if(BeginRender())
  {
    RenderBlockTexture(0, 0, texHandle, 255, 0);
    EndRender();
  }

  g_meshHandles[PLAYER]          = LoadModel("res/fbx/player/Grim_final.fbx", FBX_MESH);
  g_meshHandles[SKELETON]        = LoadModel("res/fbx/skeleton/skeleton.fbx", FBX_MESH);
  g_meshHandles[SKELETON_HALF]   = LoadModel("res/fbx/skeleton/un_skeleton.fbx", FBX_MESH);
  g_meshHandles[SKELETON_ARMOR]  = LoadModel("res/fbx/skeleton/st_skeleton.fbx", FBX_MESH);

  //ロード画面の描画2
  if(BeginRender())
  {
    RenderBlockTexture(0, 0, texHandle, 255, 1);
    EndRender();
  }

  g_meshHandles[DROP_ITEMS];
  g_meshHandles[FOG_TUBE]   = LoadModel("res/fbx/fog/fog.fbx", FBX_MESH);
  g_meshHandles[MAP] = LoadModel("res/fbx/map/map1/map1low.fbx", FBX_MESH);

    //ロード画面の描画2
  if(BeginRender())
  {
    RenderBlockTexture(0, 0, texHandle, 255, 1);
    EndRender();
  }

  g_meshHandles[TERRAIN] = LoadModel("res/fbx/map/map1/map1_atari.fbx", FBX_MESH);
  g_meshHandles[MIST_COMPRESSOR] = LoadModel("res/fbx/object/MistRantan.fbx", FBX_MESH);
  g_meshHandles[LAUNCHER]   = LoadModel("res/fbx/object/ArrowTrap.fbx", FBX_MESH);
  g_meshHandles[ARROW]      = LoadModel("res/fbx/object/Arrow.fbx", FBX_MESH);
  g_meshHandles[SPIDERWEB]  = LoadModel("res/fbx/object/kumonosu.fbx", FBX_MESH);

  //ロード画面の描画3
  if(BeginRender())
  {
    RenderBlockTexture(0, 0, texHandle, 255, 2);
    EndRender();
  }

  g_meshHandles[AUTO_SPEAR] = LoadModel("res/fbx/object/TogeTrap.fbx", FBX_MESH);
  g_meshHandles[TRAP_SPEAR] = LoadModel("res/fbx/object/TogeTrap.fbx", FBX_MESH);
  g_meshHandles[SPEAR]      = LoadModel("res/fbx/object/Toge.fbx", FBX_MESH);
  g_meshHandles[ALTER]      = LoadModel("res/fbx/object/alter.fbx", FBX_MESH);
  g_meshHandles[TREASURE]   = LoadModel("res/fbx/object/takarabako.fbx", FBX_MESH);

  //ロード画面の描画4
  if(BeginRender())
  {
    RenderBlockTexture(0, 0, texHandle, 255, 3);
    EndRender();
  }

  g_meshScaleTable[PLAYER]          = { 1.0f, 1.0f, 1.0f};
  g_meshScaleTable[SKELETON]        = { 1.0f, 1.0f, 1.0f};
  g_meshScaleTable[DROP_ITEMS]      = { 1.0f, 1.0f, 1.0f };
  g_meshScaleTable[FOG_TUBE]        = { 2.0f, 2.0f, 2.0f};
  g_meshScaleTable[MAP]             = { 1.0f, 1.0f, 1.0f};
  g_meshScaleTable[TERRAIN]         = { 1.0f, 1.0f, 1.0f};
  g_meshScaleTable[MIST_COMPRESSOR] = { 1.0f, 1.0f, 1.0f};
  g_meshScaleTable[LAUNCHER]        = { 1.0f, 1.0f, 1.0f};
  g_meshScaleTable[ARROW]           = { 2.0f, 2.0f, 2.0f};
  g_meshScaleTable[SPIDERWEB]       = { 1.0f, 1.0f, 1.0f};
  g_meshScaleTable[AUTO_SPEAR]      = { 1.0f, 1.0f, 1.0f};
  g_meshScaleTable[TRAP_SPEAR]      = { 1.0f, 1.0f, 1.0f};
  g_meshScaleTable[SPEAR]           = { 1.0f, 1.0f, 1.0f};
  g_meshScaleTable[ALTER]           = { 10.0f, 10.0f,10.0f };
  g_meshScaleTable[TREASURE]        = { 1.0f, 1.0f, 1.0f };

  LoadFbxAnimation();
}

/// <summary>
/// アニメーション管理名テーブルの初期化
/// </summary>
void InitAnimNameTable()
{
  //プレイヤーのモーション
  g_animNameTable[ANIM_PLAYER_IDLE]           = "playerIdle";
  g_animNameTable[ANIM_PLAYER_MOVE]           = "playerMove";
  g_animNameTable[ANIM_PLAYER_KNOCKBACK]      = "playerKnockBack";
  g_animNameTable[ANIM_PLAYER_DEAD]           = "playerDead";
  //スケルトンのモーション
  g_animNameTable[ANIM_SKELETON_MOVE]         = "skeletonMove";
  g_animNameTable[ANIM_SKELETON_ATTACK]       = "skeletonAttack";
  //上半身のみのスケルトンのモーション
  g_animNameTable[ANIM_HALF_SKELETON_IDLE]    = "halfSkeletonIdle";
  g_animNameTable[ANIM_HALF_SKELETON_MOVE]    = "halfSkeletonMove";
  //防具付きスケルトンのモーション
  g_animNameTable[ANIM_ARMOR_SKELETON_MOVE]   = "armorSkeletonMove";
  g_animNameTable[ANIM_ARMOR_SKELETON_ATTACK] = "armorSkeletonAttack";
}

/// <summary>
/// メッシュデータにアニメーションデータを読み込む
/// </summary>
void LoadFbxAnimation()
{
  InitAnimNameTable();
  //プレイヤーのアニメーション
  AddFbxAnimation(g_animNameTable[ANIM_PLAYER_IDLE], "res/fbx/player/taiki.fbx", g_meshHandles[PLAYER]);
  AddFbxAnimation(g_animNameTable[ANIM_PLAYER_MOVE], "res/fbx/player/moveroop.fbx", g_meshHandles[PLAYER]);
  AddFbxAnimation(g_animNameTable[ANIM_PLAYER_KNOCKBACK], "res/fbx/player/grimu_damage.fbx", g_meshHandles[PLAYER]);
  AddFbxAnimation(g_animNameTable[ANIM_PLAYER_DEAD], "res/fbx/player/grimu_dead.fbx", g_meshHandles[PLAYER]);
  //スケルトンのアニメーション
  AddFbxAnimation(g_animNameTable[ANIM_SKELETON_MOVE], "res/fbx/skeleton/skeleton_walk.fbx", g_meshHandles[SKELETON]);
  AddFbxAnimation(g_animNameTable[ANIM_SKELETON_ATTACK], "res/fbx/skeleton/skeleton_at.fbx", g_meshHandles[SKELETON]);
  //上半身のみのスケルトンのモーション
  AddFbxAnimation(g_animNameTable[ANIM_HALF_SKELETON_IDLE], "res/fbx/skeleton/un_skeleton_wait.fbx", g_meshHandles[SKELETON_HALF]);
  AddFbxAnimation(g_animNameTable[ANIM_HALF_SKELETON_MOVE], "res/fbx/skeleton/un_skeleton_walk.fbx", g_meshHandles[SKELETON_HALF]);
  //防具付きスケルトンのモーション
  AddFbxAnimation(g_animNameTable[ANIM_ARMOR_SKELETON_MOVE], "res/fbx/skeleton/st_skeleton_walk.fbx", g_meshHandles[SKELETON_ARMOR]);
  AddFbxAnimation(g_animNameTable[ANIM_ARMOR_SKELETON_ATTACK], "res/fbx/skeleton/st_skeleton_at.fbx", g_meshHandles[SKELETON_ARMOR]);
}