#pragma once

//オブジェクトの設置
void Edit();

//エディット中の詳細描画
void RenderEdit();

//マップデータとオブジェクトの再読み込み
void ChangeStage(int stageNum);

//オブジェクトの読み込み
void LoadObjData();