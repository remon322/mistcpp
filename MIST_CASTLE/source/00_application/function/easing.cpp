/*------------------------------------------------------------------
ファイル名 = easing.cpp
動作       = イージング関数のまとめ
制作者     = 高柳俊一
/------------------------------------------------------------------*/
#include <math.h>
#include <stdint.h>
#include "easing.h"

#define PAI (3.14159265359)

double EasingLinear(double t, double b, double c, double d)
{
  return c * t / d + b;
}

double EasingInQuad(double t, double b, double c, double d)
{
  t /= d;
  return c * t * t + b;
}
double EasingOutQuad(double t, double b, double c, double d)
{
  t /= d;
  return -c * t * (t - 2) + b;
}
double EasingInOutQuad(double t, double b, double c, double d)
{
  t /= d / 2.0;
  if (t < 1)
  {
    return c / 2.0 * t * t + b;
  }
  t--;
  return -c / 2.0 * (t * (t - 2) - 1) + b;
}

double EasingInCubic(double t, double b, double c, double d)
{
  t /= d;
  return c * t*t*t + b;
}
double EasingOutCubic(double t, double b, double c, double d)
{
  t /= d;
  t--;
  return c * (t*t*t + 1) + b;
}
double EasingInOutCubic(double t, double b, double c, double d)
{
  t /= d / 2.0;
  if (t < 1)
  {
    return c / 2.0 * t*t*t + b;
  }
  t -= 2;
  return c / 2.0*(t*t*t + 2) + b;
}

double EasingInQuart(double t, double b, double c, double d)
{
  t /= d;
  return c * t*t*t*t + b;
}
double EasingOutQuart(double t, double b, double c, double d)
{
  t /= d;
  t--;
  return -c * (t*t*t*t - 1) + b;
}
double EasingInOutQuart(double t, double b, double c, double d)
{
  t /= d / 2.0;
  if (t < 1)
  {
    return c / 2.0 * t*t*t*t + b;
  }
  t -= 2;
  return -c / 2.0 * (t*t*t*t - 2) + b;
}

double EasingInQuint(double t, double b, double c, double d)
{
  t /= d;
  return c * t*t*t*t*t + b;
}
double EasingOutQuint(double t, double b, double c, double d)
{
  t /= d;
  t--;
  return c * (t*t*t*t*t + 1) + b;
}
double EasingInOutQuint(double t, double b, double c, double d)
{
  t /= d / 2.0;
  if (t < 1)
  {
    return c / 2.0 * t*t*t*t*t + b;
  }
  t -= 2;
  return c / 2.0*(t*t*t*t*t + 2) + b;
}

double EasingInSine(double t, double b, double c, double d)
{
  return -c * cos(t / d * (PAI / 2.0)) + c + b;
}
double EasingOutSine(double t, double b, double c, double d)
{
  return c * sin(t / d * PAI / 2.0) + b;
}
double EasingInOutSine(double t, double b, double c, double d)
{
  return -c / 2.0 * (cos(PAI*t / d) - 1) + b;
}

double EasingInExpo(double t, double b, double c, double d)
{
  return c * pow(2, 10 * (t / d - 1)) + b;
}
double EasingOutExpo(double t, double b, double c, double d)
{
  return c * (-pow(2, -10 * t / d) + 1) + b;
}
double EasingInOutExpo(double t, double b, double c, double d)
{
  t /= d / 2.0;
  if (t < 1)
  {
    return c / 2.0 * pow(2, 10 * (t - 1)) + b;
  }
  t--;
  return c / 2.0 * (-pow(2, -10 * t) + 2) + b;
}

double EasingInCirc(double t, double b, double c, double d)
{
  t /= d;
  return -c * (sqrt(1 - t * t) - 1) + b;
}
double EasingOutCirc(double t, double b, double c, double d)
{
  t /= d;
  t--;
  return c * sqrt(1 - t * t) + b;
}
double EasingInOutCirc(double t, double b, double c, double d)
{
  t /= d / 2.0;
  if (t < 1)
  {
    return -c / 2.0 * (sqrt(1 - t * t) - 1) + b;
  }
  t -= 2;
  return c / 2 * (sqrt(1 - t * t) + 1) + b;
}

/// <summary>
/// イージングの種類を判断して行う
/// </summary>
/// <param name="t">進行度</param>
/// <param name="b">開始の値(開始時の座標やスケールなど)</param>
/// <param name="c">開始と終了の値の差分</param>
/// <param name="d">合計時間</param>
/// <returns>補間された結果の値</returns>
double JudgeAndEasing(EASING easing, double t, double b, double c, double d)
{
  if (t <= 0 || c == 0)
  {
    return b;
  }
  else if(t >= d)
  {
    return (b + c);
  }

  switch (easing)
  {
  case NONE:
    return b;
    break;
  case LINEAR:
    return EasingLinear(t, b, c, d);
    break;
  case IN_QUAD:
    return EasingInQuad(t, b, c, d);
    break;
  case OUT_QUAD:
    return EasingOutQuad(t, b, c, d);
    break;
  case IN_OUT_QUAD:
    return EasingInOutQuad(t, b, c, d);
    break;
  case IN_CUBIC:
    return EasingInCubic(t, b, c, d);
    break;
  case OUT_CUBIC:
    return EasingOutCubic(t, b, c, d);
    break;
  case IN_OUT_CUBIC:
    return EasingInOutCubic(t, b, c, d);
    break;
  case IN_QUART:
    return EasingInQuart(t, b, c, d);
    break;
  case OUT_QUART:
    return EasingOutQuart(t, b, c, d);
    break;
  case IN_OUT_QUART:
    return EasingInOutQuart(t, b, c, d);
    break;
  case IN_QUINT:
    return EasingInQuint(t, b, c, d);
    break;
  case OUT_QUINT:
    return EasingOutQuint(t, b, c, d);
    break;
  case IN_OUT_QUINT:
    return EasingInOutQuint(t, b, c, d);
    break;
  case IN_SINE:
    return EasingInSine(t, b, c, d);
    break;
  case OUT_SINE:
    return EasingOutSine(t, b, c, d);
    break;
  case IN_OUT_SINE:
    return EasingInOutSine(t, b, c, d);
    break;
  case IN_EXPO:
    return EasingInExpo(t, b, c, d);
    break;
  case OUT_EXPO:
    return EasingOutExpo(t, b, c, d);
    break;
  case IN_OUT_EXPO:
    return EasingInOutExpo(t, b, c, d);
    break;
  case IN_CIRC:
    return EasingInCirc(t, b, c, d);
    break;
  case OUT_CIRC:
    return EasingOutCirc(t, b, c, d);
    break;
  case IN_OUT_CIRC:
    return EasingInOutCirc(t, b, c, d);
    break;
  }
  return b;
}