#pragma once
#include "../lib/myDx9Lib.h"

//ベクトルのx_成分の抜出
float GetVectorComponentX(float r, float theta_, float phi_);

//ベクトルのy_成分の抜出
float GetVectorComponentY(float r, float theta_, float phi_);

//ベクトルのz成分の抜出
float GetVectorComponentZ(float r, float theta_, float phi_);

//ベクトルのxyz成分の抜出
D3DXVECTOR3 GetVectorComponentXYZ(float r, float theta_, float phi_);

//xyz成分から角度θとφの抜出
void GetVectorPolar(D3DXVECTOR3 vector, float* r, float* theta_, float* phi_);

//2D平面のx_成分とy_成分からスカラー量を求める
float GetScalar2D(float x, float y);

//3Dベクトルの2点間の距離を求める
float GetDistance(D3DXVECTOR3 a, D3DXVECTOR3 b);

//2D平面の原点(0,0)から見た2点の位置ベクトルから内積を求める
float DotProduct(float x1, float y1, float x2, float y2);

//2D平面の原点から見た2点の位置ベクトルから外積を求める
float CrossProduct(float x1, float y1, float x2, float y2);

//原点から見た2点の位置ベクトルから内積を求める
float DotProduct3D(D3DXVECTOR3 a, D3DXVECTOR3 b);

//原点から見た2点の位置ベクトルから外積を求める
D3DXVECTOR3 CrossProduct3D(D3DXVECTOR3 a, D3DXVECTOR3 b);

//ベクトルとスカラーの掛け算
D3DXVECTOR3 MulVectorScalar(D3DXVECTOR3 a, float b);

//ベクトルとスカラーの割り算
D3DXVECTOR3 DivVectorScalar(D3DXVECTOR3 a, float b);

//ベクトルを単位ベクトル化する
D3DXVECTOR3 UnitVector(D3DXVECTOR3 vector);

//ベクトルのスカラー量を取得する
float GetScalar(D3DXVECTOR3 vector);

//角度を0~2*PIにする
float NormalizeAngle(float angle);