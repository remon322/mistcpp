#pragma once

enum EASING
{
  NONE,
  LINEAR,

  IN_QUAD,
  OUT_QUAD,
  IN_OUT_QUAD,

  IN_CUBIC,
  OUT_CUBIC,
  IN_OUT_CUBIC,

  IN_QUART,
  OUT_QUART,
  IN_OUT_QUART,

  IN_QUINT,
  OUT_QUINT,
  IN_OUT_QUINT,

  IN_SINE,
  OUT_SINE,
  IN_OUT_SINE,

  IN_EXPO,
  OUT_EXPO,
  IN_OUT_EXPO,

  IN_CIRC,
  OUT_CIRC,
  IN_OUT_CIRC,
};

/// <summary>
/// イージングの種類を判断して行う
/// </summary>
/// <param name="t">進行度</param>
/// <param name="b">開始の値(開始時の座標やスケールなど)</param>
/// <param name="c">開始と終了の値の差分</param>
/// <param name="d">合計時間</param>
/// <returns>補間された結果の値</returns>
double JudgeAndEasing(EASING easing, double t, double b, double c, double d);