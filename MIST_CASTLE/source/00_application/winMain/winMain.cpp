#include <windows.h>
#include <mmsystem.h>
#include <d3dx9.h>
#include <strsafe.h>
#include "../app/app.h"
#include "../lib/myDx9Lib.h"
#include "../../01_data/sound/dslib.h"
#include "../../02_input/input.h"
#include "../../03_update/update.h"
#include "../../04_render/render.h"

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static int g_frame = 0;

int WINAPI WinMain(
  HINSTANCE hInstance,
  HINSTANCE hPrevInstance,
  PSTR lpCmdLine,
  int nCmdShow)
{
  HWND hWnd;
#ifdef DEBUG_MODE
  SetWindowOption(DISP_W, DISP_H, APP_NAME, false);
#else
  SetWindowOption(DISP_W, DISP_H, APP_NAME, false);
#endif
  //3Dの初期化とウィンドウの作成
  hWnd = InitWindow(hInstance);

  //3Dの初期化
  if(!InitD3D(hWnd))
  {
    return false;//初期化失敗
  }

  //音声の初期化
  if(!DSoundInit(hWnd, NULL))
  {
    return false;//初期化失敗
  }

  //入力の初期化
  if(!InitDinput(hWnd))
  {
    return false;//初期化失敗
  }

  //HLSLの初期化
  if(!MakeShaderEffects())
  {
    return false;//初期化失敗
  }

  //アプリケーションの初期化
  AppInit();

  MSG msg;
  ZeroMemory(&msg, sizeof(msg));
  InitInGame();
  //メインフレーム
  while(msg.message != WM_QUIT && g_isContinueGame)
  {
    if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))    // 受け取ったメッセージを送出、ポスト済みがすでにあれば指定された構造体にメッセージを格納。&msgにメッセージ情報を格納、PM_REMOVE：処理後メッセージを保存しておかない
    {
      TranslateMessage(&msg);   // 仮想キーメッセージを文字メッセージへ変換。変換が行われた場合は0以外の値が返ってくる
      DispatchMessage(&msg);    // 1 つのウィンドウプロシージャへメッセージをディスパッチ（送出）します
    }
    else
    {
      g_frame++;
      //入力の更新
      InputUpdate();

      //更新
      Update();

      //描画
      Render();
    }
  }

  Cleanup();
  DSoundRelease();
  DeleteWindow(hInstance);
  return 0;
}