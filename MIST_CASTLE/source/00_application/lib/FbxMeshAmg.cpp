//#define FBXSDK_SHARED
#include <fbxsdk.h>
#include "FbxMeshAmg.h"
#include "myDx9Lib.h"


#if _DEBUG
#pragma comment( lib, "debug/libfbxsdk-md.lib")
#else
#pragma comment( lib, "release/libfbxsdk-md.lib")
#endif

//****************************************************************
//
//	更新
//
//****************************************************************
//------------------------------------------------
//	更新
//------------------------------------------------
void FBXMESHAMG::Update()
{
  //D3DXMATRIX mat;
  ////D3DXMatrixIdentity(&mat);
  ////	ワールド変換行列(=姿勢)
  //D3DXMatrixRotationYawPitchRoll(
  //  &mat,
  //  rotation.y_, rotation.x_, rotation.z_
  //);

  //mat._11 *= scale_.x_;
  //mat._12 *= scale_.x_;
  //mat._13 *= scale_.x_;
  //mat._21 *= scale_.y_;
  //mat._22 *= scale_.y_;
  //mat._23 *= scale_.y_;
  //mat._31 *= scale_.z_;
  //mat._32 *= scale_.z_;
  //mat._33 *= scale_.z_;

  //mat._41 = position.x_;	// X座標
  //mat._42 = position.y_;	// Y座標
  //mat._43 = position.z_;	// Z座標

  ////	行列保存
  //transform = mat;
}

//------------------------------------------------
//	アニメーション
//------------------------------------------------
void FBXMESHAMG::Animate(ANIMATION_DATA* animationData, float sec)
{
  //animationData_があるかチェック
  if(animationData == NULL)
    return;

  float DeltaTime = sec;
  //	モーション時間の更新
  animationData->frame += DeltaTime * 60;
  //	ループチェック
  if(animationData->frame >= motion[animationData->motionName].NumFrame - 1)
  {
    // ループ
    animationData->frame = 0; // 全体をループ
  }
}

//****************************************************************
//
//	描画
//
//****************************************************************

const int stackMax = 1;

struct STACK_VERTEX
{
  float x, y, z, w;     // 座標
  float wx, wy, wz, ww; // 重み(ダミー)
  float tu, tv;         // UV
  float nx, ny, nz;     // 法線
  float tx, ty, tz;     // 接線
  float bx, by, bz, index;// 従法線 + index
};

//まとめて描画する用のスタック構造体
struct RENDER_STACK_DATA
{
  LPDIRECT3DTEXTURE9 textureData[stackMax];
  LPDIRECT3DTEXTURE9 normalData[stackMax];
  D3DXMATRIX         matWorld[stackMax];
  D3DXMATRIX         matRota[stackMax];
  STACK_VERTEX*      vertex[stackMax];
  int                vertexNum[stackMax];

  int                stackNum = 0;//現在スタックされている数
};

//スタック構造体の実体
static RENDER_STACK_DATA g_renderStackData;
static RENDER_STACK_DATA g_renderNoShadeStackData;

//------------------------------------------------
//	描画
//------------------------------------------------
// d3d_device        : DirectXの3Dデバイス
// shaderEffect      : シェーダーのエフェクトインターフェース
// VertexDeclaration : シェーダーの頂点データフォーマット
// matWorld          : レンダリングするもののワールド行列
void FBXMESHAMG::Render(ANIMATION_DATA* animationData, IDirect3DDevice9* d3d_device, ID3DXEffect* shaderEffect, LPDIRECT3DVERTEXDECLARATION9 VertexDeclaration, D3DXMATRIX matWorld, D3DXMATRIX matRotation, bool isShade)
{
  //	モーションが存在する場合はSkinning
  if(motion[animationData->motionName].NumFrame > 0 && animationData->motionName != "default")
  {
    Skinning(animationData);
  }

  //デフォルトテクスチャを1回だけ読み込み
  static LPDIRECT3DTEXTURE9 normalMap = NULL;
  static LPDIRECT3DTEXTURE9 texture = NULL;
  static int normalMapFlag = 0;
  static int textureFlag = 0;
  if(normalMapFlag == 0)
  {
    (D3DXCreateTextureFromFile(d3d_device,
     "res/texture/3D/default_normal.png",
     &normalMap));
    normalMapFlag++;
  }
  if(textureFlag == 0)
  {
    (D3DXCreateTextureFromFile(d3d_device,
     "res/texture/3D/default_texture.png",
     &texture));
    textureFlag++;
  }

  int start = 0;
  for(int m = 0; m < NumMesh; m++)
  {
    //テクスチャデータがないときデフォルトテクスチャにする
    int material_no = MeshMaterial[m];
    if(Textures[material_no] == NULL)
      Textures[material_no] = texture;
    if(NTextures[material_no] == NULL)
      NTextures[material_no] = normalMap;

    //スタックに描画情報を格納する
    PushRenderStack(d3d_device, shaderEffect, VertexDeclaration,
                    Textures[material_no], NTextures[material_no], matWorld, matRotation, isShade, MaterialFaces[m], Vertices, &Indices[start]);

    //頂点インデックスの開始位置を変更
    start += MaterialFaces[m];
  }
}

//スタックに描画情報を格納する
void FBXMESHAMG::PushRenderStack(IDirect3DDevice9* d3d_device, ID3DXEffect* shaderEffect, LPDIRECT3DVERTEXDECLARATION9 VertexDeclaration, LPDIRECT3DTEXTURE9 texture, LPDIRECT3DTEXTURE9 normalTex, D3DXMATRIX worldMatrix, D3DXMATRIX rotationMatrix, bool isShade, int numVertices, PolygonVertex* pVertices, DWORD* pIndices)
{
  RENDER_STACK_DATA* stackData = NULL;
  //陰の存在別に分ける
  if(isShade)
  {
    stackData = &g_renderStackData;
  }
  else
  {
    stackData = &g_renderNoShadeStackData;
  }

  //現在のスタック数がいっぱいだったら描画する
  if(stackData->stackNum >= stackMax)
  {
    RenderStack(isShade, d3d_device, shaderEffect, VertexDeclaration);
  }

  //データを格納する
  stackData->textureData[stackData->stackNum] = texture;
  stackData->normalData[stackData->stackNum] = normalTex;
  stackData->matWorld[stackData->stackNum] = worldMatrix;
  stackData->matRota[stackData->stackNum] = rotationMatrix;

  //カメラから頂点の距離の2乗を入れる変数
  float vertexDistanceSquare = 0;
  //カメラの座標を取得
  D3DXVECTOR3 camPos(0, 0, 0);
  shaderEffect->GetValue("g_cameraPos", &camPos, sizeof(float) * 3);
  //カメラの座標にワールド逆行列を掛ける
  D3DXMATRIX worldInverse;
  D3DXMatrixInverse(&worldInverse, NULL, &worldMatrix);
  D3DXVec3TransformCoord(&camPos, &camPos, &worldInverse);
  //フォグの最濃距離を取得
  float fogFarDistance = 0;
  shaderEffect->GetValue("g_fogEndRange", &fogFarDistance, sizeof(float));
  //描画に必要な頂点数を数える
  int renderVerticesNum = 0;

  //霧の範囲内にあるポリゴンの頂点数をカウント
  for(int i = 0; i < numVertices; i += 3)
  {
    D3DXVECTOR3 minPos, maxPos;
    minPos = D3DXVECTOR3(pVertices[pIndices[i]].x, pVertices[pIndices[i]].y, pVertices[pIndices[i]].z);;
    maxPos = D3DXVECTOR3(pVertices[pIndices[i]].x, pVertices[pIndices[i]].y, pVertices[pIndices[i]].z);;
    //最少座標と最大座標を調べる
    minPos.x = min(minPos.x, pVertices[pIndices[i + 1]].x);
    minPos.y = min(minPos.y, pVertices[pIndices[i + 1]].y);
    minPos.z = min(minPos.z, pVertices[pIndices[i + 1]].z);
    maxPos.x = max(maxPos.x, pVertices[pIndices[i + 1]].x);
    maxPos.y = max(maxPos.y, pVertices[pIndices[i + 1]].y);
    maxPos.z = max(maxPos.z, pVertices[pIndices[i + 1]].z);

    minPos.x = min(minPos.x, pVertices[pIndices[i + 2]].x);
    minPos.y = min(minPos.y, pVertices[pIndices[i + 2]].y);
    minPos.z = min(minPos.z, pVertices[pIndices[i + 2]].z);
    maxPos.x = max(maxPos.x, pVertices[pIndices[i + 2]].x);
    maxPos.y = max(maxPos.y, pVertices[pIndices[i + 2]].y);
    maxPos.z = max(maxPos.z, pVertices[pIndices[i + 2]].z);

    //中心座標と半径を出してポリゴンサイズに準じた球の当たり判定を取る
    D3DXVECTOR3 centerPos = (maxPos + minPos) / 2.0f;
    float       radius;
    radius = maxPos.x - minPos.x;
    radius = max(radius, maxPos.y - minPos.y);
    radius = max(radius, maxPos.z - minPos.z);

    D3DXVECTOR3 distPos = centerPos - camPos;
    float distSquare = distPos.x * distPos.x + distPos.y * distPos.y + distPos.z * distPos.z;
    //ポリゴンが範囲内なら描画する(２乗同士で計算することで平方根を使わない)
    if(distSquare < (radius + fogFarDistance) * (radius + fogFarDistance))
    {
      renderVerticesNum += 3;
    }
  }//for

  //必要な頂点分のメモリ確保 & 頂点数の記録
  stackData->vertex[stackData->stackNum] = new STACK_VERTEX[renderVerticesNum];
  stackData->vertexNum[stackData->stackNum] = renderVerticesNum;


  //頂点が確保したメモリのどこに格納されるか特定するためカウント
  int cnt = 0;
  //霧の範囲内にあるポリゴンをコピー
  for(int i = 0; i < numVertices; i += 3)
  {
    D3DXVECTOR3 minPos, maxPos;
    minPos = D3DXVECTOR3(pVertices[pIndices[i]].x, pVertices[pIndices[i]].y, pVertices[pIndices[i]].z);;
    maxPos = D3DXVECTOR3(pVertices[pIndices[i]].x, pVertices[pIndices[i]].y, pVertices[pIndices[i]].z);;
    //最少座標と最大座標を調べる
    minPos.x = min(minPos.x, pVertices[pIndices[i + 1]].x);
    minPos.y = min(minPos.y, pVertices[pIndices[i + 1]].y);
    minPos.z = min(minPos.z, pVertices[pIndices[i + 1]].z);
    maxPos.x = max(maxPos.x, pVertices[pIndices[i + 1]].x);
    maxPos.y = max(maxPos.y, pVertices[pIndices[i + 1]].y);
    maxPos.z = max(maxPos.z, pVertices[pIndices[i + 1]].z);

    minPos.x = min(minPos.x, pVertices[pIndices[i + 2]].x);
    minPos.y = min(minPos.y, pVertices[pIndices[i + 2]].y);
    minPos.z = min(minPos.z, pVertices[pIndices[i + 2]].z);
    maxPos.x = max(maxPos.x, pVertices[pIndices[i + 2]].x);
    maxPos.y = max(maxPos.y, pVertices[pIndices[i + 2]].y);
    maxPos.z = max(maxPos.z, pVertices[pIndices[i + 2]].z);

    //中心座標と半径を出してポリゴンサイズに準じた球の当たり判定を取る
    D3DXVECTOR3 centerPos = (maxPos + minPos) / 2.0f;
    float       radius;
    radius = maxPos.x - minPos.x;
    radius = max(radius, maxPos.y - minPos.y);
    radius = max(radius, maxPos.z - minPos.z);

    D3DXVECTOR3 distPos = centerPos - camPos;
    float distSquare = distPos.x * distPos.x + distPos.y * distPos.y + distPos.z * distPos.z;
    //ポリゴンが範囲内なら描画する(２乗同士で計算することで平方根を使わない)
    if(distSquare < (radius + fogFarDistance) * (radius + fogFarDistance))
    {
      memcpy(&stackData->vertex[stackData->stackNum][cnt],     //STACK_VERTEXの中身がほぼ同じなためmemcpyで1頂点分まとめてコピー
             &pVertices[pIndices[i]],
             sizeof(PolygonVertex));
      memcpy(&stackData->vertex[stackData->stackNum][cnt + 1], //STACK_VERTEXの中身がほぼ同じなためmemcpyで1頂点分まとめてコピー
             &pVertices[pIndices[i + 1]],
             sizeof(PolygonVertex));
      memcpy(&stackData->vertex[stackData->stackNum][cnt + 2], //STACK_VERTEXの中身がほぼ同じなためmemcpyで1頂点分まとめてコピー
             &pVertices[pIndices[i + 2]],
             sizeof(PolygonVertex));
      //3頂点分カウントを進める
      cnt += 3;
    }
  }

  //格納されているスタック数のカウントを増やす
  stackData->stackNum++;

  //現在のスタック数がいっぱいだったら描画する
  if(stackData->stackNum >= stackMax)
  {
    RenderStack(isShade, d3d_device, shaderEffect, VertexDeclaration);
  }
}

//スタックにたまっている分をレンダリングする
void FBXMESHAMG::RenderStack(bool isShade, IDirect3DDevice9* d3d_device, ID3DXEffect* shaderEffect, LPDIRECT3DVERTEXDECLARATION9 VertexDeclaration)
{
  int           verticesNum = 0;  //全頂点数
  STACK_VERTEX* renderVertices;   //描画する頂点情報
  RENDER_STACK_DATA* renderStackData = NULL;

  //陰の有無によって描画するスタック内容を判断
  if(isShade)
    renderStackData = &g_renderStackData;
  else
    renderStackData = &g_renderNoShadeStackData;

  //全頂点数カウント
  for(int i = 0; i < renderStackData->stackNum; i++)
  {
    verticesNum += renderStackData->vertexNum[i];
  }

  //描画頂点が一つもなければ何もしない
  if(verticesNum == 0)
  {
    renderStackData->stackNum = 0;
    return;
  }

  //メモリ確保
  renderVertices = new STACK_VERTEX[verticesNum];

  //情報格納
  int cnt = 0;
  for(int i = 0; i < renderStackData->stackNum; i++)
  {
    memcpy(&renderVertices[cnt], renderStackData->vertex[i], sizeof(STACK_VERTEX) * renderStackData->vertexNum[i]);
    cnt += renderStackData->vertexNum[i];
  }

  //アクティブなテクニックを設定
  if(isShade)
    shaderEffect->SetTechnique("RenderSceneWithTexture");
  else
    shaderEffect->SetTechnique("RenderSceneWithTextureNoShade");

  //行列配列をシェーダーに送る
  HRESULT hr;
  hr = shaderEffect->SetMatrix("g_mWorld", renderStackData->matWorld);
  hr = shaderEffect->SetMatrix("g_mRotation", renderStackData->matRota);

  //テクスチャを設定
  for (int i = 0; i < renderStackData->stackNum; i++)
  {
    char texStr[24];
    char norStr[24];
    sprintf(texStr, "g_MeshTexture%d", i);
    sprintf(norStr, "g_MeshNormalMap%d", i);
    if (renderStackData->textureData[i] != NULL)
      shaderEffect->SetTexture(texStr, renderStackData->textureData[i]);
    if (renderStackData->normalData[i] != NULL)
      shaderEffect->SetTexture(norStr, renderStackData->normalData[i]);
  }

  UINT nPasses;
  UINT iPass;
  //描画
  shaderEffect->Begin(&nPasses, 0);
  for(iPass = 0; iPass < nPasses; iPass++)
  {
    //アクティブなテクニックの中でパスを開始
    shaderEffect->BeginPass(iPass);
    d3d_device->SetVertexDeclaration(VertexDeclaration);

    //ワイヤーフレーで描画
    //d3d_device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

    //頂点データのレンダリング
    d3d_device->DrawPrimitiveUP(D3DPT_TRIANGLELIST, verticesNum / 3, renderVertices, sizeof(STACK_VERTEX));

    //アクティブなテクニックの中でパスを終了
    shaderEffect->EndPass();
  }
  shaderEffect->End();

  //スタックされたデータの破棄
  delete[] renderVertices;
  //陰の有無によって破棄するスタック内容を判断
  if(isShade)
  {
    //陰有
    for(int i = 0; i < g_renderStackData.stackNum; i++)
    {
      g_renderStackData.textureData[i] = NULL;
      g_renderStackData.normalData[i] = NULL;
      delete[] g_renderStackData.vertex[i];
      g_renderStackData.vertex[i] = NULL;
      g_renderStackData.vertexNum[i] = 0;
    }
    g_renderStackData.stackNum = 0;
  }
  else
  {//陰無
    for(int i = 0; i < g_renderNoShadeStackData.stackNum; i++)
    {
      g_renderNoShadeStackData.textureData[i] = NULL;
      g_renderNoShadeStackData.normalData[i] = NULL;
      delete[] g_renderNoShadeStackData.vertex[i];
      g_renderNoShadeStackData.vertex[i] = NULL;
      g_renderNoShadeStackData.vertexNum[i] = 0;
    }
    g_renderNoShadeStackData.stackNum = 0;
  }
}


//****************************************************************

//***********
// テクスチャ多重読み込み禁止
static char TextureName[1000][128];
static int  TextureCount[1000]; // 参照カウンタ
static IDirect3DTexture9* TextureSource[1000];
static bool IsInitializeTexture = false;

void InitTexture()
{
  for(int i = 0; i < 1000; i++)
  {
    TextureName[i][0] = '\0';
    TextureSource[i] = NULL;
    TextureCount[i] = 0;
  }
  IsInitializeTexture = true;
}

IDirect3DTexture9* LoadTexture(IDirect3DDevice9 *d3d_device, char* filename)
{
  if(IsInitializeTexture == false)
  {
    InitTexture();
  }
  //	既に存在する場合
  for(int i = 0; i < 1000; i++)
  {
    if(TextureName[i][0] == '\0') continue;
    if(strcmp(TextureName[i], filename) == 0)
    {
      TextureCount[i]++;
      return TextureSource[i];
    }
  }

  //	空き場所に読み込み
  for(int i = 0; i < 1000; i++)
  {
    if(TextureName[i][0] != '\0') continue;

    IDirect3DTexture9* tex;
    HRESULT hr = D3DXCreateTextureFromFile(d3d_device, filename, &tex);
    if(hr != D3D_OK) { WriteOutLog("  %sの読み込み失敗", filename);  return NULL;}
    else { WriteOutLog("  %sの読み込み成功", filename); }
    TextureSource[i] = tex;
    strcpy(TextureName[i], filename);
    TextureCount[i] = 1;
    return tex;
  }
  return NULL;
}

void ReleaseTexture(IDirect3DTexture9* tex)
{
  //	検索
  for(int i = 0; i < 1000; i++)
  {
    if(TextureSource[i] != tex) continue;
    TextureCount[i]--;
    if(TextureCount[i] <= 0)
    {
      if(TextureSource[i] != NULL)
      {
        TextureSource[i]->Release();
        TextureSource[i] = NULL;
      }
      TextureName[i][0] = '\0';
    }
    return;
  }
  //	管理外のテクスチャ
  if(tex != NULL)
  {
    tex->Release();
  }
}


//****************************************************************
//
//	初期化
//
//****************************************************************
FBXMESHAMG::FBXMESHAMG()
{
  //	材質設定
  //	アンビエント(環境)カラー
  material.Ambient.r = 1.0f;
  material.Ambient.g = 1.0f;
  material.Ambient.b = 1.0f;
  material.Ambient.a = 1.0f;
  //	ディフューズ(素材の)カラー
  material.Diffuse.r = 1.0f;
  material.Diffuse.g = 1.0f;
  material.Diffuse.b = 1.0f;
  material.Diffuse.a = 1.0f;
  //	スペキュラー（テカり）カラー
  material.Specular.r = 1.0f;
  material.Specular.g = 1.0f;
  material.Specular.b = 1.0f;
  material.Specular.a = 1.0f;
  material.Power = 15.0f;
  //	エミッシブ（発光）
  material.Emissive.r = 0.0f;
  material.Emissive.g = 0.0f;
  material.Emissive.b = 0.0f;
  material.Emissive.a = 0.0f;
}

//****************************************************************
//
//	解放
//
//****************************************************************
FBXMESHAMG::~FBXMESHAMG()
{
  //	頂点情報解放
  delete[] Vertices;
  delete[] Indices;
  delete[] Weights;
  delete[] VerticesSrc;
  //	材質関連解放
  delete[] MaterialFaces;
  delete[] MeshMaterial;
  //	テクスチャ解放
  for(int i = 0; i < NumMesh; i++)
  {
    if(Textures[i] != NULL)
      //			Textures[i]->Release();
      ReleaseTexture(Textures[i]);

    if(NTextures[i] != NULL)
      //			NTextures[i]->Release();
      ReleaseTexture(NTextures[i]);
  }
  delete[] Textures;
  delete[] NTextures;
  //	モーション関連解放
  for(auto i = motion.begin(); i != motion.end(); i++)
  {
    Motion* M = &i->second;
    for(int bone = 0; bone < NumBone; bone++)
    {
      delete[] M->key[bone];
    }
  }
  motion.clear();
}

//****************************************************************
//
//	ファイル読み込み
//
//****************************************************************

void FBXMESHAMG::Create(IDirect3DDevice9 *d3d_device, const char* filename)
{
  //ファイル名を取り除く
  strcpy(FBXDir, filename);
  for(int n = strlen(FBXDir) - 1; n >= 0; n--)
  {
    if(FBXDir[n] == '/' || FBXDir[n] == '\\')
    {
      FBXDir[n + 1] = '\0';
      break;
    }
  }

  Load(d3d_device, filename);
  //  情報初期化
  Frame = 0;
}

void FBXMESHAMG::Load(IDirect3DDevice9 *d3d_device, const char* filename)
{
  FbxManager* manager = FbxManager::Create();
  FbxScene* scene = FbxScene::Create(manager, "");
  //	ファイルからシーンに読み込み
  FbxImporter* importer = FbxImporter::Create(manager, "");
  importer->Initialize(filename);
  importer->Import(scene);
  importer->Destroy();

  //	モーション情報取得
  FbxArray<FbxString*> names;
  scene->FillAnimStackNameArray(names);

  if(names != NULL)
  {
    //	モーションが存在するとき
    FbxTakeInfo* take = scene->GetTakeInfo(names[0]->Buffer());
    FbxLongLong start = take->mLocalTimeSpan.GetStart().Get();
    FbxLongLong stop = take->mLocalTimeSpan.GetStop().Get();
    FbxLongLong fps60 = FbxTime::GetOneFrameValue(FbxTime::eFrames60);
    StartFrame = (int)(start / fps60);
    motion["default"].NumFrame = (int)((stop - start) / fps60);
  }
  else
  {
    StartFrame = 0;
    motion["default"].NumFrame = 0;
  }

  //	モデルを材質ごとに分割
  FbxGeometryConverter fgc(manager);
  fgc.SplitMeshesPerMaterial(scene, true);
  fgc.Triangulate(scene, true);

  NumBone = 0;

  //	メッシュ数
  NumMesh = scene->GetSrcObjectCount<FbxMesh>();

  //	頂点数計算
  int work = 0;
  for(int m = 0; m < NumMesh; m++)
  {
    FbxMesh* mesh = scene->GetSrcObject<FbxMesh>(m);
    int num = mesh->GetPolygonVertexCount();
    work += num; // 合計頂点数
  }

  //	頂点確保
  Vertices = new PolygonVertex[work];
  Indices = new DWORD[work];
  Weights = new WEIGHT[work];

  NumVertices = 0;
  //	初期化
  for(int v = 0; v < work; v++)
  {
    Weights[v].count = 0;
  }

  //材質ごとのポリゴン頂点数
  MaterialFaces = new int[NumMesh];
  MeshMaterial = new int[NumMesh];
  Textures = new IDirect3DTexture9*[NumMesh];
  NTextures = new IDirect3DTexture9*[NumMesh];
  for(int m = 0; m < NumMesh; m++)
  {
    Textures[m] = NULL;
    NTextures[m] = NULL;
  }

  //	頂点読み込み
  for(int m = 0; m < NumMesh; m++)
  {
    FbxMesh* mesh = scene->GetSrcObject<FbxMesh>(m);
    int num = mesh->GetPolygonVertexCount();
    MaterialFaces[m] = num;

    //	メッシュの使用材質取得
    FbxLayerElementMaterial* LEM = mesh->GetElementMaterial();
    if(LEM != NULL)
    {
      //	ポリゴンに貼られている材質番号
      int material_index = LEM->GetIndexArray().GetAt(0);
      //	メッシュ材質のmaterial_index番目を取得
      FbxSurfaceMaterial* material = mesh->GetNode()->GetSrcObject<FbxSurfaceMaterial>(material_index);
      LoadMaterial(d3d_device, m, material);
    }
    //	使用材質設定
    MeshMaterial[m] = m;

    //	頂点情報読み込み
    LoadPosition(mesh);		//	座標読み込み

    //	インデックス設定(三角形ごと)
    int i = 0;
    for(/*int */i = 0; i < num; i += 3)
    {
      Indices[i + 0 + NumVertices] = i + 2 + NumVertices;
      Indices[i + 1 + NumVertices] = i + 1 + NumVertices;
      Indices[i + 2 + NumVertices] = i + 0 + NumVertices;
    }

    LoadNormal(mesh);		//	法線読み込み
    LoadUV(mesh);			//	テクスチャUV
    LoadVertexColor(mesh);	//	頂点カラー読み込み
    calcTangentBinormal(mesh);

    //	ボーン読み込み
    LoadBone(mesh);
    NumVertices += num;
  }

  //NumFaces = NumVertices / 3;
  //OptimizeVertices();

  //	頂点元データ保存
  VerticesSrc = new PolygonVertex[NumVertices];
  memcpy(VerticesSrc, Vertices, sizeof(PolygonVertex)*NumVertices);

  //	ウェイト正規化
  // ５本以上にまたっがてる場合のため
  for(int v = 0; v < NumVertices; v++)
  {
    float n = 0;
    //	頂点のウェイトの合計値
    for(int w = 0; w < Weights[v].count; w++)
    {
      n += Weights[v].weight[w];
    }
    //	正規化
    for(int w = 0; w < Weights[v].count; w++)
    {
      Weights[v].weight[w] /= n;
    }
  }

  //	解放
  scene->Destroy();
  manager->Destroy();

  //Play("default");

  WriteOutLog("loadXFile 成功 ファイル名 : %s", filename);
}

//****************************************************************
//
//	頂点情報読み込み
//
//****************************************************************
//------------------------------------------------
//	座標読み込み
//------------------------------------------------
void FBXMESHAMG::LoadPosition(FbxMesh* mesh)
{
  int* index = mesh->GetPolygonVertices();
  FbxVector4* source = mesh->GetControlPoints();
  // メッシュのトランスフォーム
  FbxVector4 T = mesh->GetNode()->GetGeometricTranslation(FbxNode::eSourcePivot);
  FbxVector4 R = mesh->GetNode()->GetGeometricRotation(FbxNode::eSourcePivot);
  FbxVector4 S = mesh->GetNode()->GetGeometricScaling(FbxNode::eSourcePivot);
  FbxAMatrix TRS = FbxAMatrix(T, R, S);
  //	全頂点変換
  for(int v = 0; v < mesh->GetControlPointsCount(); v++)
  {
    source[v] = TRS.MultT(source[v]);
  }

  // 頂点座標読み込み
  int num = mesh->GetPolygonVertexCount();
  for(int v = 0; v < num; v++)
  {
    int vindex = index[v];

    Vertices[v + NumVertices].x = (float)-source[vindex][0];
    Vertices[v + NumVertices].y = (float)source[vindex][1];
    Vertices[v + NumVertices].z = (float)source[vindex][2];
    Vertices[v + NumVertices].w = 1.0f;

    Vertices[v + NumVertices].tu = 0;
    Vertices[v + NumVertices].tv = 0;
    //Vertices[v + NumVertices].color_ = 0xFFFFFFFF;
  }
}

//------------------------------------------------
//	法線読み込み
//------------------------------------------------
void FBXMESHAMG::LoadNormal(FbxMesh* mesh)
{
  FbxArray<FbxVector4> normal;
  D3DXVECTOR3 vNormal;
  mesh->GetPolygonVertexNormals(normal);
  for(int v = 0; v < normal.Size(); v++)
  {
    vNormal.x = -(float)normal[v][0];
    vNormal.y = (float)normal[v][1];
    vNormal.z = (float)normal[v][2];
    D3DXVec3Normalize(&vNormal, &vNormal);
    Vertices[v + NumVertices].nx = vNormal.x;
    Vertices[v + NumVertices].ny = vNormal.y;
    Vertices[v + NumVertices].nz = vNormal.z;
  }
}

//------------------------------------------------
//	ＵＶ読み込み
//------------------------------------------------
void FBXMESHAMG::LoadUV(FbxMesh* mesh)
{
  FbxStringList names;
  mesh->GetUVSetNames(names);
  FbxArray<FbxVector2> uv;
  mesh->GetPolygonVertexUVs(names.GetStringAt(0), uv);
  for(int v = 0; v < uv.Size(); v++)
  {
    Vertices[v + NumVertices].tu = (float)(uv[v][0]);
    Vertices[v + NumVertices].tv = (float)(1.0 - uv[v][1]);
  }
}

//------------------------------------------------
//	頂点カラー読み込み
//------------------------------------------------
void FBXMESHAMG::LoadVertexColor(FbxMesh* mesh)
{

  int vColorLayerCount = mesh->GetElementVertexColorCount();
  if(mesh->GetElementVertexColorCount() <= 0) return;
  //    頂点カラーレイヤー取得
  FbxGeometryElementVertexColor* element = mesh->GetElementVertexColor(0);

  //  保存形式の取得
  FbxGeometryElement::EMappingMode mapmode = element->GetMappingMode();
  FbxGeometryElement::EReferenceMode refmode = element->GetReferenceMode();

  //    ポリゴン頂点に対するインデックス参照形式のみ対応
  if(mapmode == FbxGeometryElement::eByPolygonVertex)
  {
    if(refmode == FbxGeometryElement::eIndexToDirect)
    {
      FbxLayerElementArrayTemplate<int>* index = &element->GetIndexArray();
      int indexCount = index->GetCount();
      for(int j = 0; j < indexCount; j++)
      {
        // FbxColor取得
        FbxColor c = element->GetDirectArray().GetAt(index->GetAt(j));
        // DWORD型のカラー作成        
        DWORD color = ((DWORD)(c.mAlpha * 255) << 24) + ((DWORD)(c.mRed * 255) << 16) + ((DWORD)(c.mGreen * 255) << 8) + ((DWORD)(c.mBlue * 255));
        //Vertices[j + NumVertices].color_ = color_;
      }
    }
  }
}

//------------------------------------------------
//  接線,従法線計算
//------------------------------------------------
void FBXMESHAMG::calcTangentBinormal(FbxMesh* mesh)
{
  int vertexNum = mesh->GetPolygonVertexCount();
  int polygonNum = vertexNum / 3;
  for(int poly = 0; poly < polygonNum; poly++)
  {
    D3DXVECTOR3 p0 = D3DXVECTOR3(Vertices[poly * 3 + 0].x, Vertices[poly * 3 + 0].y, Vertices[poly * 3 + 0].z);
    D3DXVECTOR3 p1 = D3DXVECTOR3(Vertices[poly * 3 + 1].x, Vertices[poly * 3 + 1].y, Vertices[poly * 3 + 1].z);
    D3DXVECTOR3 p2 = D3DXVECTOR3(Vertices[poly * 3 + 2].x, Vertices[poly * 3 + 2].y, Vertices[poly * 3 + 2].z);

    D3DXVECTOR2 uv0 = D3DXVECTOR2(Vertices[poly * 3 + 0].tu,Vertices[poly * 3 + 0].tv);
    D3DXVECTOR2 uv1 = D3DXVECTOR2(Vertices[poly * 3 + 1].tu,Vertices[poly * 3 + 1].tv);
    D3DXVECTOR2 uv2 = D3DXVECTOR2(Vertices[poly * 3 + 2].tu,Vertices[poly * 3 + 2].tv);

    //接線と従法線の格納場所
    D3DXVECTOR3 tangent;
    D3DXVECTOR3 binormal;

    //接線と従法線を求める
    CalcTangentAndBinormal(&p0, &uv0, &p1, &uv1, &p2, &uv2, &tangent, &binormal);

    //各頂点に接線と従法線を格納
    for(int i = 0; i < 3; i++)
    {
      Vertices[poly * 3 + i].tx = tangent.x;
      Vertices[poly * 3 + i].ty = tangent.y;
      Vertices[poly * 3 + i].tz = tangent.z;
      Vertices[poly * 3 + i].bx = binormal.x;
      Vertices[poly * 3 + i].bz = binormal.z;
      Vertices[poly * 3 + i].by = binormal.y;
    }
  }
}

//****************************************************************
//	材質読み込み
//****************************************************************
void FBXMESHAMG::LoadMaterial(IDirect3DDevice9 *d3d_device, int index, FbxSurfaceMaterial * material)
{
  FbxProperty prop = material->FindProperty(FbxSurfaceMaterial::sDiffuse);

  //	テクスチャ読み込み
  const char* path = NULL;
  int fileTextureCount = prop.GetSrcObjectCount<FbxFileTexture>();
  if(fileTextureCount > 0)
  {
    FbxFileTexture* FileTex = prop.GetSrcObject<FbxFileTexture>(0);
    path = FileTex->GetFileName();
  }
  else
  {
    int numLayer = prop.GetSrcObjectCount<FbxLayeredTexture>();
    if(numLayer > 0)
    {
      FbxLayeredTexture* LayerTex = prop.GetSrcObject<FbxLayeredTexture>(0);
      FbxFileTexture* FileTex = LayerTex->GetSrcObject<FbxFileTexture>(0);
      path = FileTex->GetFileName();
    }
  }
  if(path == NULL) return;

  //  C:\\AAA\\BBB\\a.fbx  C:/AAA/BBB/a.fbx
  const char* name = &path[strlen(path)];
  for(int i = 0; i < (int)strlen(path); i++)
  {
    name--;
    if(name[0] == '/') { name++; break; }
    if(name[0] == '\\') { name++; break; }
  }
  char work[128];
  strcpy(work, FBXDir);		//"AAA/BBB/";
  //strcat(work, "texture/");	//"AAA/BBB/texture/"
  strcat(work, name);			//"AAA/BBB/texture/a.png

  char filename[128];
  strcpy(filename, work);
  //D3DXCreateTextureFromFile(d3d_device, filename, &Textures[index] );
  Textures[index] = LoadTexture(d3d_device, filename);

  //	法線マップ読み込み
  strcpy(work, FBXDir);		//"AAA/BBB/";
  strcat(work, "N");	//"AAA/BBB/texture/N"
  strcat(work, name);			//"AAA/BBB/texture/Na.png
  strcpy(filename, work);
  //D3DXCreateTextureFromFile(d3d_device, filename, &NTextures[index]);
  NTextures[index] = LoadTexture(d3d_device, filename);

}

//****************************************************************
//	ボーン検索
//****************************************************************
int FBXMESHAMG::FindBone(const char* name)
{
  int bone = -1; // 見つからない
  for(int i = 0; i < NumBone; i++)
  {
    if(strcmp(name, Bone[i].Name) == 0)
    {
      bone = i;
      break;
    }
  }
  return bone;
}

void FBXMESHAMG::LoadBone(FbxMesh* mesh)
{
  //	メッシュ頂点数
  int num = mesh->GetPolygonVertexCount();

  //	スキン情報の有無
  int skinCount = mesh->GetDeformerCount(FbxDeformer::eSkin);
  if(skinCount <= 0)
  {
    LoadMeshAnim(mesh);
    return;
  }
  FbxSkin* skin = static_cast<FbxSkin*>(mesh->GetDeformer(0, FbxDeformer::eSkin));
  //	ボーン数
  int nBone = skin->GetClusterCount();
  //	全ボーン情報取得
  for(int bone = 0; bone < nBone; bone++)
  {
    //	ボーン情報取得
    FbxCluster* cluster = skin->GetCluster(bone);
    FbxAMatrix trans;
    cluster->GetTransformMatrix(trans);
    trans.mData[0][1] *= -1;
    trans.mData[0][2] *= -1;
    trans.mData[1][0] *= -1;
    trans.mData[2][0] *= -1;
    trans.mData[3][0] *= -1;

    //	ボーン名取得
    const char* name = cluster->GetLink()->GetName();

    //	ボーン検索
    bool isNewBone = false;
    int bone_no = FindBone(name);
    if(bone_no < 0)
    {
      bone_no = NumBone;
      NumBone++;
      isNewBone = true;
    }
    if(isNewBone)
    {
      strcpy(Bone[bone_no].Name, name);
      //	オフセット行列作成
      FbxAMatrix LinkMatrix;
      cluster->GetTransformLinkMatrix(LinkMatrix);
      LinkMatrix.mData[0][1] *= -1;
      LinkMatrix.mData[0][2] *= -1;
      LinkMatrix.mData[1][0] *= -1;
      LinkMatrix.mData[2][0] *= -1;
      LinkMatrix.mData[3][0] *= -1;

      FbxAMatrix Offset = LinkMatrix.Inverse() * trans;
      FbxDouble* OffsetM = (FbxDouble*)Offset;

      //	オフセット行列保存
      for(int i = 0; i < 16; i++)
      {
        Bone[bone_no].OffsetMatrix.m[i / 4][i % 4] = (float)OffsetM[i];
      }

      //	キーフレーム読み込み
      LoadKeyFrames("default", bone_no, cluster->GetLink());
    }

    //	ウェイト読み込み
    int wgtcount = cluster->GetControlPointIndicesCount();
    int* wgtindex = cluster->GetControlPointIndices();
    double* wgt = cluster->GetControlPointWeights();

    int* index = mesh->GetPolygonVertices();

    for(int i = 0; i < wgtcount; i++)
    {
      int wgtindex2 = wgtindex[i];
      //	全ポリゴンからwgtindex2番目の頂点検索
      for(int v = 0; v < num; v++)
      {
        if(index[v] != wgtindex2) continue;
        //	頂点にウェイト保存
        int w = Weights[v + NumVertices].count;
        if(w >= 4)
        {
          continue;
        }
        Weights[v + NumVertices].bone[w] = bone_no;
        Weights[v + NumVertices].weight[w] = (float)wgt[i];
        Weights[v + NumVertices].count++;
      }
    }
  }
}

//	ボーンのないメッシュのアニメーション
void FBXMESHAMG::LoadMeshAnim(FbxMesh* mesh)
{
  FbxNode* node = mesh->GetNode();

  int bone_no = NumBone;
  strcpy(Bone[bone_no].Name, node->GetName());

  //	オフセット行列（原点に移動させる行列）
  D3DXMatrixIdentity(
    &Bone[bone_no].OffsetMatrix
  );

  //	キーフレーム読み込み
  LoadKeyFrames("default", bone_no, node);

  //	ウェイト設定
  int num = mesh->GetPolygonVertexCount();
  for(int i = 0; i < num; i++)
  {
    Weights[i + NumVertices].bone[0] = bone_no;
    Weights[i + NumVertices].weight[0] = 1.0f;
    Weights[i + NumVertices].count = 1;
  }

  NumBone++;
}

//	キーフレーム読み込み
void FBXMESHAMG::LoadKeyFrames(std::string name, int bone, FbxNode* bone_node)
{
  //	メモリ確保
  Motion* M = &motion[name];
  M->key[bone] =
    new D3DXMATRIX[M->NumFrame + 1];

  double time = StartFrame * (1.0 / 60);
  FbxTime T;
  for(int f = 0; f < motion[name].NumFrame; f++)
  {
    T.SetSecondDouble(time);
    //	T秒の姿勢行列をGet
    FbxMatrix m = bone_node->EvaluateGlobalTransform(T);
    m.mData[0][1] *= -1;// _12
    m.mData[0][2] *= -1;// _13
    m.mData[1][0] *= -1;// _21
    m.mData[2][0] *= -1;// _31
    m.mData[3][0] *= -1;// _41

    FbxDouble* mat = (FbxDouble*)m;
    for(int i = 0; i < 16; i++)
    {
      motion[name].key[bone][f].m[i / 4][i % 4] = (float)mat[i];
    }

    time += 1.0 / 60.0;
  }
}


//	ボーン行列の補間
static void MatrixInterporate(D3DXMATRIX& out, D3DXMATRIX& A, D3DXMATRIX B, float rate)
{
  out = A * (1.0f - rate) + B * rate;
}

void FBXMESHAMG::Skinning(ANIMATION_DATA* animationData)
{
  if(animationData == NULL)
    return;
  Motion* M = &motion[animationData->motionName];
  if(M == NULL) return;

  //	配列用変数
  int f = (int)animationData->frame;
  // 行列準備
  D3DXMATRIX KeyMatrix[256];
  for(int b = 0; b < NumBone; b++)
  {
    //	行列補間
    D3DXMATRIX m;
    MatrixInterporate(m, M->key[b][f], M->key[b][f + 1], animationData->frame - (int)animationData->frame);
    Bone[b].transform = m;
    // キーフレーム
    KeyMatrix[b] = Bone[b].OffsetMatrix * m;
  }

  // 頂点変形
  for(int v = 0; v < NumVertices; v++)
  {
    // 頂点 * ボーン行列
    // b = v番目の頂点の影響ボーン[n]
    if(Weights[v].count <= 0) continue;

    Vertices[v].x = 0;
    Vertices[v].y = 0;
    Vertices[v].z = 0;

    // 影響個数分ループ
    for(int n = 0; n < Weights[v].count; n++)
    {
      int b = Weights[v].bone[n];

      float x = VerticesSrc[v].x;
      float y = VerticesSrc[v].y;
      float z = VerticesSrc[v].z;
      // 座標を影響力分移動
      Vertices[v].x += (x*KeyMatrix[b]._11 + y * KeyMatrix[b]._21 + z * KeyMatrix[b]._31 + 1 * KeyMatrix[b]._41)*Weights[v].weight[n];
      Vertices[v].y += (x*KeyMatrix[b]._12 + y * KeyMatrix[b]._22 + z * KeyMatrix[b]._32 + 1 * KeyMatrix[b]._42)*Weights[v].weight[n];
      Vertices[v].z += (x*KeyMatrix[b]._13 + y * KeyMatrix[b]._23 + z * KeyMatrix[b]._33 + 1 * KeyMatrix[b]._43)*Weights[v].weight[n];

      float nx = VerticesSrc[v].nx;
      float ny = VerticesSrc[v].ny;
      float nz = VerticesSrc[v].nz;
      // 法線を影響力分変換
      Vertices[v].nx += (nx*KeyMatrix[b]._11 + ny * KeyMatrix[b]._21 + nz * KeyMatrix[b]._31)*Weights[v].weight[n];
      Vertices[v].ny += (nx*KeyMatrix[b]._12 + ny * KeyMatrix[b]._22 + nz * KeyMatrix[b]._32)*Weights[v].weight[n];
      Vertices[v].nz += (nx*KeyMatrix[b]._13 + ny * KeyMatrix[b]._23 + nz * KeyMatrix[b]._33)*Weights[v].weight[n];

      float bx = VerticesSrc[v].bx;
      float by = VerticesSrc[v].by;
      float bz = VerticesSrc[v].bz;
      // 従法線を影響力分変換
      Vertices[v].bx += (bx*KeyMatrix[b]._11 + by * KeyMatrix[b]._21 + bz * KeyMatrix[b]._31)*Weights[v].weight[n];
      Vertices[v].by += (bx*KeyMatrix[b]._12 + by * KeyMatrix[b]._22 + bz * KeyMatrix[b]._32)*Weights[v].weight[n];
      Vertices[v].bz += (bx*KeyMatrix[b]._13 + by * KeyMatrix[b]._23 + bz * KeyMatrix[b]._33)*Weights[v].weight[n];

      float tx = VerticesSrc[v].tx;
      float ty = VerticesSrc[v].ty;
      float tz = VerticesSrc[v].tz;
      // 接線を影響力分変換
      Vertices[v].tx += (tx*KeyMatrix[b]._11 + ty * KeyMatrix[b]._21 + tz * KeyMatrix[b]._31)*Weights[v].weight[n];
      Vertices[v].ty += (tx*KeyMatrix[b]._12 + ty * KeyMatrix[b]._22 + tz * KeyMatrix[b]._32)*Weights[v].weight[n];
      Vertices[v].tz += (tx*KeyMatrix[b]._13 + ty * KeyMatrix[b]._23 + tz * KeyMatrix[b]._33)*Weights[v].weight[n];

      // 各成分の正規化
      D3DXVECTOR3 normal   = D3DXVECTOR3(Vertices[v].nx, Vertices[v].ny, Vertices[v].nz);
      D3DXVECTOR3 tangent  = D3DXVECTOR3(Vertices[v].tx, Vertices[v].ty, Vertices[v].tz);
      D3DXVECTOR3 binormal = D3DXVECTOR3(Vertices[v].bx, Vertices[v].by, Vertices[v].bz);
      D3DXVec3Normalize(&normal, &normal);
      D3DXVec3Normalize(&tangent, &tangent);
      D3DXVec3Normalize(&binormal, &binormal);
      Vertices[v].nx = normal.x;
      Vertices[v].ny = normal.y;
      Vertices[v].nz = normal.z;
      Vertices[v].tx = tangent.x;
      Vertices[v].ty = tangent.y;
      Vertices[v].tz = tangent.z;
      Vertices[v].bx = binormal.x;
      Vertices[v].by = binormal.y;
      Vertices[v].bz = binormal.z;

    }
  }
}

void FBXMESHAMG::AddMotion(std::string name, const char * filename)
{
  FbxManager* manager = FbxManager::Create();
  FbxScene* scene = FbxScene::Create(manager, "");
  //	ファイルからシーンに読み込み
  FbxImporter* importer = FbxImporter::Create(manager, "");
  importer->Initialize(filename);
  importer->Import(scene);
  importer->Destroy();

  //	モーション情報取得
  FbxArray<FbxString*> names;
  scene->FillAnimStackNameArray(names);

  int n = names.Size();
  const char *anim_name = names[0]->Buffer();

  FbxTakeInfo* take = scene->GetTakeInfo(anim_name);
  FbxLongLong start = take->mLocalTimeSpan.GetStart().Get();
  FbxLongLong stop = take->mLocalTimeSpan.GetStop().Get();
  FbxLongLong fps60 = FbxTime::GetOneFrameValue(FbxTime::eFrames60);

  StartFrame = (int)(start / fps60);
  motion[name].NumFrame = (int)((stop - start) / fps60);
  //	ルートノード取得
  FbxNode* root = scene->GetRootNode();

  //	全ボーン読み込み
  for(int b = 0; b < NumBone; b++)
  {
    //	ボーンノード検索
    FbxNode* bone = root->FindChild(Bone[b].Name);
    if(bone == NULL) continue;
    //	キーフレーム読み込み
    LoadKeyFrames(name, b, bone);
  }
  //	解放
  scene->Destroy();
  manager->Destroy();
}

// レイ判定
// pos_から(pos_+vec)にレイを発射 交点=out 面法線=normal
// 当たらなければ-1がかえる
int FBXMESHAMG::Raycast(const D3DXVECTOR3& pos, const D3DXVECTOR3& vec, float Dist, D3DXVECTOR3& out, D3DXVECTOR3& normal)
{
  int ret = -1;
  int NumFace = NumVertices / 3;

  D3DXVECTOR3 v1, v2, v3;
  D3DXVECTOR3 N;
  D3DXVECTOR3 line1, line2, line3;
  D3DXVECTOR3 temp;
  D3DXVECTOR3 cp;

  D3DXVECTOR3 p;
  D3DXVECTOR3 p1, p2, p3;

  float neart = Dist;

  // 全ポリゴンループ
  for(int face = 0; face < NumFace; face++)
  {
    // 面頂点取得
    int a = Indices[face * 3 + 0];
    v1.x = Vertices[a].x;	v1.y = Vertices[a].y;	v1.z = Vertices[a].z;

    int b = Indices[face * 3 + 1];
    v2.x = Vertices[b].x;	v2.y = Vertices[b].y;	v2.z = Vertices[b].z;

    int c = Indices[face * 3 + 2];
    v3.x = Vertices[c].x;	v3.y = Vertices[c].y;	v3.z = Vertices[c].z;

    // 3辺
    line1 = v2 - v1; // v1 -> v2
    line2 = v3 - v2; // v2 -> v3
    line3 = v1 - v3; // v3 -> v1

    // 面法線(２辺の外積)
    D3DXVec3Cross(&N, &line1, &line2);

    // (pos_->1点)を法線に射影(≒高さ)
    p = v1 - pos;
    float tp = D3DXVec3Dot(&p, &N);
    // レイを法線に射影
    float tv = D3DXVec3Dot(&vec, &N);
    // 表裏判定
    if(tv >= 0) continue;

    // 交点算出
    float t = tp / tv;
    cp = vec * t + pos;

    // 反対はNG
    if(t < 0) continue;
    // 一番近い点のみ
    if(t > neart) continue;

    // 内点判定
    p1 = v1 - cp; //交点->1点
    D3DXVec3Cross(&temp, &p1, &line1);
    // 反対向き(±90°以上)ならば外
    if(D3DXVec3Dot(&temp, &N) < 0)
      continue;

    p2 = v2 - cp; //交点->1点
    D3DXVec3Cross(&temp, &p2, &line2);
    // 反対向き(±90°以上)ならば外
    if(D3DXVec3Dot(&temp, &N) < 0)
      continue;

    p3 = v3 - cp; //交点->1点
    D3DXVec3Cross(&temp, &p3, &line3);
    // 反対向き(±90°以上)ならば外
    if(D3DXVec3Dot(&temp, &N) < 0)
      continue;

    // 情報設定
    out = cp;
    normal = N;
    ret = face;
    neart = t;
  }
  // 法線正規化
  D3DXVec3Normalize(&normal, &normal);

  return	ret;
}


//****************************************************************
//	頂点最適化
//****************************************************************
void FBXMESHAMG::OptimizeVertices()
{
  int currentNum = 0;
  for(int v = 0; v < NumVertices; v++)
  {
    int sameIndex = -1;
    //	同一頂点検索
    for(int old = 0; old < currentNum; old++)
    {
      if(Vertices[v].x != Vertices[old].x) continue;
      if(Vertices[v].y != Vertices[old].y) continue;
      if(Vertices[v].z != Vertices[old].z) continue;
      if(Vertices[v].nx != Vertices[old].nx) continue;
      if(Vertices[v].ny != Vertices[old].ny) continue;
      if(Vertices[v].nz != Vertices[old].nz) continue;
      if(Vertices[v].tu != Vertices[old].tu) continue;
      if(Vertices[v].tv != Vertices[old].tv) continue;
      //if (Vertices[v].color_ != Vertices[old].color_) continue;

      sameIndex = old;
      break;
    }

    int target = v;
    if(sameIndex == -1)
    {
      //	新規頂点
      CopyMemory(&Vertices[currentNum], &Vertices[v], sizeof(PolygonVertex));
      CopyMemory(&Weights[currentNum], &Weights[v], sizeof(WEIGHT));
      target = currentNum;
      currentNum++;
    }
    else
    {
      target = sameIndex;
    }
    //	インデックス更新
    for(int i = 0; i < NumVertices; i++)
    {
      if(Indices[i] == v) Indices[i] = target;
    }
  }

  //	新バッファ確保
  PolygonVertex* buf = new PolygonVertex[currentNum];
  CopyMemory(buf, Vertices, sizeof(PolygonVertex) * currentNum);
  NumVertices = currentNum;

  delete[] Vertices;
  Vertices = buf;
}

// 3頂点とUV値から指定座標でのU軸（Tangent）及びV軸（Binormal）を算出
//
// p0, p1, p2    : ローカル空間での頂点座標（ポリゴン描画順）
// uv0, uv1, uv2 : 各頂点のUV座標
// outTangent    : U軸（Tangent）出力
// outBinormal   : V軸（Binormal）出力
void FBXMESHAMG::CalcTangentAndBinormal(D3DXVECTOR3* p0, D3DXVECTOR2* uv0,
                                        D3DXVECTOR3* p1, D3DXVECTOR2* uv1,
                                        D3DXVECTOR3* p2, D3DXVECTOR2* uv2,
                                        D3DXVECTOR3* outTangent, D3DXVECTOR3* outBinormal)
{
  // 5次元→3次元頂点に
  D3DXVECTOR3 CP0[3] = {
    D3DXVECTOR3(p0->x, uv0->x, uv0->y),
    D3DXVECTOR3(p0->y, uv0->x, uv0->y),
    D3DXVECTOR3(p0->z, uv0->x, uv0->y),
  };
  D3DXVECTOR3 CP1[3] = {
    D3DXVECTOR3(p1->x, uv1->x, uv1->y),
    D3DXVECTOR3(p1->y, uv1->x, uv1->y),
    D3DXVECTOR3(p1->z, uv1->x, uv1->y),
  };
  D3DXVECTOR3 CP2[3] = {
    D3DXVECTOR3(p2->x, uv2->x, uv2->y),
    D3DXVECTOR3(p2->y, uv2->x, uv2->y),
    D3DXVECTOR3(p2->z, uv2->x, uv2->y),
  };

  // 平面パラメータからUV軸座標算出
  float U[3], V[3];
  for(int i = 0; i < 3; ++i)
  {
    D3DXVECTOR3 V1 = CP1[i] - CP0[i];
    D3DXVECTOR3 V2 = CP2[i] - CP1[i];
    D3DXVECTOR3 ABC;
    D3DXVec3Cross(&ABC, &V1, &V2);

    if(ABC.x == 0.0f)
    {
      //MessageBox(NULL, "ポリゴンかUV上のポリゴンが縮退してます！", NULL, NULL);
      memset(outTangent, 0, sizeof(D3DXVECTOR3));
      memset(outBinormal, 0, sizeof(D3DXVECTOR3));
      return;
    }
    U[i] = -ABC.y / ABC.x;
    V[i] = -ABC.z / ABC.x;
  }

  memcpy(outTangent, U, sizeof(float) * 3);
  memcpy(outBinormal, V, sizeof(float) * 3);

  // 正規化
  D3DXVec3Normalize(outTangent, outTangent);
  D3DXVec3Normalize(outBinormal, outBinormal);
}