#include <d3dx9.h>
#include <stdint.h>
#include "app.h"
#include "math.h"
#include "../../00_application/app/app.h"
#include "../../01_data/model/loadModel.h"
#include "../../01_data/texture/loadTexture.h"
#include "../../01_data/sound/loadSound.h"
#include "../../01_data/status/loadStatus.h"
#include "../../03_update/update.h"
#include "../../06_outGame/title/title.h"
#include "../../06_outGame/opening/opening.h"
#include "../../06_outGame/gameover/gameover.h"
#include "../../06_outGame/clear/clear.h"
#include "../../06_outGame/staffroll/staffroll.h"

//ゲーム情報
struct GAME g_game;

//fps値計算用
static int g_oldTime;    //前フレームのtimeGetTimeの値
static int g_nowTime;    //現在フレームのtimeGetTimeの値
static float g_fps;      //上二つから求めたfps値
static float g_speedRate;//60fpsを基準としたゲーム速度修正用倍率

//ゲーム継続flag
bool g_isContinueGame = true;

/// <summary>
/// アプリケーションの初期化
/// </summary>
void AppInit()
{
  LoadStatus();//config.txtからの詳細値読み込み
  g_game.gameState_ = GAME_STATE_TITLE;
  g_game.gameMode_ = GAMEMODE_NORMAL;
  InitMeshHandles();    //メッシュモデル、モーション読み込み
  InitTextureHandles(); //2Dテクスチャ読み込み
  InitSoundHandles();   //音声読み込み
}

/// <summary>
/// fps値のチェック(1fに一回のみ呼び出すこと)
/// </summary>
/// <returns>fps値</returns>
float CheckFPS()
{
  g_oldTime = g_nowTime;
  g_nowTime = timeGetTime();
  //fpsを求める
  float advanceTime = (float)(g_nowTime - g_oldTime);
  if(advanceTime != 0)
  {
    g_fps = 1.0f / advanceTime * 1000.0f;
    g_speedRate = 60.0f / g_fps;
  }
  return g_fps;
}

/// <summary>
/// fps値の取得(checkFPS後はこちらでFPS値を取得すること)
/// </summary>
/// <returns>fps値</returns>
float GetFPS()
{
  return g_fps;
}

/// <summary>
/// ゲーム速度修正用倍率の取得
/// </summary>
/// <returns>ゲーム速度修正用倍率</returns>
float GetSpeedRate()
{
  return g_speedRate;
}

/// <summary>
/// ゲームの状態を遷移する
/// </summary>
/// <param name="gameState_">次に決めたいゲーム状態(GAME_STATE参照)</param>
void ChangeGameState(int gameState)
{
  //gameState_の値が不正な場合何もしない
  if(0 <= gameState && gameState < GAME_STATE_MAX)
  {
    g_game.gameState_ = gameState;
  }

  //BGMの停止
  StopBGM();

  //各状態の初期化
  switch(gameState)
  {
  case GAME_STATE_TITLE:
    InitTitle();
    break;
  case GAME_STATE_OPENING:
    InitOpening();
    break;
  case GAME_STATE_INGAME:
    InitInGame();
    break;
  case GAME_STATE_GAMEOVER:
    InitGameover();
    break;
  case GAME_STATE_CLEAR:
    InitClear();
    break;
  case GAME_STATE_STAFFROLL:
    InitStaffroll();
    break;
  case GAME_STATE_END:
    g_isContinueGame = false;
    break;
  }
}

/// <summary>
/// ゲームの状態を遷移する
/// </summary>
/// <param name="gameMode_">次に決めたいゲームモード(GAME_MODE参照)</param>
void ChangeGameMode(int gameMode)
{
  //gameState_の値が不正な場合何もしない
  if(0 <= gameMode && gameMode < GAMEMODE_MAX)
  {
    g_game.gameMode_ = (GAMEMODE)gameMode;
  }
}

/// <summary>
/// ゲーム状態の取得
/// </summary>
/// <returns>現在のゲーム状態</returns>
int GetGameState()
{
  return g_game.gameState_;
}

/// <summary>
/// ゲームモードの取得
/// </summary>
/// <returns>現在のゲームモード</returns>
int GetGameMode()
{
  return g_game.gameMode_;
}