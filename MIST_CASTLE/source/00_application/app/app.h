#pragma once
#include <d3dx9.h>

//デバッグモードにする(非デバッグ時は定数をコメントアウト)
#define DEBUG_MODE

#define DISP_W (1280)//ディスプレイ横幅
#define DISP_H (720) //ディスプレイ縦幅
#define APP_NAME ("MistCastle")//アプリケーション名
#define GRAVITY_ACCELE (0.5f)    //重力加速度

///(maxマクロは大きいほうを取り出し、minマクロは小さいほうを取り出すマクロであるため注意)
#define CLUMP(Min,n,Max) (max(Min,min(n,Max))) //nの値をMinからMAXの間に制限する

/// <summary>
/// ゲームシーン列挙
/// </summary>
enum GAME_STATE
{
  GAME_STATE_TITLE,
  GAME_STATE_OPENING,
  GAME_STATE_INGAME,
  GAME_STATE_GAMEOVER,
  GAME_STATE_CLEAR,
  GAME_STATE_STAFFROLL,
  GAME_STATE_END,

  GAME_STATE_MAX,
};

/// <summary>
/// ゲームモード列挙
/// </summary>
enum GAMEMODE
{
  GAMEMODE_NORMAL,
  GAMEMODE_HEALTH_INFINITY,
  GAMEMODE_DEBUG,
  GAMEMODE_DEBUG_HIT_JUDGE,
  GAMEMODE_MAP_CREATE,

  GAMEMODE_MAX,
};

//ゲーム情報構造体
struct GAME
{
  int gameState_;
  GAMEMODE gameMode_;
};

//ゲーム継続flag
extern bool g_isContinueGame;

//アプリケーションの初期化
void AppInit();

//fps値のチェック(1fに一回のみ呼び出すこと)
float CheckFPS();

//fps値の取得(checkFPS後はこちらでFPS値を取得すること)
float GetFPS();

//ゲーム速度修正用倍率の取得
float GetSpeedRate();

//ゲームの状態を遷移する(enum GAME_STATEを参照)
void ChangeGameState(int gameState_);

//ゲームモードを遷移する(enum GAMEMODEを参照)
void ChangeGameMode(int gameMode);

//ゲームの状態の取得
int GetGameState();

//ゲームモードの取得
int GetGameMode();