#include "input.h"

//キーボード入力情報
static struct KEY_BOARD   g_oldKey;
static struct KEY_BOARD   g_nowKey;
//マウス入力情報
static struct MOUSE       g_oldMouse;
static struct MOUSE       g_nowMouse;
//パッド入力情報
static struct JOY_STICK   g_oldJoyStick;
static struct JOY_STICK   g_nowJoyStick;

/// <summary>
/// 入力情報の更新
/// </summary>
void InputUpdate()
{
  g_oldKey = g_nowKey;
  g_nowKey = GetKeyBoardState();
  g_oldMouse = g_nowMouse;
  g_nowMouse = GetMouseState();
  g_oldJoyStick = g_nowJoyStick;
  g_nowJoyStick = GetJoyStickState();
  //SetCursorPos(640, 360);
  ShowCursor(false);
}

/// <summary>
/// キーボードの情報の取得
/// </summary>
/// <returns>キーボードの情報</returns>
KEY_BOARD GetKeyBoard()
{
  return g_nowKey;
}

/// <summary>
/// キーボードを押した瞬間の情報の取得
/// </summary>
/// <returns>キーボードの情報</returns>
KEY_BOARD GetTrgInKeyBoard()
{
  KEY_BOARD result = {0};
  result.esc     = g_nowKey.esc    > g_oldKey.esc;
  result.num0    = g_nowKey.num0   > g_oldKey.num0;
  result.num1    = g_nowKey.num1   > g_oldKey.num1;
  result.num2    = g_nowKey.num2   > g_oldKey.num2;
  result.num3    = g_nowKey.num3   > g_oldKey.num3;
  result.num4    = g_nowKey.num4   > g_oldKey.num4;
  result.num5    = g_nowKey.num5   > g_oldKey.num5;
  result.num6    = g_nowKey.num6   > g_oldKey.num6;
  result.num7    = g_nowKey.num7   > g_oldKey.num7;
  result.num8    = g_nowKey.num8   > g_oldKey.num8;
  result.num9    = g_nowKey.num9   > g_oldKey.num9;
  result.numPad0 = g_nowKey.numPad0> g_oldKey.numPad0;
  result.numPad1 = g_nowKey.numPad1> g_oldKey.numPad1;
  result.numPad2 = g_nowKey.numPad2> g_oldKey.numPad2;
  result.numPad3 = g_nowKey.numPad3> g_oldKey.numPad3;
  result.numPad4 = g_nowKey.numPad4> g_oldKey.numPad4;
  result.numPad5 = g_nowKey.numPad5> g_oldKey.numPad5;
  result.numPad6 = g_nowKey.numPad6> g_oldKey.numPad6;
  result.numPad7 = g_nowKey.numPad7> g_oldKey.numPad7;
  result.numPad8 = g_nowKey.numPad8> g_oldKey.numPad8;
  result.numPad9 = g_nowKey.numPad9> g_oldKey.numPad9;
  result.plus    = g_nowKey.plus   > g_oldKey.plus;
  result.minus   = g_nowKey.minus  > g_oldKey.minus;
  result.slash   = g_nowKey.slash  > g_oldKey.slash;
  result.asterisk= g_nowKey.asterisk  > g_oldKey.asterisk;
  result.tab     = g_nowKey.tab    > g_oldKey.tab;
  result.a       = g_nowKey.a      > g_oldKey.a;
  result.b       = g_nowKey.b      > g_oldKey.b;
  result.c       = g_nowKey.c      > g_oldKey.c;
  result.d       = g_nowKey.d      > g_oldKey.d;
  result.e       = g_nowKey.e      > g_oldKey.e;
  result.f       = g_nowKey.f      > g_oldKey.f;
  result.g       = g_nowKey.g      > g_oldKey.g;
  result.h       = g_nowKey.h      > g_oldKey.h;
  result.i       = g_nowKey.i      > g_oldKey.i;
  result.j       = g_nowKey.j      > g_oldKey.j;
  result.k       = g_nowKey.k      > g_oldKey.k;
  result.l       = g_nowKey.l      > g_oldKey.l;
  result.n       = g_nowKey.n      > g_oldKey.n;
  result.m       = g_nowKey.m      > g_oldKey.m;
  result.o       = g_nowKey.o      > g_oldKey.o;
  result.p       = g_nowKey.p      > g_oldKey.p;
  result.q       = g_nowKey.q      > g_oldKey.q;
  result.r       = g_nowKey.r      > g_oldKey.r;
  result.s       = g_nowKey.s      > g_oldKey.s;
  result.t       = g_nowKey.t      > g_oldKey.t;
  result.u       = g_nowKey.u      > g_oldKey.u;
  result.v       = g_nowKey.v      > g_oldKey.v;
  result.w       = g_nowKey.w      > g_oldKey.w;
  result.x       = g_nowKey.x      > g_oldKey.x;
  result.y       = g_nowKey.y      > g_oldKey.y;
  result.z       = g_nowKey.z      > g_oldKey.z;
  result.rEnter  = g_nowKey.rEnter > g_oldKey.rEnter;
  result.lCtrl   = g_nowKey.lCtrl  > g_oldKey.lCtrl;
  result.lShift  = g_nowKey.lShift > g_oldKey.lShift;
  result.space   = g_nowKey.space  > g_oldKey.space;
  result.alt     = g_nowKey.alt    > g_oldKey.alt;
  result.f1      = g_nowKey.f1     > g_oldKey.f1;
  result.f2      = g_nowKey.f2     > g_oldKey.f2;
  result.f3      = g_nowKey.f3     > g_oldKey.f3;
  result.f4      = g_nowKey.f4     > g_oldKey.f4;
  result.f5      = g_nowKey.f5     > g_oldKey.f5;
  result.f6      = g_nowKey.f6     > g_oldKey.f6;
  result.f7      = g_nowKey.f7     > g_oldKey.f7;
  result.f8      = g_nowKey.f8     > g_oldKey.f8;
  result.f9      = g_nowKey.f9     > g_oldKey.f9;
  result.f10     = g_nowKey.f10    > g_oldKey.f10;
  result.f11     = g_nowKey.f11    > g_oldKey.f11;
  result.f12     = g_nowKey.f12    > g_oldKey.f12;
  result.up      = g_nowKey.up     > g_oldKey.up;
  result.down    = g_nowKey.down   > g_oldKey.down;
  result.left    = g_nowKey.left   > g_oldKey.left;
  result.right   = g_nowKey.right  > g_oldKey.right;
  return result;
}

/// <summary>
/// キーボードを離した瞬間の情報の取得
/// </summary>
/// <returns>キーボードの情報</returns>
KEY_BOARD GetTrgOutKeyBoard()
{
 KEY_BOARD result = {0};
  result.esc     = g_nowKey.esc    < g_oldKey.esc;
  result.num0    = g_nowKey.num0   < g_oldKey.num0;
  result.num1    = g_nowKey.num1   < g_oldKey.num1;
  result.num2    = g_nowKey.num2   < g_oldKey.num2;
  result.num3    = g_nowKey.num3   < g_oldKey.num3;
  result.num4    = g_nowKey.num4   < g_oldKey.num4;
  result.num5    = g_nowKey.num5   < g_oldKey.num5;
  result.num6    = g_nowKey.num6   < g_oldKey.num6;
  result.num7    = g_nowKey.num7   < g_oldKey.num7;
  result.num8    = g_nowKey.num8   < g_oldKey.num8;
  result.num9    = g_nowKey.num9   < g_oldKey.num9;
  result.numPad0 = g_nowKey.numPad0< g_oldKey.numPad0;
  result.numPad1 = g_nowKey.numPad1< g_oldKey.numPad1;
  result.numPad2 = g_nowKey.numPad2< g_oldKey.numPad2;
  result.numPad3 = g_nowKey.numPad3< g_oldKey.numPad3;
  result.numPad4 = g_nowKey.numPad4< g_oldKey.numPad4;
  result.numPad5 = g_nowKey.numPad5< g_oldKey.numPad5;
  result.numPad6 = g_nowKey.numPad6< g_oldKey.numPad6;
  result.numPad7 = g_nowKey.numPad7< g_oldKey.numPad7;
  result.numPad8 = g_nowKey.numPad8< g_oldKey.numPad8;
  result.numPad9 = g_nowKey.numPad9< g_oldKey.numPad9;
  result.plus    = g_nowKey.plus   < g_oldKey.plus;
  result.minus   = g_nowKey.minus  < g_oldKey.minus;
  result.slash   = g_nowKey.slash  < g_oldKey.slash;
  result.asterisk= g_nowKey.asterisk  < g_oldKey.asterisk;
  result.tab     = g_nowKey.tab    < g_oldKey.tab;
  result.a       = g_nowKey.a      < g_oldKey.a;
  result.b       = g_nowKey.b      < g_oldKey.b;
  result.c       = g_nowKey.c      < g_oldKey.c;
  result.d       = g_nowKey.d      < g_oldKey.d;
  result.e       = g_nowKey.e      < g_oldKey.e;
  result.f       = g_nowKey.f      < g_oldKey.f;
  result.g       = g_nowKey.g      < g_oldKey.g;
  result.h       = g_nowKey.h      < g_oldKey.h;
  result.i       = g_nowKey.i      < g_oldKey.i;
  result.j       = g_nowKey.j      < g_oldKey.j;
  result.k       = g_nowKey.k      < g_oldKey.k;
  result.l       = g_nowKey.l      < g_oldKey.l;
  result.n       = g_nowKey.n      < g_oldKey.n;
  result.m       = g_nowKey.m      < g_oldKey.m;
  result.o       = g_nowKey.o      < g_oldKey.o;
  result.p       = g_nowKey.p      < g_oldKey.p;
  result.q       = g_nowKey.q      < g_oldKey.q;
  result.r       = g_nowKey.r      < g_oldKey.r;
  result.s       = g_nowKey.s      < g_oldKey.s;
  result.t       = g_nowKey.t      < g_oldKey.t;
  result.u       = g_nowKey.u      < g_oldKey.u;
  result.v       = g_nowKey.v      < g_oldKey.v;
  result.w       = g_nowKey.w      < g_oldKey.w;
  result.x       = g_nowKey.x      < g_oldKey.x;
  result.y       = g_nowKey.y      < g_oldKey.y;
  result.z       = g_nowKey.z      < g_oldKey.z;
  result.rEnter  = g_nowKey.rEnter < g_oldKey.rEnter;
  result.lCtrl   = g_nowKey.lCtrl  < g_oldKey.lCtrl;
  result.lShift  = g_nowKey.lShift < g_oldKey.lShift;
  result.space   = g_nowKey.space  < g_oldKey.space;
  result.alt     = g_nowKey.alt    < g_oldKey.alt;
  result.f1      = g_nowKey.f1     < g_oldKey.f1;
  result.f2      = g_nowKey.f2     < g_oldKey.f2;
  result.f3      = g_nowKey.f3     < g_oldKey.f3;
  result.f4      = g_nowKey.f4     < g_oldKey.f4;
  result.f5      = g_nowKey.f5     < g_oldKey.f5;
  result.f6      = g_nowKey.f6     < g_oldKey.f6;
  result.f7      = g_nowKey.f7     < g_oldKey.f7;
  result.f8      = g_nowKey.f8     < g_oldKey.f8;
  result.f9      = g_nowKey.f9     < g_oldKey.f9;
  result.f10     = g_nowKey.f10    < g_oldKey.f10;
  result.f11     = g_nowKey.f11    < g_oldKey.f11;
  result.f12     = g_nowKey.f12    < g_oldKey.f12;
  result.up      = g_nowKey.up     < g_oldKey.up;
  result.down    = g_nowKey.down   < g_oldKey.down;
  result.left    = g_nowKey.left   < g_oldKey.left;
  result.right   = g_nowKey.right  < g_oldKey.right;
  return result;
}

/// <summary>
/// マウスの情報の取得
/// </summary>
/// <returns>マウスの情報</returns>
MOUSE GetMouse()
{
  return g_nowMouse;
}

/// <summary>
/// マウスボタンを押した瞬間の情報の取得
/// </summary>
/// <returns>マウスの情報</returns>
MOUSE GetTrgInMouseButton()
{
  MOUSE result = { 0 };
  result.x = g_nowMouse.x != g_oldMouse.x;
  result.y = g_nowMouse.y != g_oldMouse.y;
  result.z = g_nowMouse.z != g_oldMouse.z;
  for(int i = 0; i < 8; i++)
    result.button[i] = g_nowMouse.button[i] > g_oldMouse.button[i];
  return result;
}

/// <summary>
/// マウスボタンを離した瞬間の情報の取得
/// </summary>
/// <returns>マウスの情報</returns>
MOUSE GetTrgOutMouseButton()
{
  MOUSE result = {0};
  result.x = g_nowMouse.x == 0 && g_nowMouse.x != g_oldMouse.x;
  result.y = g_nowMouse.y == 0 && g_nowMouse.y != g_oldMouse.y;
  result.z = g_nowMouse.z == 0 && g_nowMouse.z != g_oldMouse.z;
  for(int i = 0; i < 8; i++)
    result.button[i] = g_nowMouse.button[i] < g_oldMouse.button[i];
  return result;
}

/// <summary>
/// ゲームパッドの情報の取得
/// </summary>
/// <returns>ゲームパッドの情報</returns>
JOY_STICK GetJoyStickButton()
{
  return g_nowJoyStick;
}

/// <summary>
/// ゲームパッドボタンを押した瞬間の情報の取得
/// </summary>
/// <returns>ゲームパッドの情報</returns>
JOY_STICK GetTrgInJoyStickButton()
{
  JOY_STICK result = g_nowJoyStick;
  result.up     = g_nowJoyStick.up    > g_oldJoyStick.up   ;
  result.down   = g_nowJoyStick.down  > g_oldJoyStick.down ;
  result.left   = g_nowJoyStick.left  > g_oldJoyStick.left ;
  result.right  = g_nowJoyStick.right > g_oldJoyStick.right;
  for(int i = 0; i < 24; i++)
    result.button[i] = g_nowJoyStick.button[i] > g_oldJoyStick.button[i];
  return result;
}

/// <summary>
/// ゲームパッドボタンを離した瞬間の情報の取得
/// </summary>
/// <returns>ゲームパッドの情報</returns>
JOY_STICK GetTrgOutJoyStickButton()
{
  JOY_STICK result = g_nowJoyStick;
  result.up     = g_nowJoyStick.up    < g_oldJoyStick.up   ;
  result.down   = g_nowJoyStick.down  < g_oldJoyStick.down ;
  result.left   = g_nowJoyStick.left  < g_oldJoyStick.left ;
  result.right  = g_nowJoyStick.right < g_oldJoyStick.right;
  for(int i = 0; i < 24; i++)
    result.button[i] = g_nowJoyStick.button[i] < g_oldJoyStick.button[i];
  return result;
}