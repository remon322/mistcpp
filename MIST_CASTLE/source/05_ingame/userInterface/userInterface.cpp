#include "../camera/camera.h"
#include "../fog/fog.h"
#include "../object/object.h"
#include "../object/player/player.h"
#include "../object/treasure/treasure.h"
#include "../stageManager/stageManager.h"
#include "../../00_application/app/app.h"
#include "../../00_application/lib/myDx9Lib.h"
#include "../../01_data/texture/loadTexture.h"

#define HP_X (103)          //HPの左上X座標
#define HP_Y (38)           //HPの左上Y座標
#define SP_X (103)          //SPの左上X座標
#define SP_Y (81)           //SPの左上Y座標
#define LEFT_WIND_X  (540 + 500)  //残り数ウィンドウの左上X座標
#define LEFT_WIND_Y  (330 + 300)  //残り数ウィンドウの左上Y座標
#define TENS_PLACE_X (652 + 500)  //10の位の左上X座標
#define TENS_PLACE_Y (340 + 300)  //10の位の左上Y座標
#define ONES_PLACE_X (675 + 500)  //1の位の左上X座標
#define ONES_PLACE_Y (340 + 300)  //1の位の左上Y座標

#define TREASURE_ARROW_RANGE (750.0f) //宝箱への矢印を表示する範囲

static int g_healthMax  = 0;                //プレイヤーのHP最大値
static int g_health     = 0;                //プレイヤーのHP
static int g_magicPoint = 0;                //プレイヤーのMP
static int g_fogLevel = 1;                  //霧の濃さレベル
static int g_healthGuageShave = 0;          //HPゲージの削るpix数
static int g_staminaGuageShave = 0;         //SPゲージの削るpix数
static int g_treasureTensPlaceNum;          //のこり財宝数の10の位の数字
static int g_treasureOnesPlaceNum;          //のこり財宝数の1の位の数字
static bool g_isArrowRender;                //矢印を描画するか？
static D3DXVECTOR3 g_arrowTargetPos;        //矢印の描画向き目標地点
static bool g_isTreasureArrowRender;        //宝箱向きの矢印を描画するか？
static D3DXVECTOR3 g_treasureArrowTargetPos;//宝箱向きの矢印の描画向き目標地点
static int g_arrowSpriteNum;                //矢印のスプライト現在枚数
static int g_spriteStartTime;               //スプライト開始時間

/// <summary>
/// UIの情報の更新
/// </summary>
void UpdateUserInterface()
{
  OBJ_DATA player = GetPlayerData();
  if(!player.option_) { return; }
  PLAYER_OPTION* playerOp = (PLAYER_OPTION*)player.option_;

  //描画用データの更新(プレイヤーのデータから参照)
  g_healthMax = playerOp->healthMax_;
  g_health = playerOp->health_;
  g_magicPoint = CLUMP(0, playerOp->magicPoint_, 3);

  //霧の濃さ取得
  GetFogInfomation(&g_fogLevel, NULL, NULL);
  g_fogLevel = CLUMP(0, g_fogLevel, 4);

  //ゲージの削るpix数計算
  g_healthGuageShave = 360.0f * (1.0f - ((float)playerOp->health_ / (float)playerOp->healthMax_));
  g_staminaGuageShave = 245.0f * (1.0f - ((float)playerOp->stamina_ / (float)playerOp->staminaMax_));

  //残りの宝の数
  int treasureNum = GetTreasureNum();
  g_treasureTensPlaceNum = treasureNum / 10;
  g_treasureOnesPlaceNum = treasureNum % 10;

  //ゴールへの矢印を表示
  OBJ_DATA* treasure = GetNearTreasure(GetPlayerData().pos_, TREASURE_ARROW_RANGE);
  if(treasure != NULL)
  {
    //描画falseからtrueに切り替わるときの時間を保存
    if(g_isTreasureArrowRender == false)
      g_spriteStartTime = timeGetTime();
    //描画をtrueに
    g_isTreasureArrowRender = true;
    //宝箱方向に向ける
    float tmpY;
    g_treasureArrowTargetPos = GetPlayerData().pos_;
    tmpY = g_treasureArrowTargetPos.y + 1000000;
    g_treasureArrowTargetPos += UnitVector(treasure->pos_ - g_treasureArrowTargetPos);
    g_treasureArrowTargetPos.y = tmpY;
  }
  else
  {
    //描画をfalseに
    g_isTreasureArrowRender = false;
  }

  //ゴールへの矢印を表示
  if(treasureNum <= 0)
  {
    //描画falseからtrueに切り替わるときの時間を保存
    if(g_isArrowRender == false)
      g_spriteStartTime = timeGetTime();
    //描画をtrueに
    g_isArrowRender = true;
    //ゴール方向に向ける
    float tmpY;
    g_arrowTargetPos = GetPlayerData().pos_;
    tmpY = g_arrowTargetPos.y + 100;
    g_arrowTargetPos += UnitVector(GetGoalPos() - g_arrowTargetPos);
    g_arrowTargetPos.y = tmpY;
  }
  else
    g_isArrowRender = false;

  //スプライトの更新
  g_arrowSpriteNum = (timeGetTime() - g_spriteStartTime) / 70 % 20;
}

/// <summary>
/// UIの描画
/// </summary>
void RenderUserInterface()
{
  //UI背景
  RenderTexture(0, 0, g_textureHandles[UI_BASE], 255);
  //HP
  RenderTextureShave(HP_X, HP_Y, g_textureHandles[UI_HP_COLOR], 255, g_healthGuageShave, 0);
  RenderTexture(0, 0, g_textureHandles[UI_HP_FRAME], 255);
  //SP
  RenderTextureShave(SP_X, SP_Y, g_textureHandles[UI_SP_COLOR], 255, g_staminaGuageShave, 0);
  RenderTexture(0, 0, g_textureHandles[UI_SP_FRAME], 255);
  //スキル
  RenderBlockTexture(0, 0, g_textureHandles[UI_SKILL_MINUS], 255, 0);
  RenderBlockTexture(0, 0, g_textureHandles[UI_SKILL_PLUS], 255, 0);
  //霧の濃さ
  RenderBlockTexture(0, 0, g_textureHandles[UI_MIST_LEVEL], 255, g_fogLevel);
  //残りの宝数
  RenderTexture(LEFT_WIND_X, LEFT_WIND_Y, g_textureHandles[LEFT_WINDOW], 255);
  RenderBlockTexture(TENS_PLACE_X, TENS_PLACE_Y, g_textureHandles[NUMBERS], 255, g_treasureTensPlaceNum);
  RenderBlockTexture(ONES_PLACE_X, ONES_PLACE_Y, g_textureHandles[NUMBERS], 255, g_treasureOnesPlaceNum);

  //誘導矢印の描画
  if(g_isTreasureArrowRender)
    RenderBlockBillboardRot(GetPlayerData().pos_, g_treasureArrowTargetPos, 0.0f, g_textureHandles[UI_TREASURE_ARROW], 255, g_arrowSpriteNum);
  //誘導矢印の描画
  if(g_isArrowRender)
    RenderBlockBillboardRot(GetPlayerData().pos_, g_arrowTargetPos, 0.0f, g_textureHandles[UI_ARROW], 255, g_arrowSpriteNum);

  //アイテム欄
  //RenderTexture(980, 0, g_textureHandles[UI_ITEM_CHOOSE_BASE], 255);
  //RenderTexture(980, 0, g_textureHandles[UI_ITEM_CHOOSE_UP], 255);
  //RenderBlockTexture(980, 0, g_textureHandles[UI_ITEM_CHOOSE_LEFT], 255, 0);
  //RenderBlockTexture(980, 0, g_textureHandles[UI_ITEM_CHOOSE_RIGHT], 255, 1);
}