#include "../object/player/player.h"
#include "../object/treasure/treasure.h"
#include "../../00_application/function/math.h"
#include "../../00_application/app/app.h"
#include "../../01_data/sound/dslib.h"
#include "../../01_data/sound/loadSound.h"
#include "../../01_data/stageEdit/stageEdit.h"
#include "../../05_ingame/camera/camera.h"
#include "../../05_ingame/object/player/player.h"

static int g_nowStageNum; //現在のステージナンバー
static D3DXVECTOR3 g_goalPos; //ゴール座標

/// <summary>
/// ステージマネージャーの初期化
/// </summary>
void InitStage()
{
  g_nowStageNum = 0;
  ChangeStage(g_nowStageNum);
  //ステージ1の開始地点にプレイヤーを移動させる
  GetPlayerAddress()->pos_ = D3DXVECTOR3(0.0f, 0.0f, 1500.0f);
  g_goalPos = D3DXVECTOR3(7000.0f, 0.0f, -2750.0f);
}

/// <summary>
/// ステージの管理
/// </summary>
void StageManagement()
{
  if(g_nowStageNum == 0)
  {
    //ステージ1のゴール地点に到達しているか判断
    if(GetTreasureNum() <= 0 && GetDistance(GetPlayerData().pos_ , g_goalPos) < 250.0f && GetGameMode() != GAMEMODE_MAP_CREATE)
    {
      g_nowStageNum = 1;
      //音を止める
      for(int i = 0; i < SOUND_MAX; i++)
        DSoundStop(g_soundHandles[i]);

      //ゴール座標をステージ2用にする
      g_goalPos = D3DXVECTOR3(-1000.0f, 0.0f, 9300.0f);

      //ロード画像を描画する
      void* texHandle = LoadBlockTexture("res/texture/2D/load.png", 4, 1);
      if(BeginRender())
      {
        RenderBlockTexture(0, 0, texHandle, 255, 0);
        EndRender();
      }

      //ゲームの状態を遷移する
      ChangeStage(1);
      //プレイヤー座標を変化させる
      GetPlayerAddress()->pos_ = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
      GetPlayerAddress()->yawAngle_ = PI / 2.0f;
      //カメラの向きを変化させる
      GetCameraAddress()->phi_ = PI / 2.0f;
      //ステージ2のBGMを鳴らす
      DSoundPlay(g_soundHandles[MAP2_BGM], TRUE);
    }
  }
  else if(g_nowStageNum == 1)
  {
    //ステージ2のゴール地点に到達しているか判断
    if(GetTreasureNum() <= 0 && GetDistance(GetPlayerData().pos_, g_goalPos) < 300.0f && GetGameMode() != GAMEMODE_MAP_CREATE)
    {
      g_nowStageNum = 2;

      //音を止める
      for (int i = 0; i < SOUND_MAX; i++)
        DSoundStop(g_soundHandles[i]);

      //ロード画像を描画する
      void* texHandle = LoadBlockTexture("res/texture/2D/load.png", 4, 1);
      if (BeginRender())
      {
        RenderBlockTexture(0, 0, texHandle, 255, 0);
        EndRender();
      }

      //ゲームの状態を遷移する
      ChangeStage(0);
      //ゲームの状態を遷移する
      ChangeGameState(GAME_STATE_CLEAR);
    }
  }
}

/// <summary>
/// ゴール地点座標の取得
/// </summary>
/// <returns>ゴール地点座標</returns>
D3DXVECTOR3 GetGoalPos()
{
  return g_goalPos;
}