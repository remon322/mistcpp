#pragma once

//ステージマネージャーの初期化
void InitStage();

//ステージの管理
void StageManagement();

// ゴール地点座標の取得
D3DXVECTOR3 GetGoalPos();