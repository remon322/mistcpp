
#include "camera.h"
#include "../hitJudge/hitJudge.h"
#include "../object/object.h"
#include "../object/player/player.h"
#include "../../00_application/lib/myDx9Lib.h"
#include "../../00_application/app/app.h"
#include "../../00_application/function/math.h"
#include "../../01_data/status/loadStatus.h"
#include "../../02_input/input.h"

//カメラ情報
static struct CAMERA g_camera;

/// <summary>
/// カメラの初期化
/// </summary>
void InitCamera()
{
  g_camera.phi_ = PI;
  g_camera.theta_ = PI - PI * 3 / 7.0f;
}

/// <summary>
/// カメラ情報の更新
/// </summary>
void UpdateCamera()
{
  D3DXVECTOR3 viewPos = GetPlayerData().pos_; //カメラの注視点
  KEY_BOARD key = GetKeyBoard();
  MOUSE   mouse = GetMouse();
  JOY_STICK joy = GetJoyStickButton();

  //入力からカメラの回転角度の更新
  if(key.up || joy.rightStickY || mouse.y < 0)
    g_camera.theta_ -= GetStatus()->cameraRoteSpeed_ * max(key.up, max(joy.rightStickY, -mouse.y));
  if(key.down || joy.rightStickY || mouse.y > 0)
    g_camera.theta_ -= GetStatus()->cameraRoteSpeed_ * min(-key.down, min(joy.rightStickY, -mouse.y));
  if(key.right || joy.rightStickX || mouse.x > 0)
    g_camera.phi_ -= GetStatus()->cameraRoteSpeed_ * max(key.right, max(joy.rightStickX, mouse.x));
  if(key.left || joy.rightStickX || mouse.x < 0)
    g_camera.phi_ -= GetStatus()->cameraRoteSpeed_ * min(-key.left, min(joy.rightStickX, mouse.x));

  //角度の正規化
  g_camera.theta_ = NormalizeAngle(g_camera.theta_);
  g_camera.phi_ = NormalizeAngle(g_camera.phi_);

  //注視点の設定
  viewPos.y += 175.0f;
  
  //カメラ座標の更新
  g_camera.theta_ = CLUMP(PI / 4.0f, g_camera.theta_, 3 * PI / 4.0f);
  g_camera.vEyePt_ = GetVectorComponentXYZ(-550.0f, g_camera.theta_, g_camera.phi_ + PI / 2.0f);
  g_camera.vEyePt_.x += viewPos.x;
  g_camera.vEyePt_.y += viewPos.y;
  g_camera.vEyePt_.z += viewPos.z;

  //リストの先頭を取得
  OBJ_DATA* nowPointer = GetHeadObjData();
  //オブジェクトから地形オブジェクトを探す
  while(nowPointer)
  {
    if(nowPointer->kind_ == OBJ_KIND_TERRAIN)
      break;
    else
      nowPointer = nowPointer->pNext_;//次へ
  }
 
  if(nowPointer != NULL)
  {
    float distance;
    D3DXVECTOR3 interPos;
    D3DXVECTOR3 normal;
    if(IsHitRay(&distance, &interPos, &normal, viewPos, g_camera.vEyePt_ - viewPos, *nowPointer) && distance < 550.0f)
      g_camera.vEyePt_ = interPos;
  }

  //カメラの注目向きの更新
  g_camera.vLookatPt_.x = viewPos.x;
  g_camera.vLookatPt_.y = viewPos.y;
  g_camera.vLookatPt_.z = viewPos.z;
  g_camera.vUpVec_ = {0.0f, 1.0f, 0.0f};

  //カメラ角度のリセット
  key = GetTrgInKeyBoard();
  if(key.tab)
  {
    g_camera.phi_ = GetPlayerData().yawAngle_;
  }
}

/// <summary>
/// カメラ情報の取得
/// </summary>
/// <returns>カメラ情報</returns>
CAMERA GetCamera()
{
  return g_camera;
}

/// <summary>
/// カメラ情報アドレスの取得
/// </summary>
/// <returns>カメラ情報アドレス</returns>
CAMERA* GetCameraAddress()
{
  return &g_camera;
}