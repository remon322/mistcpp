
#include "../hitJudge/hitJudge.h"
#include "../camera/camera.h"
#include "../fog/fog.h"
#include "../object/object.h"
#include "../object/player/player.h"
#include "../../00_application/app/app.h"
#include "../../00_application/lib/myDx9Lib.h"
#include "../../01_data/status/loadStatus.h"
#include "../../01_data/stageEdit/stageEdit.h"
#include "../../02_input/input.h"

//fpsをカウントするため、PCの時間を取得する
static int g_oldTime = 0;
static int g_nowTime = 0;

/// <summary>
/// デバッグモードでの更新
/// </summary>
void UpdateDebug()
{
  KEY_BOARD key = GetTrgInKeyBoard();
  JOY_STICK joy = GetTrgInJoyStickButton();

  //霧の濃さ操作
  if(key.num1)
    SetFogLevel(0);
  if(key.num2)
    SetFogLevel(1);
  if(key.num3)
    SetFogLevel(2);
  if(key.num4)
    SetFogLevel(3);
  if(key.num5)
    SetFogLevel(4);

  //ゲームモードの切り替え
  if(key.f1)
    ChangeGameMode(GAMEMODE_NORMAL);
  if(key.f2)
    ChangeGameMode(GAMEMODE_HEALTH_INFINITY);
  if(key.f3)
    ChangeGameMode(GAMEMODE_DEBUG);
  if(key.f4)
    ChangeGameMode(GAMEMODE_DEBUG_HIT_JUDGE);
  if(key.f5)
    ChangeGameMode(GAMEMODE_MAP_CREATE);

  //マップエディットモード
  if(GetGameMode() == GAMEMODE_MAP_CREATE)
  {
    Edit();
  }

  //情報読み込み
  if(key.f12)
  {
    LoadStatus();
    InitFog();
  }
}

/// <summary>
/// デバッグ情報の描画
/// </summary>
void RenderDebug()
{
  //プレイヤーデータの描画
  OBJ_DATA playerData = GetPlayerData();
  DrawPrintf(10, 15 * 1, 0xffff00ff, "player.x       = %.2f", playerData.pos_.x);
  DrawPrintf(10, 15 * 2, 0xffff00ff, "player.y       = %.2f", playerData.pos_.y);
  DrawPrintf(10, 15 * 3, 0xffff00ff, "player.z       = %.2f", playerData.pos_.z);
  DrawPrintf(10, 15 * 4, 0xffff00ff, "player.animID  = %d", playerData.animationData_.animID);
  DrawPrintf(10, 15 * 5, 0xffff00ff, "player.nowTime = %.2f", playerData.animationData_.nowTime);
  DrawPrintf(10, 15 * 6, 0xffff00ff, "player.endTime = %.2f", playerData.animationData_.loopEndTime);
  DrawPrintf(10, 15 * 7, 0xffff00ff, "player.loopRate= %.2f", playerData.animationData_.loopRate);
  DrawPrintf(10, 15 * 8, 0xffff00ff, "player.yawAngle= %.2f", playerData.yawAngle_);

  //オブジェクトの情報
  OBJ_DATA* objData = GetHeadObjData();
  bool hitFlag = false;
  int hitObjNum = 0;
  while (objData)
  {
    if(objData->kind_ != OBJ_KIND_PLAYER){
      {
        hitFlag = true;
        break;
      }
    }

    hitObjNum++;
    objData = objData->pNext_;
  }
  if(hitFlag)
  {
    DrawPrintf(10, 15 * 9, 0xffff00ff, "playerObjHit   = true");
    DrawPrintf(10, 15 * 10, 0xffff00ff, "objNum         = %d", hitObjNum);
  }
  else
  {
    DrawPrintf(10, 15 * 9, 0xffff00ff, "playerObjHit   = false");
    DrawPrintf(10, 15 * 10, 0xffff00ff, "objNum         = NONE");
  }

  //あたり判定を描画する
  objData = GetHeadObjData();
  while(objData)
  {
    D3DXVECTOR3 pos = objData->pos_;
    HIT_LIST* hit = objData->headHitList_;

    while(hit)
    {
      D3DXVECTOR3 renderPos;
      switch(hit->formKind)
      {
      case FORM_SPHERE:
        RenderSphere(hit->info.sphere.center + pos, hit->info.sphere.radius, 0x50ff0000);
        break;
      case FORM_FAN:
        //TODO:renderFan;
        break;
      case FORM_BOX:
        renderPos = hit->info.box.center + pos;
        RenderBox(renderPos,
                  hit->info.box.width,
                  hit->info.box.height,
                  hit->info.box.depth,
                  0.0f,
                  0x50ff8000);
        break;
      case FORM_CAPSULE:
        renderPos = hit->info.capsule.center + pos;
        RenderCylinder(renderPos,
                       hit->info.capsule.radius,
                       hit->info.capsule.height,
                       0x50ffffff);
        RenderSphere(D3DXVECTOR3(renderPos.x, renderPos.y + hit->info.capsule.height / 2.0f, renderPos.z), hit->info.capsule.radius, 0x50ffffff);
        RenderSphere(D3DXVECTOR3(renderPos.x, renderPos.y - hit->info.capsule.height / 2.0f, renderPos.z), hit->info.capsule.radius, 0x50ffffff);
        break;
      case FORM_FAN_PILLAR:
        renderPos = hit->info.fanPillar.center + pos;
        RenderFanPillar(renderPos,
                        hit->info.fanPillar.radius,
                        hit->info.fanPillar.height,
                        hit->info.fanPillar.fanAngle,
                        hit->info.fanPillar.direction,
                        0x50ff80ff);
        break;
      }

      hit = hit->pNext;
    }
    objData = objData->pNext_;
  }

  //fps値の計算
  static int frame = 0;
  static float fps = 0;
  const int intervalFrame = 30;
  frame++;
  //intervalFrameの値フレームが進行するごとに時間をとり平均fpsを求める
  if(frame % intervalFrame == 0)
  {
    g_oldTime = g_nowTime;
    g_nowTime = timeGetTime();
    //fpsを求める
    float advanceTime = (float)(g_nowTime - g_oldTime) / (float)intervalFrame;
    if(advanceTime != 0)
    {
      fps = 1.0f / advanceTime * 1000.0f;
    }
  }
  DrawPrintf(10, 640, 0xff00ffff, "fps = %.2f", fps);

  //パッドのキー入力情報描画
  JOY_STICK joy = GetJoyStickButton();
  DrawPrintf(1000, 15 * 1, 0xffffff00, "joy.leftStickX  = %.2f", joy.leftStickX);
  DrawPrintf(1000, 15 * 2, 0xffffff00, "joy.leftStickY  = %.2f", joy.leftStickY);
  DrawPrintf(1000, 15 * 3, 0xffffff00, "joy.rightStickX = %.2f", joy.rightStickX);
  DrawPrintf(1000, 15 * 4, 0xffffff00, "joy.rightStickY = %.2f", joy.rightStickY);
  DrawPrintf(1000, 15 * 5, 0xffffff00, "joy.z           = %.2f", joy.z);
  DrawPrintf(1000, 15 * 6, 0xffffff00, "joy.up          = %d", joy.up);
  DrawPrintf(1000, 15 * 7, 0xffffff00, "joy.down        = %d", joy.down);
  DrawPrintf(1000, 15 * 8, 0xffffff00, "joy.left        = %d", joy.left);
  DrawPrintf(1000, 15 * 9, 0xffffff00, "joy.right       = %d", joy.right);

  //オブジェクト数
  int objNum = GetObjectNum();
  DrawPrintf(1000, 655, 0xffffff00, "objNum          = %d", objNum);

  //グリッド線の描画
  RenderRay(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(50.0f, 0.0f, 0.0f), 0xffff0000);
  RenderRay(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 50.0f, 0.0f), 0xff00ff00);
  RenderRay(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 50.0f), 0xff0000ff);

  //カメラ情報の描画
  CAMERA camera = GetCamera();
  DrawPrintf(10, 15 * 19, 0xffffffff, "camera.angle       = %.2f", camera.phi_);

  //霧情報の描画
  float fog[2];
  int level;
  int advance;
  advance = GetFogInfomation(&level, &fog[0], &fog[1]);
  DrawPrintf(10, 15 * 20, 0xffffaaaa, "fog.near       = %d", level);
  DrawPrintf(10, 15 * 21, 0xffffaaaa, "fog.near       = %.2f", fog[0]);
  DrawPrintf(10, 15 * 22, 0xffffaaaa, "fog.far        = %.2f", fog[1]);
  DrawPrintf(10, 15 * 23, 0xffffaaaa, "fog.advance    = %d", advance);
}