#include "hitJudge.h"
#include "../../00_application/lib/myDx9Lib.h"
#include "../../00_application/app/app.h"
#include "../../00_application/function/math.h"
#include "../../05_ingame/camera/camera.h"
#include "../../05_inGame/object/object.h"
#include "../../05_inGame/object/player/player.h"

//当たり判定リストの削除
void DeleteHitList(OBJ_DATA* objData, HIT_LIST* deletePointer);

/// <summary>
/// レイとオブジェクトの交差判定
/// </summary>
/// <param name="distance">(out)交点との距離</param>
/// <param name="interPos">(out)交点の座標</param>
/// <param name="normal">(out)交差したポリゴンの法線</param>
/// <param name="rayStartPos">reyの開始座標</param>
/// <param name="rayVec">reyのベクトル</param>
/// <param name="targetObj">レイを飛ばすターゲットのオブジェクト</param>
/// <returns>レイが当たっていればtrue</returns>
bool IsHitRay(float* distance, D3DXVECTOR3* interPos, D3DXVECTOR3* normal, D3DXVECTOR3 rayStartPos, D3DXVECTOR3 rayVec, OBJ_DATA targetObj)
{
  bool result = false;
  int index;
  //レイの当たり判定
  result = RayIntersect(distance, &index, interPos, normal, rayStartPos, rayVec, targetObj.pos_, targetObj.pMeshHandle_, targetObj.yawAngle_, targetObj.scale_);
  return result;
}

/// <summary>
/// 球と球の当たり判定
/// </summary>
/// <param name="objPosA">オブジェクトAの中心座標</param>
/// <param name="hitA">オブジェクトAの当たり判定情報</param>
/// <param name="objPosB">オブジェクトBの中心座標</param>
/// <param name="hitB">オブジェクトBの当たり判定情報</param>
/// <returns>レイが当たっていればtrue</returns>
bool IsHitSphereSphere(D3DXVECTOR3 objPosA, HIT_SPHERE hitA, D3DXVECTOR3 objPosB, HIT_SPHERE hitB)
{
  bool result = false;
  //オブジェクト座標の相対座標からワールド座標に変換
  D3DXVECTOR3 worldPosA = hitA.center + objPosA;
  D3DXVECTOR3 worldPosB = hitB.center + objPosB;

  float distance = GetDistance(worldPosA, worldPosB);
  if (distance <= hitA.radius + hitB.radius)
  {
    result = true;
  }
  return result;
}

/// <summary>
/// 扇形と点の当たり判定
/// </summary>
/// <param name="objPosA">オブジェクトAの中心座標</param>
/// <param name="hitA">オブジェクトAの当たり判定情報</param>
/// <param name="objPosB">オブジェクトBの中心座標</param>
/// <param name="hitB">オブジェクトBの当たり判定情報</param>
/// <returns>レイが当たっていればtrue</returns>
bool IsHitFanSphere(D3DXVECTOR3 objPosA, HIT_FAN hitA, D3DXVECTOR3 objPosB, HIT_SPHERE hitB)
{
  bool result = false;
  //オブジェクト座標の相対座標からワールド座標に変換
  D3DXVECTOR3 worldPosA = hitA.center + objPosA;
  D3DXVECTOR3 worldPosB = hitB.center + objPosB;

  //球体として当たっているか判定
  float distance = GetDistance(worldPosA, worldPosB);
  if (distance > (hitA.radius + hitB.radius))
  {
    //球の段階で当たっていないので弾く
    return result;
  }
  
  //２つのセンター間で内積をとって当たっているか判定
  D3DXVECTOR3 directionAtoB = UnitVector(worldPosA - worldPosB);
  float dot = DotProduct3D(UnitVector(hitA.direction), directionAtoB);
  if (cos(hitA.fanAngle / 2.0f) > dot)
  {
    result = true;
  }
  
  return result;
}

/// <summary>
/// 軸に沿ったボックスとスフィアのあたり判定
/// </summary>
/// <param name="objPosA">オブジェクトAの中心座標</param>
/// <param name="hitA">オブジェクトAの当たり判定情報</param>
/// <param name="objPosB">オブジェクトBの中心座標</param>
/// <param name="hitB">オブジェクトBの当たり判定情報</param>
/// <returns>レイが当たっていればtrue</returns>
bool IsHitBoxSphere(D3DXVECTOR3 objPosA, HIT_BOX hitA, D3DXVECTOR3 objPosB, HIT_SPHERE hitB)
{
  bool result = false;
  //オブジェクト座標の相対座標からワールド座標に変換
  D3DXVECTOR3 worldPosA = hitA.center + objPosA;
  D3DXVECTOR3 worldPosB = hitB.center + objPosB;

  //立方体の全頂点の定義
  D3DXVECTOR3 vertex[8] = {
    {worldPosA.x + hitA.width / 2.0f, worldPosA.y + hitA.height / 2.0f, worldPosA.z + hitA.depth / 2.0f},
    {worldPosA.x - hitA.width / 2.0f, worldPosA.y + hitA.height / 2.0f, worldPosA.z + hitA.depth / 2.0f},
    {worldPosA.x + hitA.width / 2.0f, worldPosA.y - hitA.height / 2.0f, worldPosA.z + hitA.depth / 2.0f},
    {worldPosA.x - hitA.width / 2.0f, worldPosA.y - hitA.height / 2.0f, worldPosA.z + hitA.depth / 2.0f},
    {worldPosA.x + hitA.width / 2.0f, worldPosA.y + hitA.height / 2.0f, worldPosA.z - hitA.depth / 2.0f},
    {worldPosA.x - hitA.width / 2.0f, worldPosA.y + hitA.height / 2.0f, worldPosA.z - hitA.depth / 2.0f},
    {worldPosA.x + hitA.width / 2.0f, worldPosA.y - hitA.height / 2.0f, worldPosA.z - hitA.depth / 2.0f},
    {worldPosA.x - hitA.width / 2.0f, worldPosA.y - hitA.height / 2.0f, worldPosA.z - hitA.depth / 2.0f},
  };

  //立方体の中に中心点があるか判定
  if(worldPosA.x + hitA.width / 2.0f > worldPosB.x
     &&worldPosA.x - hitA.width / 2.0f < worldPosB.x
     &&worldPosA.y + hitA.height / 2.0f > worldPosB.y
     &&worldPosA.y - hitA.height / 2.0f < worldPosB.y
     &&worldPosA.z + hitA.depth / 2.0f > worldPosB.z
     &&worldPosA.z - hitA.depth / 2.0f < worldPosB.z)
  {
    result = true;
    return result;
  }

  //全頂点がスフィアの中にあるか判定
  for(int i = 0; i < 8; i++)
  {
    float distance = GetDistance(vertex[i], worldPosB);
    if(distance <= hitB.radius)
    {
      result = true;
      return result;
    }
  }

  return result;
}

/// <summary>
/// y軸方向に伸びたカプセルとスフィアのあたり判定
/// </summary>
/// <param name="objPosA">オブジェクトAの中心座標</param>
/// <param name="hitA">オブジェクトAの当たり判定情報</param>
/// <param name="objPosB">オブジェクトBの中心座標</param>
/// <param name="hitB">オブジェクトBの当たり判定情報</param>
/// <returns>レイが当たっていればtrue</returns>
bool IsHitCapsuleSphere(D3DXVECTOR3 objPosA, HIT_CAPSULE hitA, D3DXVECTOR3 objPosB, HIT_SPHERE hitB)
{
  bool result = false;
  //オブジェクト座標の相対座標からワールド座標に変換
  D3DXVECTOR3 worldPosA = hitA.center + objPosA;
  D3DXVECTOR3 worldPosB = hitB.center + objPosB;

  //xz平面における点のお互いの距離を求める
  float distance = GetDistance(D3DXVECTOR3(worldPosA.x, 0.0f, worldPosA.z), D3DXVECTOR3(worldPosB.x, 0.0f, worldPosB.z));

  //真上から見て当たっているか判定
  if(distance > hitA.radius + hitB.radius)
  {
    //当たっていないので弾く
    return result;
  }

  //スフィアの中心点が円柱の中にあるか判定
  if(worldPosB.y >= worldPosA.y - hitA.height / 2.0f
     && worldPosB.y <= worldPosA.y + hitA.height / 2.0f)
  {
    //円柱の中にあるのでヒットしている
    result = true;
    return result;
  }

  //上側のスフィアとhitBのスフィアのあたり判定
  distance = GetDistance(D3DXVECTOR3(worldPosA.x, worldPosA.y + hitA.height / 2.0f, worldPosA.z), worldPosB);
  if(distance <= hitA.radius + hitB.radius)
  {
    //スフィア同士が当たっているのでヒットしている
    result = true;
    return result;
  }

  //下側のスフィアとhitBのスフィアのあたり判定
  distance = GetDistance(D3DXVECTOR3(worldPosA.x, worldPosA.y - hitA.height / 2.0f, worldPosA.z), worldPosB);
  if(distance <= hitA.radius + hitB.radius)
  {
    //スフィア同士が当たっているのでヒットしている
    result = true;
    return result;
  }

  //最終的な値を返す
  return result;
}

/// <summary>
/// y軸に沿った扇形柱とスフィアのあたり判定
/// </summary>
/// <param name="objPosA">オブジェクトAの中心座標</param>
/// <param name="hitA">オブジェクトAの当たり判定情報</param>
/// <param name="objPosB">オブジェクトBの中心座標</param>
/// <param name="hitB">オブジェクトBの当たり判定情報</param>
/// <returns>レイが当たっていればtrue</returns>
bool IsHitFanPillarSphere(D3DXVECTOR3 objPosA, HIT_FAN_PILLAR hitA, D3DXVECTOR3 objPosB, HIT_SPHERE hitB)
{
  bool result = false;
  //オブジェクト座標の相対座標からワールド座標に変換
  D3DXVECTOR3 worldPosA = hitA.center + objPosA;
  D3DXVECTOR3 worldPosB = hitB.center + objPosB;

  //xz平面における点のお互いの距離を求める
  float distance = GetDistance(D3DXVECTOR3(worldPosA.x, 0.0f, worldPosA.z), D3DXVECTOR3(worldPosB.x, 0.0f, worldPosB.z));

  //xz平面において円形同士が離れているか判定
  if(distance > (hitA.radius + hitB.radius))
  {
    //２つの円が完全に離れているため当たっていない
    return result;
  }
  //※ここから下は円形同士で見た場合はふれあっている

  //y_座標が離れているか判定
  //左辺:スフィアの中心y_座標基準で、円柱の円からスフィアの中心点がはみ出している場合高さの補正(球体であるため 真上での距離＝＝真横からの距離 となる)
  //右辺:円柱の上面と底面のそれぞれのy_座標
  if(worldPosB.y + hitB.radius - max(0.0f, (distance - hitA.radius)) < worldPosA.y - hitA.height / 2.0f
   || worldPosB.y - hitB.radius + max(0.0f, (distance - hitA.radius)) > worldPosA.y + hitA.height / 2.0f)
  {
    //y_座標が円柱の中にないため弾く
    return result;
  }
  //※ここから下はy_座標においてふれあっている
 
  //xz平面において円形として当たっているか判定
  if(distance <= hitB.radius)
  {
    //対象の範囲内に扇形の中心点があるためヒットしている
    result = true;
    return result;
  }

  //xz平面上のhitAの座標を基準とした、向きとスフィアの座標の単位ベクトルを求める
  D3DXVECTOR3 dir = UnitVector(D3DXVECTOR3(hitA.direction.x, 0.0f, hitA.direction.z));
  D3DXVECTOR3 localPosB = UnitVector(D3DXVECTOR3(worldPosB.x - worldPosA.x, 0.0f, worldPosB.z - worldPosA.z));
  
  //内積を用いてxz平面上の扇形と点のあたり判定をする
  float dot = DotProduct(dir.x, dir.z, localPosB.x, localPosB.z);
  if(cos(hitA.fanAngle / 2.0f) < dot)
  {
    //点が扇形に内包されており、円形同士がふれあっているためヒットしている
    result = true;
    return result;
  }

  //円の方程式と直線の方程式から、２次方程式を作り、解の公式の判別式を用いてあたっているか判断する

  //直線の方程式の変数(ax+b=y_)
  float lineX, lineY, lineA, lineB;
  //２次方程式の変数(ax^2+bx+c=0)
  float quadraticA, quadraticB, quadraticC;
  // 解の公式の判別式
  float discriminant;

  //左側の扇形の線と円の交差判定
  lineX = hitA.direction.x + cos(hitA.fanAngle / 2.0f);
  lineY = hitA.direction.z + sin(hitA.fanAngle / 2.0f);
  lineA = lineY / lineX;
  lineB = lineY - lineA * lineX;

  quadraticA = (2 * lineA * lineA);
  quadraticB = (2 * lineA * lineB);
  quadraticC = (lineB * lineB - hitB.radius * hitB.radius);

  discriminant = quadraticB * quadraticB - 4 * quadraticA * quadraticC;

  if (discriminant >= 0)
  {
    //result = true;
    return result;
  }

  //右側の扇形の線と円の交差判定
  lineX = hitA.direction.x + cos(hitA.fanAngle / 2.0f);
  lineY = hitA.direction.z + sin(hitA.fanAngle / 2.0f);
  lineA = lineY / lineX;
  lineB = lineY - lineA * lineX;

  quadraticA = (2 * lineA * lineA);
  quadraticB = (2 * lineA * lineB);
  quadraticC = (lineB * lineB - hitB.radius * hitB.radius);

  discriminant = quadraticB * quadraticB - 4 * quadraticA * quadraticC;

  if (discriminant >= 0)
  {
    result = true;
    return result;
  }

  //最終的な値を返す
  return result;
}

/// <summary>
/// オブジェクトの移動とあたり判定に応じた押し出し
/// </summary>
/// <param name="object">移動するオブジェクト</param>
/// <param name="moveVec">移動ベクトル</param>
/// <returns>地形にあたった場合true</returns>
bool MoveObject(OBJ_DATA* object, D3DXVECTOR3 moveVec)
{
  //対象のオブジェクトを探す
  OBJ_DATA* targetObj = GetHeadObjData();
  while(targetObj != NULL)
  {
    //対象か判断する
    if((targetObj->kind_ & (OBJ_KIND_TERRAIN)) <= 0)
    {
      //対象ではないので次のループへ
      targetObj = targetObj->pNext_;
      continue;
    }
  
    float outDistance = 0.0f;
    D3DXVECTOR3 interPos;
    D3DXVECTOR3 normal;
    bool hit = IsHitRay(&outDistance, &interPos, &normal, object->pos_, moveVec, *targetObj);
  
    if(hit)
    {
      //交点に座標を移動
      object->pos_ = interPos;
      //壁から少し離すため法線を引く
      object->pos_ += normal * 2.0f;
      //D3DXVECTOR3 Slip = moveVec - DotProduct3D(normal, moveVec) / (GetScalar(normal) * GetScalar(normal)) * normal;
      //depth++;
      //object->pos_ += Slip;
      //if(depth < 3)
      //{
      //  MoveObject(object, moveVec, depth);
      //}
      return true;
    }
  
    targetObj = targetObj->pNext_;
  }
  object->pos_ = object->pos_ + moveVec;
  return false;
}

/// <summary>
/// オブジェクトの押し出し
/// </summary>
/// <param name="objectA">押し出されるオブジェクト</param>
/// <param name="rA">オブジェクトAの半径</param>
/// <param name="objectB">押し出すオブジェクト</param>
/// <param name="rB">オブジェクトBの半径</param>
/// <returns>押し出しがあった場合true</returns>
bool PushObject(OBJ_DATA* objectA,float rA , OBJ_DATA* objectB, float rB)
{
  //同じオブジェクトなら弾く
  if(objectA == objectB) { return false; }

  //2点間の距離をとる
  D3DXVECTOR3 posA, posB;
  posA = objectA->pos_;
  posB = objectB->pos_;
  float distance = GetDistance(posA, posB);

  //半径が重なっていれば押し出す
  if(distance < rA + rB)
  {
    D3DXVECTOR3 dir;
    dir = UnitVector(posA - posB);//押し出しの移動向きベクトル
    objectA->pos_ += dir * (rA + rB - distance);
    return true;
  }
  return false;
}

/// <summary>
/// キャラオブジェクトとの押し出し
/// </summary>
/// <param name="object">押し出されるオブジェクト</param>
void PushObjectCharacter(OBJ_DATA* object)
{
  //オブジェクトリストの先頭を取得
  OBJ_DATA* nowObj = GetHeadObjData();
  while(nowObj)
  {
    if((nowObj->kind_ & (OBJ_KIND_PLAYER | OBJ_KIND_SKELETON)) > 0)
    {
      //当たり判定がNULLならはじく
      if(object->headHitList_ != NULL && nowObj->headHitList_ != NULL)
      {
        //キャラクターの当たり判定の先頭に本体当たり判定がある前提
        PushObject(object, object->headHitList_->info.sphere.radius,
                   nowObj, nowObj->headHitList_->info.sphere.radius);
      }
    }
    //オブジェクトリストの次を参照する
    nowObj = nowObj->pNext_;
  }
}

/// <summary>
/// オブジェクトのy方向のみの移動
/// </summary>
/// <param name="object">移動するオブジェクト</param>
/// <param name="gravityVec">y成分の移動量</param>
/// <returns>地形にあたった場合true</returns>
bool Landing(OBJ_DATA* object, float gravityVec)
{
  float outDistance = 0.0f;
  float nextPosY = object->pos_.y + gravityVec;
  D3DXVECTOR3 objectPos = object->pos_;
  D3DXVECTOR3 moveVec = D3DXVECTOR3(0.0f, gravityVec - 1.0f, 0.0f);
  D3DXVECTOR3 interPos;
  D3DXVECTOR3 normal;
  bool result = false;

  //objectの仮移動
  object->pos_.y += 1.0f;

  //対象のオブジェクトを探す
  OBJ_DATA* targetObj = GetHeadObjData();
  while (targetObj != NULL)
  {
    //対象か判断する
    if ((targetObj->kind_ & (OBJ_KIND_TERRAIN)) <= 0)
    {
      //対象ではないので次のループへ
      targetObj = targetObj->pNext_;
      continue;
    }

    //ポリゴンとレイのあたり判定
    bool hit = IsHitRay(&outDistance, &interPos, &normal, object->pos_, moveVec, *targetObj);

    if (hit)
    {
      result = true;
      if(nextPosY < interPos.y)
      {
        nextPosY = interPos.y;
      }
    }
    //ターゲットのリストを次へ
    targetObj = targetObj->pNext_;
  }
  object->pos_.y = nextPosY;
  return result;
}

/// <summary>
/// すべてのオブジェクトのあたり判定ツリー処理
/// </summary>
void HitJudge()
{
  //オブジェクトリストの先頭を取得
  OBJ_DATA* nowObj = GetHeadObjData();
  while (nowObj)
  {
    //スケルトンが一定距離以上離れていたら判定しない
    if(nowObj->kind_ == OBJ_KIND_SKELETON && GetDistance(nowObj->pos_, GetCamera().vEyePt_) > 2500)
    {
      //次へ
      nowObj = nowObj->pNext_;
      continue;
    }
    //あたり判定をすべて参照
    HIT_LIST* hitList = nowObj->headHitList_;
    while (hitList)
    {
      OBJ_DATA* targetObj = GetHeadObjData();
      if (!targetObj) { break; }

      //当たり判定の生存時間のカウントダウン
      //カウントが0になったら当たり判定を削除
      if(hitList->frame != INFINITY_FRAME)
        hitList->frame -= GetSpeedRate();
      if (hitList->frame < 0 && hitList->frame != INFINITY_FRAME)
      {
        HIT_LIST* next = hitList->pNext;
        DeleteHitList(nowObj, hitList);
        hitList = next;
        continue;
      }

      //対象オブジェクトをすべて参照
      while (targetObj)
      {
        int mask = (hitList->target & targetObj->kind_);
        if (mask == 0)
        {
          //対象オブジェクトではないので次へ
          targetObj = targetObj->pNext_;
          continue;
        }
        //次のターゲットリストを保持
        OBJ_DATA* nextTargetObj = targetObj->pNext_;

        //当たり判定ツリーを参照
        HIT_LIST* targetHitList = targetObj->headHitList_;
        bool hitFlag = false;
        while (targetHitList != NULL)
        {
          if (targetHitList->kind != BODY)
          {
            //ターゲットの本体あたり判定でないため飛ばす
            targetHitList = targetHitList->pNext;
            continue;
          }
          if (hitFlag)
          {
            //すでにヒットしているため残りの当たり判定を飛ばす
            break;
          }

          //当たり判定の形状ごとに判断
          //ターゲット側はすべてスフィアで判定
          switch (hitList->formKind)
          {
          case FORM_SPHERE:
            hitFlag = IsHitSphereSphere(nowObj->pos_, hitList->info.sphere, targetObj->pos_, targetHitList->info.sphere);
            break;
          case FORM_FAN:
            hitFlag = IsHitFanSphere(nowObj->pos_, hitList->info.fan, targetObj->pos_, targetHitList->info.sphere);
              break;
          case FORM_BOX:
            hitFlag = IsHitBoxSphere(nowObj->pos_, hitList->info.box, targetObj->pos_, targetHitList->info.sphere);
            break;
          case FORM_CAPSULE:
            hitFlag = IsHitCapsuleSphere(nowObj->pos_, hitList->info.capsule, targetObj->pos_, targetHitList->info.sphere);
            break;
          case FORM_FAN_PILLAR:
            hitFlag = IsHitFanPillarSphere(nowObj->pos_, hitList->info.fanPillar, targetObj->pos_, targetHitList->info.sphere);
            break;
          }
          targetHitList = targetHitList->pNext;
        }
        //ヒット時の処理を行う
        if (hitFlag && hitList->hitFunc != NULL)
        {
          hitList->hitFunc(nowObj, targetObj);
        }

        //ターゲットリストを次にする
        targetObj = nextTargetObj;
      }

      //あたり判定リストの次を参照する
      hitList = hitList->pNext;
    }

    //オブジェクトリストの次を参照する
    nowObj = nowObj->pNext_;
  }
}

/// <summary>
/// 当たり判定リストの初期化(リセットではないため、すでに情報が入っている場合メモリリークの可能性有)
/// </summary>
/// <param name="objData">前後のリストを消したいオブジェクト</param>
void InitHitList(OBJ_DATA* objData)
{
  objData->headHitList_ = NULL;
  objData->endHitList_ = NULL;
  objData->hitList_ = NULL;
}

/// <summary>
/// 当たり判定リストの追加
/// </summary>
/// <param name="objData">当たり判定を追加したいオブジェクト</param>
/// <returns>追加された当たり判定</returns>
HIT_LIST* AddHitList(OBJ_DATA* objData)
{
  //リストが一つもないとき
  if(objData->endHitList_ == NULL)
  {
    //メモリの確保
    HIT_LIST *newPointer = (HIT_LIST*)malloc(sizeof(HIT_LIST));
    memset(newPointer, 0, sizeof(HIT_LIST));

    //前後をNULLに
    newPointer->pNext = NULL;
    newPointer->pPrev = NULL;

    //オブジェクトのリスト開始等の情報を入れる
    objData->hitList_ = newPointer;
    objData->headHitList_ = newPointer;
    objData->endHitList_ = newPointer;

    //判定成功時の処理ポインタをNULLに
    newPointer->hitFunc = NULL;
    return newPointer;
  }

  //メモリの確保と確保メモリの前後へのリンク
  HIT_LIST *newPointer = (HIT_LIST*)malloc(sizeof(HIT_LIST));
  HIT_LIST *prevPointer = objData->endHitList_;

  //前後のメモリのリンク
  prevPointer->pNext = newPointer;
  newPointer->pPrev = prevPointer;
  newPointer->pNext = NULL;

  //判定成功時の処理ポインタをNULLに
  newPointer->hitFunc = NULL;

  //オブジェクトのリスト最後尾を入れる
  objData->endHitList_ = newPointer;
  return newPointer;
}

/// <summary>
/// 当たり判定リストの削除
/// </summary>
/// <param name="objData">当たり判定を削除したいオブジェクト</param>
/// <param name="deletePointer">削除したい当たり判定</param>
void DeleteHitList(OBJ_DATA* objData, HIT_LIST* deletePointer)
{
  if(deletePointer == NULL)
  {
    return;
  }

  //リストの前後をつなぎ合わせる
  HIT_LIST* prevPointer = deletePointer->pPrev;
  HIT_LIST* nextPointer = deletePointer->pNext;

  //削除対象が先頭ポインタの場合
  if(deletePointer == objData->headHitList_)
  {
    objData->headHitList_ = objData->headHitList_->pNext;
  }
  else
  {
    prevPointer->pNext = nextPointer;
  }
  //削除対象が最終ポインタだった場合
  if(deletePointer == objData->endHitList_)
  {
    objData->endHitList_ = objData->endHitList_->pPrev;
  }
  else
  {
    nextPointer->pPrev = prevPointer;
  }

  free(deletePointer);
}

/// <summary>
/// 球体型のあたり判定の追加
/// </summary>
/// <param name="kind_">あたり判定の種類(HIT_KINDを参照)</param>
/// <param name="target">あたり判定の対象(OBJ_KINDをor演算で参照)</param>
/// <param name="g_frame">継続時間 (INFINITY_FRAMEを指定で永久化)</param>
/// <param name="objData">あたり判定を付与するオブジェクト情報のポインタ</param>
/// <param name="centerPos">判定の中心座標</param>
/// <param name="r">あたり判定の半径</param>
/// <param name="hitFunc">ヒットした際に処理する関数のポインタ</param>
/// <returns>追加された判定のポインタ</returns>
HIT_LIST* AddHitListSphere(int kind, int target, int frame, OBJ_DATA* objData, D3DXVECTOR3 centerPos,
                           float r, void (*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB))
{
  HIT_LIST* hitList = AddHitList(objData);
  hitList->formKind = FORM_SPHERE;
  hitList->kind = (HIT_KIND)kind;
  hitList->target = target;
  hitList->frame = frame;
  hitList->hitFunc = hitFunc;
  hitList->info.sphere.center = centerPos;
  hitList->info.sphere.radius = r;
  return hitList;
}


/// <summary>
/// 扇型のあたり判定の追加
/// </summary>
/// <param name="kind_">あたり判定の種類(HIT_KINDを参照)</param>
/// <param name="target">あたり判定の対象(OBJ_KINDをor演算で参照)</param>
/// <param name="g_frame">継続時間 (INFINITY_FRAMEを指定で永久化)</param>
/// <param name="objData">あたり判定を付与するオブジェクト情報のポインタ</param>
/// <param name="centerPos">判定の中心座標</param>
/// <param name="fanAngle">扇形の中心角度</param>
/// <param name="direction">扇形を展開する向き</param>
/// <param name="r">扇形の半径</param>
/// <param name="hitFunc">ヒットした際に処理する関数のポインタ</param>
/// <returns>追加された判定のポインタ</returns>
HIT_LIST* AddHitListFan(int kind, int target, int frame, OBJ_DATA* objData, D3DXVECTOR3 centerPos,
                        float fanAngle, D3DXVECTOR3 direction, float r, void(*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB))
{
  HIT_LIST* hitList = AddHitList(objData);
  hitList->formKind = FORM_FAN;
  hitList->kind = (HIT_KIND)kind;
  hitList->target = target;
  hitList->frame = frame;
  hitList->hitFunc = hitFunc;
  hitList->info.fan.center = centerPos;
  hitList->info.fan.radius = r;
  hitList->info.fan.fanAngle = fanAngle;
  hitList->info.fan.direction = direction;
  return hitList;
}

/// <summary>
/// 立方体(AABB)のあたり判定の追加
/// </summary>
/// <param name="kind_">あたり判定の種類(HIT_KINDを参照)</param>
/// <param name="target">あたり判定の対象(OBJ_KINDをor演算で参照)</param>
/// <param name="g_frame">継続時間 (INFINITY_FRAMEを指定で永久化)</param>
/// <param name="objData">あたり判定を付与するオブジェクト情報のポインタ</param>
/// <param name="centerPos">判定の中心座標</param>
/// <param name="width">立方体の横幅 (x_方向の幅)</param>
/// <param name="height">立方体の縦幅 (y_方向の幅)</param>
/// <param name="depth">立方体の奥行 (z_方向の幅)</param>
/// <param name="hitFunc">ヒットした際に処理する関数のポインタ</param>
/// <returns>追加された判定のポインタ</returns>
HIT_LIST* AddHitListBox(int kind, int target, int frame, OBJ_DATA* objData, D3DXVECTOR3 centerPos,
                        float width, float height, float depth, void(*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB))
{
  HIT_LIST* hitList = AddHitList(objData);
  hitList->formKind = FORM_BOX;
  hitList->kind = (HIT_KIND)kind;
  hitList->target = target;
  hitList->frame = frame;
  hitList->hitFunc = hitFunc;
  hitList->info.box.center = centerPos;
  hitList->info.box.width  = width;
  hitList->info.box.height = height;
  hitList->info.box.depth  = depth;
  return hitList;
}

/// <summary>
/// カプセルのあたり判定の追加
/// </summary>
/// <param name="kind_">あたり判定の種類(HIT_KINDを参照)</param>
/// <param name="target">あたり判定の対象(OBJ_KINDをor演算で参照)</param>
/// <param name="g_frame">継続時間 (INFINITY_FRAMEを指定で永久化)</param>
/// <param name="objData">あたり判定を付与するオブジェクト情報のポインタ</param>
/// <param name="centerPos">判定の中心座標</param>
/// <param name="height">円柱の縦幅 (y_方向の幅)</param>
/// <param name="radius">円の半径</param>
/// <param name="hitFunc">ヒットした際に処理する関数のポインタ</param>
/// <returns>追加された判定のポインタ</returns>
HIT_LIST* AddHitListCapsule(int kind, int target, int frame, OBJ_DATA* objData, D3DXVECTOR3 centerPos,
                            float height, float radius, void(*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB))
{
  HIT_LIST* hitList = AddHitList(objData);
  hitList->formKind = FORM_CAPSULE;
  hitList->kind = (HIT_KIND)kind;
  hitList->target = target;
  hitList->frame = frame;
  hitList->hitFunc = hitFunc;
  hitList->info.capsule.center = centerPos;
  hitList->info.capsule.height = height;
  hitList->info.capsule.radius = radius;
  return hitList;
}

/// <summary>
/// 扇形柱のあたり判定の追加
/// </summary>
/// <param name="kind_">あたり判定の種類(HIT_KINDを参照)</param>
/// <param name="target">あたり判定の対象(OBJ_KINDをor演算で参照)</param>
/// <param name="g_frame">継続時間 (INFINITY_FRAMEを指定で永久化)</param>
/// <param name="objData">あたり判定を付与するオブジェクト情報のポインタ</param>
/// <param name="centerPos">判定の中心座標</param>
/// <param name="height">立方体の縦幅 (y_方向の幅)</param>
/// <param name="radius">扇形の半径</param>
/// <param name="fanAngle">扇形の中心角度</param>
/// <param name="direction">扇形を展開する方向(y_の値は無視される)</param>
/// <param name="hitFunc">ヒットした際に処理する関数のポインタ</param>
/// <returns>追加された判定のポインタ</returns>
HIT_LIST* AddHitListFanPillar(int kind, int target, int frame, OBJ_DATA* objData, D3DXVECTOR3 centerPos,
                              float height, float radius, float fanAngle, D3DXVECTOR3 direction, void (*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB))
{
  HIT_LIST* hitList = AddHitList(objData);
  hitList->formKind = FORM_FAN_PILLAR;
  hitList->kind = (HIT_KIND)kind;
  hitList->target = target;
  hitList->frame = frame;
  hitList->hitFunc = hitFunc;
  hitList->info.fanPillar.center = centerPos;
  hitList->info.fanPillar.height = height;
  hitList->info.fanPillar.radius = radius;
  hitList->info.fanPillar.fanAngle = fanAngle;
  hitList->info.fanPillar.direction = direction;
  return hitList;
}
