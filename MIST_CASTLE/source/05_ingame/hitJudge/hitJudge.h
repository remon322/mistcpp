#pragma once
#include "../object/object.h"

//当たり判定を永続化させるための時間
#define INFINITY_FRAME (0x0fffffff)

//球体の当たり判定
struct HIT_SPHERE
{
  D3DXVECTOR3 center;
  float radius;
};

//扇形の当たり判定
struct HIT_FAN
{
  D3DXVECTOR3 center;
  D3DXVECTOR3 direction;
  float radius;
  float fanAngle;      //扇形の中心角
};

//箱の当たり判定
struct HIT_BOX
{
  D3DXVECTOR3 center;
  float width;  // 横幅 x方向の幅
  float height; // 縦幅 y方向の幅
  float depth;  // 奥行 z方向の幅
};

//カプセルの当たり判定
struct HIT_CAPSULE
{
  D3DXVECTOR3 center;
  float height; // 縦幅 y方向の幅
  float radius; // 半径
};

//扇形柱の当たり判定
struct HIT_FAN_PILLAR
{
  D3DXVECTOR3 center;
  float height;
  float radius;
  float fanAngle;
  D3DXVECTOR3 direction;
};

//当たり判定情報
union HIT_INFO
{
  HIT_SPHERE     sphere;
  HIT_FAN        fan;
  HIT_BOX        box;
  HIT_CAPSULE    capsule;
  HIT_FAN_PILLAR fanPillar;
};

//当たり判定の形の種類
enum HIT_FORM_KIND
{
  FORM_SPHERE,
  FORM_FAN,
  FORM_BOX,
  FORM_CAPSULE,
  FORM_FAN_PILLAR,
};

//何のあたり判定か識別子
enum HIT_KIND
{
  BODY,
  ATTACK,
  ACCESS,
  SEARCH,
};

//当たり判定リスト オブジェクト毎にリストを付与
struct HIT_LIST
{
  int target;             //OBJ_KIND列挙をor演算で代入
  float frame;            //あたり判定ののこり継続時間
  HIT_FORM_KIND formKind; //あたり判定の形状
  HIT_KIND kind;          //あたり判定の種類
  HIT_INFO info;          //あたり判定に必要な情報
  HIT_LIST* pPrev;        //リストの次ポインタ
  HIT_LIST* pNext;        //リストの前ポインタ
  void (*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB); //接触時の処理の関数ポインタ
};

//レイとオブジェクトの交差判定
bool IsHitRay(float* distance, D3DXVECTOR3* interPos, D3DXVECTOR3* normal, D3DXVECTOR3 rayStartPos, D3DXVECTOR3 rayVec, OBJ_DATA targetObj);

//オブジェクトの移動とあたり判定に応じた押し出し
//押し出しがあった場合true 何にも当たらなければfalse
bool MoveObject(OBJ_DATA* object, D3DXVECTOR3 moveVec);

//オブジェクトの押し出し
bool PushObject(OBJ_DATA* objectA, float rA, OBJ_DATA* objectB, float rB);

//オブジェクトのy方向のみの移動
bool Landing(OBJ_DATA* object, float gravityVec);

// キャラオブジェクトとの押し出し
void PushObjectCharacter(OBJ_DATA* object);

//すべてのオブジェクトのあたり判定処理
void HitJudge();

//当たり判定リストの初期化
void InitHitList(OBJ_DATA* objData);

//当たり判定リストの追加
//返り値 追加された判定のポインタ
HIT_LIST* AddHitList(OBJ_DATA* objData);

//当たり判定リストの削除
void DeleteHitList(OBJ_DATA* objData, HIT_LIST* deletePointer);

//球体のあたり判定の追加
//返り値 追加された判定のポインタ
HIT_LIST* AddHitListSphere(int kind, int target, int frame, OBJ_DATA* objData, D3DXVECTOR3 centerPos,
                           float r, void(*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB));

//扇形のあたり判定の追加
//返り値 追加された判定のポインタ
HIT_LIST* AddHitListFan(int kind, int target, int frame, OBJ_DATA* objData, D3DXVECTOR3 centerPos,
                        float fanAngle, D3DXVECTOR3 direction, float r, void(*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB));

//立方体のあたり判定の追加
//返り値 追加された判定のポインタ
HIT_LIST* AddHitListBox(int kind, int target, int frame, OBJ_DATA* objData, D3DXVECTOR3 centerPos,
                        float width, float height, float depth, void(*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB));

//カプセルのあたり判定の追加
//返り値 追加された判定のポインタ
HIT_LIST* AddHitListCapsule(int kind, int target, int frame, OBJ_DATA* objData, D3DXVECTOR3 centerPos,
                            float height, float radius, void(*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB));

//扇形柱のあたり判定の追加
//返り値 追加された判定のポインタ
HIT_LIST* AddHitListFanPillar(int kind, int target, int frame, OBJ_DATA* objData, D3DXVECTOR3 centerPos,
                              float height, float radius, float fanAngle, D3DXVECTOR3 direction, void(*hitFunc)(OBJ_DATA* objA, OBJ_DATA* objB));

