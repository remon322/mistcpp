#pragma once

//フォグの情報を取得
int GetFogInfomation(int* fogLevel, float* nearFog, float* farFog);

//フォグの初期化
void InitFog();

//霧の濃さを1段階上げる
void UpFogLevel();

//霧の濃さを1段階下げる
void DownFogLevel();

//霧の濃さを設定する levelは1~5まで
void SetFogLevel(int level);

//霧の更新
void UpdateFog();

//ビルボードによって霧エフェクトを出す
void RenderMist();