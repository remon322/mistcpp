#pragma once
#include <d3dx9.h>
#include "../object.h"

//ミストコンプレッサーの詳細
struct MIST_COMPRESSOR_OPTION
{
  bool  isSaveUp_;    //霧を溜めている状態のときtrue
  int   useTexNum_;   //使う連番の枚数
  float advanceTime_; //アニメーション進行時間
  float accessBlockTime_; //アクセス拒否時間(0以下で許可)
};

//ミストコンプレッサーの生成
OBJ_DATA* GenerateMistCompressor(D3DXVECTOR3 pos, bool isSaveUp);

//ミストコンプレッサーの更新
void UpdateMistCompressor(OBJ_DATA* mistComp);

//ミストコンプレッサーの描画
void RenderMistCompresssor(OBJ_DATA* mistComp);

//ミストコンプレッサーの消去
void DeleteMistCompressor(OBJ_DATA* mistComp);