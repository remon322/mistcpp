#pragma once
#include "../../00_application/function/math.h"

//オブジェクトの種類
//ビットごとにflagにできる
enum OBJ_KIND
{
  OBJ_KIND_PLAYER          = 0x00000001, //プレイヤーキャラクター
  OBJ_KIND_FOG             = 0x00000002, //霧の表現用の筒
  OBJ_KIND_MAP             = 0x00000004, //描画する地形
  OBJ_KIND_TERRAIN         = 0x00000008, //当たり判定用の地形
  OBJ_KIND_SKELETON        = 0x00000020, //スケルトン
  OBJ_KIND_MIST_COMPRESSOR = 0x00000100, //ミストコンプレッサー
  OBJ_KIND_LAUNCHER        = 0x00000200, //発射装置
  OBJ_KIND_ARROW           = 0x00000400, //矢
  OBJ_KIND_SPIDERWEB       = 0x00000800, //蜘蛛の巣
  OBJ_KIND_AUTO_SPEAR      = 0x00001000, //自動で出入りする槍
  OBJ_KIND_TRAP_SPEAR      = 0x00002000, //踏むと作動する槍
  OBJ_KIND_DROP_ITEM       = 0x00010000, //ドロップアイテム
  OBJ_KIND_ALTER           = 0x00020000, //祭壇
  OBJ_KIND_LIGHT_SOURCE    = 0x00040000, //光源
  OBJ_KIND_TREASURE        = 0x00080000, //宝箱
};

//描画の優先順位(数字が小さい順)
enum RENDER_PRIORITY
{
  PRIORITY_FAST,
  PRIORITY_NO_ALPHA,//透明テクスチャなし
  PRIORITY_ALPHA,//透明テクスチャあり

  PRIORITY_MAX,
};

//オブジェクト情報
struct OBJ_DATA
{
  int                 kind_;           //オブジェクトの種類(OBJ_KIND基準)
  int                 renderPriority_; //描画の優先順位
  bool                isShade_;        //陰の有無(trueで陰をつける)
  D3DXVECTOR3         pos_;            //オブジェクトのワールド座標
  D3DXVECTOR3         scale_;          //オブジェクトの大きさ
  float               yawAngle_;       //y軸固定の回転角度
  void*               option_;         //種類ごとの追加情報
  void*               pMeshHandle_;    //メッシュ情報の参照元のアドレス
  ANIMATION_DATA      animationData_;  //アニメーション情報
  struct HIT_LIST*    hitList_;        //当たり判定リスト
  struct HIT_LIST*    headHitList_;    //当たり判定リストの先頭
  struct HIT_LIST*    endHitList_;     //当たり判定リストの最後尾
  OBJ_DATA*           pPrev_;          //リストの前情報
  OBJ_DATA*           pNext_;          //リストの後情報
};

//オブジェクトリストの先頭ポインタ
OBJ_DATA* GetHeadObjData();

//オブジェクトリストの最後尾ポインタ
OBJ_DATA* GetEndObjData();

//引数のアドレスのメッシュデータのメモリを解放し、前後のリストをつなげる
void DeleteListObjData(OBJ_DATA* deletePointer);

//オブジェクトの追加
//追加したオブジェクトデータのポインタを返す
OBJ_DATA* AddObjData(int objKind, D3DXVECTOR3 pos, D3DXVECTOR3 scale, float yawAngle);

//オブジェクトの初期設置
void InitObject();

//オブジェクトの更新
void UpdateObject();

//すべてのオブジェクトの描画
void RenderObject();

//オブジェクトの個数を求める
int GetObjectNum();

//全オブジェクトの開放
void ReleaseObject();

//メッシュハンドルの読み込み直し
void ReloadMeshHandle();