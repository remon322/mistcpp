#pragma once
#include "../object.h"

//光源の種類列挙
enum LIGHT_ID
{
  COLOR_ORANGE,          //オレンジ

  COLOR_MAX,
};

//光源の詳細情報
struct LIGHT_SOURCE_OPTION
{
  int   lightID;       //光のID
  float lightDistance; //光の強さ
};


// 光源の設置
OBJ_DATA* GenerateLightSource(D3DXVECTOR3 pos, int lightID);

// 光源の更新
void UpdateLightSource(OBJ_DATA* light);

// 光源の描画
void RenderLightSource(OBJ_DATA* light);

// 光源の消去
void DeleteLightSource(OBJ_DATA* light);