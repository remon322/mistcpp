
#include "lightSource.h"
#include "../object.h"
#include "../player/player.h"
#include "../../hitJudge/hitJudge.h"
#include "../../camera/camera.h"
#include "../../../00_application/lib/myDx9Lib.h"
#include "../../../00_application/app/app.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/texture/loadTexture.h"
#include "../../../01_data/status/loadStatus.h"

#define DEFFAULT_LIGHT_DISTANCE (1000);

//光源の色テーブル
static float g_lightColorTable[COLOR_MAX][4]
{
  { 1.0f, 0.588f, 0.0f, 1.0f},
};

/// <summary>
/// 光源の設置
/// </summary>
/// <param name="pos_">光源を設置する座標</param>
/// <param name="lightID">光源のカラーID</param>
/// <returns>生成されたオブジェクトポインタ</returns>
OBJ_DATA* GenerateLightSource(D3DXVECTOR3 pos, int lightID)
{
  OBJ_DATA* light = NULL;
  LIGHT_SOURCE_OPTION* lightOp = NULL;

  //アイテムオブジェクトのメモリの確保
  light = AddObjData(OBJ_KIND_DROP_ITEM, pos, g_meshScaleTable[DROP_ITEMS], 0.0f);

  //確保できているか確認
  if(!light) { return NULL; }
  if(!light->option_) { DeleteListObjData(light); return NULL; }

  //アイテムIDを上書き
  lightOp = (LIGHT_SOURCE_OPTION*)light->option_;
  lightOp->lightID = lightID;
  lightOp->lightDistance = DEFFAULT_LIGHT_DISTANCE;

  return light;
}

/// <summary>
/// 光源の更新
/// </summary>
/// <param name="light">更新する光源オブジェクトポインタ</param>
void UpdateLightSource(OBJ_DATA* light)
{
  if(!light) { return; }
  if(!light->option_) { return; }

  LIGHT_SOURCE_OPTION* lightOp = (LIGHT_SOURCE_OPTION*)light->option_;

  //光源をセットする
  PushLightSource(light->pos_,
                  D3DXVECTOR4(g_lightColorTable[lightOp->lightID][0], g_lightColorTable[lightOp->lightID][1], g_lightColorTable[lightOp->lightID][2], g_lightColorTable[lightOp->lightID][3]),
                  GetStatus()->mistCompLightRange_,
                  GetDistance(light->pos_, GetCamera().vEyePt_));
}

/// <summary>
/// 光源の描画
/// </summary>
/// <param name="item">描画する光源オブジェクトポインタ</param>
void RenderLightSource(OBJ_DATA* light)
{
  if(light->kind_ == OBJ_KIND_LIGHT_SOURCE)
  {
    RenderBillboard(light->pos_, GetCamera().vEyePt_, g_textureHandles[LIGHT_SOURCE], 255);
  }
}

/// <summary>
/// 光源の消去
/// </summary>
/// <param name="light">消去したい光源オブジェクトポインタ</param>
void DeleteLightSource(OBJ_DATA* light)
{
  DeleteListObjData(light);
}