#include "../object.h"
#include "../player/player.h"
#include "../../hitJudge/hitJudge.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/status/loadStatus.h"

//蜘蛛の巣にプレイヤーが当たったときの処理
void PlayerMoveSlow(OBJ_DATA* spiderweb, OBJ_DATA* target);

/// <summary>
/// 蜘蛛の巣の生成
/// </summary>
/// <param name="pos_">蜘蛛の巣の生成座標</param>
/// <returns>追加されたオブジェクトアドレス</returns>
OBJ_DATA* GenerateSpiderweb(D3DXVECTOR3 pos)
{
  //メモリの確保
  OBJ_DATA* spiderweb = AddObjData(OBJ_KIND_SPIDERWEB, pos, g_meshScaleTable[SPIDERWEB], 0);
  spiderweb->option_ = NULL;
  //あたり判定の設定
  AddHitListSphere(BODY, 0, INFINITY_FRAME, spiderweb, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 3.0f, NULL);
  AddHitListBox(ATTACK, OBJ_KIND_PLAYER, INFINITY_FRAME, spiderweb, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 100.0f, 20.0f, 100.0f, PlayerMoveSlow);

  return spiderweb;
}

/// <summary>
/// 蜘蛛の巣の更新
/// </summary>
/// <param name="skeleton">更新する蜘蛛の巣オブジェクト</param>
void UpdateSpiderweb(OBJ_DATA* skeleton)
{
  ;
}

/// <summary>
/// 蜘蛛の巣にプレイヤーが当たったときの処理
/// </summary>
/// <param name="spiderweb">蜘蛛の巣オブジェクト(ダミー)</param>
/// <param name="player">速度を遅くするプレイヤーオブジェクト</param>
void PlayerMoveSlow(OBJ_DATA* spiderweb, OBJ_DATA* player)
{
  PLAYER_OPTION* playerOp = (PLAYER_OPTION*)player->option_;
  //鈍足状態異常にする
  playerOp->isSlow_ = true;
}