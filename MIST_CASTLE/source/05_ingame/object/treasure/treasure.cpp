
#include "treasure.h"
#include "../object.h"
#include "../player/player.h"
#include "../../hitJudge/hitJudge.h"
#include "../../camera/camera.h"
#include "../../fog/fog.h"
#include "../../../00_application/lib/myDx9Lib.h"
#include "../../../00_application/app/app.h"
#include "../../../01_data/sound/dslib.h"
#include "../../../01_data/sound/loadSound.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/texture/loadTexture.h"
#include "../../../01_data/status/loadStatus.h"

#define TREASURE_EFFECT_NUM   (2)
#define TREASURE_EFFECT_SPEED (1000 / 300)

// 宝箱の取得
void UseTreasure(OBJ_DATA* treasure, OBJ_DATA* player);

/// <summary>
/// 宝箱の個数の取得(全オブジェクト参照しカウント)
/// </summary>
/// <returns>宝箱の個数</returns>
int GetTreasureNum()
{
  int cnt = 0;
  //すべてのオブジェクトを参照
  OBJ_DATA* now = GetHeadObjData();
  while(now != NULL)
  {
    //宝箱以外であれば次へ
    if(now->kind_ != OBJ_KIND_TREASURE)
    {
      now = now->pNext_;
      continue;
    }
    //宝箱はカウントして次へ
    cnt++;
    now = now->pNext_;
  }
  return cnt;
}

/// <summary>
/// 範囲内で最も近い宝箱のデータの取得
/// </summary>
/// <param name="centerPos"></param>
/// <param name="range">取得する宝箱の範囲</param>
/// <returns>範囲内で最も近い宝箱のデータ</returns>
OBJ_DATA* GetNearTreasure(D3DXVECTOR3 centerPos,float range)
{
  OBJ_DATA* result = NULL;
  //すべてのオブジェクトを参照
  OBJ_DATA* now = GetHeadObjData();
  while(now != NULL)
  {
    //宝箱以外であれば次へ
    if(now->kind_ != OBJ_KIND_TREASURE)
    {
      now = now->pNext_;
      continue;
    }

    //宝箱は比較する
    if(GetDistance(now->pos_, centerPos) < range)
    {
      if(result == NULL)
      {
        result = now;
      }
      else
      {
        if(GetDistance(now->pos_, centerPos) < GetDistance(result->pos_, centerPos))
          result = now;
      }//if(result == NULL)
    }//if(GetDistance(now->pos_, centerPos) < range)

    //次のオブジェクトへ
    now = now->pNext_;
  }//while
  //最終結果を返す
  return result;
}

/// <summary>
/// 宝箱の生成
/// </summary>
/// <param name="pos_">宝箱を設置する座標</param>
/// <returns>生成されたオブジェクトポインタ</returns>
OBJ_DATA* GenerateTreasure(D3DXVECTOR3 pos)
{
  OBJ_DATA* treasure = NULL;

  //アイテムオブジェクトのメモリの確保
  treasure = AddObjData(OBJ_KIND_TREASURE, pos, g_meshScaleTable[TREASURE], 0.0f);

  //確保できているか確認
  if(!treasure) { return NULL; }

  //あたり判定の追加
  AddHitListSphere(ACCESS, OBJ_KIND_PLAYER, INFINITY_FRAME, treasure, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 50.0f, UseTreasure);

  return treasure;
}

/// <summary>
/// 宝箱のエフェクトの描画
/// </summary>
/// <param name="treasure">宝箱オブジェクトポインタ</param>
void RenderTreasure(OBJ_DATA* treasure)
{
  //霧の濃度取得
  float farFog = 0;
  GetFogInfomation(NULL, NULL, &farFog);
  //霧の最大濃さより遠くにあればはじく
  if(GetDistance(treasure->pos_, GetCamera().vEyePt_) > farFog)
    return;

  //宝箱エフェクトの描画
  int effectNum = timeGetTime() / TREASURE_EFFECT_SPEED % TREASURE_EFFECT_NUM;
  RenderBlockBillboardShiftRot(treasure->pos_, D3DXVECTOR3(0, 100, 50), GetCamera().vEyePt_, 0.0f, g_textureHandles[TREASURE_EFFECT], 255, effectNum * 2 + 1);//奥
  RenderBlockBillboardShiftRot(treasure->pos_, D3DXVECTOR3(0, 100, -50), GetCamera().vEyePt_, 0.0f, g_textureHandles[TREASURE_EFFECT], 255, effectNum * 2);   //手前
}

/// <summary>
/// 宝箱の消去
/// </summary>
/// <param name="treasure">消去したい宝箱オブジェクトポインタ</param>
void DeleteTreasure(OBJ_DATA* treasure)
{
  DeleteListObjData(treasure);
}

/// <summary>
/// 宝箱の取得
/// </summary>
/// <param name="treasure">回収された宝箱オブジェクトポインタ</param>
/// <param name="player">回収したプレイヤーオブジェクトポインタ</param>
void UseTreasure(OBJ_DATA* treasure, OBJ_DATA* player)
{
  //情報が不正であればはじく
  if(treasure == NULL || player == NULL) { return; }

  PLAYER_OPTION* playerOp = (PLAYER_OPTION*)player->option_;

  //宝箱の取得数を増やす
  playerOp->treasureGetNum_++;

  //宝箱取得音を鳴らす
  DSoundStop(g_soundHandles[TREASURE_GET_SE]);
  DSoundPlay(g_soundHandles[TREASURE_GET_SE], false);

  //宝箱を削除する
  DeleteListObjData(treasure);
}