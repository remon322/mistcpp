#pragma once
#include "../object.h"

//アイテムの詳細情報
struct TREASURE_OPTION
{

};

// 宝箱の個数の取得(全オブジェクト参照しカウント)
int GetTreasureNum();

// 範囲内で最も近い宝箱のデータの取得
OBJ_DATA* GetNearTreasure(D3DXVECTOR3 centerPos, float range);

// 宝箱の生成
OBJ_DATA* GenerateTreasure(D3DXVECTOR3 pos);

// 宝箱のエフェクトの描画
void RenderTreasure(OBJ_DATA* treasure);

// 宝箱の消去
void DeleteTreasure(OBJ_DATA* treasure);

// 宝箱の取得
void UseTreasure(OBJ_DATA* treasure, OBJ_DATA* player);

// 宝箱の消去
void DeleteTreasure(OBJ_DATA* treasure);