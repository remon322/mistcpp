#pragma once

//祭壇の詳細
struct ALTER_OPTION
{
  bool        isMove_;          //移動後true
  int         moveAdvanceTime_; //移動の進行時間
  D3DXVECTOR3 startPos_;        //移動前の座標
};

//ミストコンプレッサーの生成
OBJ_DATA* GenerateAlter(D3DXVECTOR3 pos, float yawAngle);

//ミストコンプレッサーの更新
void UpdateAlter(OBJ_DATA* mistComp);

//ミストコンプレッサーの消去
void DeleteAlter(OBJ_DATA* skeleton);