#pragma once
#include <d3dx9.h>
#include "../object.h"

//スケルトンの状態識別子
enum E_SKELETON_STATE
{
  SKELETON_STATE_IDLE,      //待機
  SKELETON_STATE_IDLE_PREPARE,//待機の準備
  SKELETON_STATE_APPROACH,  //近づく
  SKELETON_STATE_ATTACK,    //攻撃
  SKELETON_STATE_DEATH,     //死亡
  SKELETON_STATE_NO_MOVE,   //棒立ち
};

//スケルトンの種類
enum E_SKELETON_KIND
{
  SKELETON_KIND_NORMAL,
  SKELETON_KIND_HALF,
  SKELETON_KIND_ARMOR,
};

//敵の追加情報
struct SKELETON_OPTION
{
  E_SKELETON_STATE prevState_;       //前の状態
  E_SKELETON_STATE nowState_;        //現在の状態
  E_SKELETON_KIND  skeKind_;         //スケルトンの種類
  D3DXVECTOR3      startPos_;        //開始時の位置
  float            nextYawAngle_;    //次に向くべき方向
  float            searchRadius_;    //プレイヤーを探すときの歩き回る範囲
  float            moveAdvanceTime_; //行動の進行時間
  int              health_;          //体力
  int              attack_;          //攻撃力
  bool             attackFlag_;      //攻撃中かのフラグ
};

// スケルトンのステータス初期化
void InitSkeletonStatus();

// スケルトンの当たり判定と霧濃度の関連付け※全オブジェクトを参照する
void SkeletonLinkFogLevel();

//スケルトンの生成
OBJ_DATA* GenerateSkeleton(D3DXVECTOR3 pos, int kind);

//スケルトンの更新
void UpdateSkeleton(OBJ_DATA* skeleton);

//スケルトンの消去
void DeleteSkeleton(OBJ_DATA* skeleton);