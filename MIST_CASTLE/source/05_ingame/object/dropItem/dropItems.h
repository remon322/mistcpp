#pragma once
#include "../object.h"

//アイテムの種類列挙
enum ITEM_ID
{
  ITEM_STAMINA_POTION,    //SP回復ポーション
  ITEM_HEAL_POTION,       //HP回復ポーション
};

//アイテムの詳細情報
struct ITEM_OPTION
{
  int   itemID;     //アイテムの種類
  int   useTexNum;  //使う連番の枚数
  float advanceTime;//アイテムの進行時間(アニメーション用)
};

//アイテムの生成
OBJ_DATA* GenerateDropItems(D3DXVECTOR3 pos, int itemID);

//アイテムの更新
void UpdateDropItems(OBJ_DATA* item);

//ドロップアイテムの描画
void RenderDropItems(OBJ_DATA* item);

//アイテムの消去
void DeleteDropItems(OBJ_DATA* item);