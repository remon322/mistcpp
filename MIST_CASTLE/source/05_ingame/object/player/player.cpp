
#include "player.h"
#include "../object.h"
#include "../skeleton/skeleton.h"
#include "../mistCompressor/mistCompressor.h"
#include "../altar/altar.h"
#include "../../fog/fog.h"
#include "../../camera/camera.h"
#include "../../hitJudge/hitJudge.h"
#include "../../../01_data/sound/dslib.h"
#include "../../../00_application/function/math.h"
#include "../../../00_application/app/app.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/sound/loadSound.h"
#include "../../../01_data/status/loadStatus.h"
#include "../../../02_input/input.h"

#define ERROR_DIRECTION           (-9999.0f) //向きのエラー番号

#define KNOCK_BACK_DISTANCE       (30.0f)    //ノックバック距離
#define KNOCK_BACK_TIME_RATE      (GetStatus()->playerInvincibleTime_ * 3.0 / 30)    //ノックバック距離
#define DEATH_TIME                (150.0f)   //死亡時の時間
#define DEATH_ANIMATION_TIME      (115.0f)   //死亡時のアニメーション時間

#define STAMINA_REGEN_CT          (60.0f)    //スタミナの回復のクールタイム
#define PLAYER_LIGHT_RANGE        (500.0f)   //プレイヤーから放たれる光の距離

//ダッシュ中かのフラグ
bool dashFlag = false;

//プレイヤーがジャンプする処理
void PlayerJump(PLAYER_OPTION* playerOption);
//プレイヤーの移動 
//戻り値:移動時true 不動時false
bool PlayerMove(OBJ_DATA* pPlayer);
//他オブジェクトにアクセスする処理
void PlayerAccessObject(OBJ_DATA* player);
//プレイヤーのアニメーション切り替え
void PlayerAnimationChange(OBJ_DATA* pPlayer);
//プレイヤーのノックバック処理
void KnockBackPlayer(OBJ_DATA* player);
//ミストコンプレッサーへのアクセス
void AccessMistCompressor(OBJ_DATA* player, OBJ_DATA* target);
//祭壇へのアクセス
void AccessAlter(OBJ_DATA* player, OBJ_DATA* target);

/// <summary>
/// プレイヤーのデータの初期化(オプションはメモリ確保済みとする)
/// </summary>
/// <param name="player">初期化したいプレイヤーオブジェクト</param>
void InitPlayer(OBJ_DATA* player)
{
  if(!player) { return; }
  if(!player->option_) { return; }
  PLAYER_OPTION* playerOp = (PLAYER_OPTION*)player->option_;

  //あたり判定の設定
  AddHitListSphere(BODY, 0, INFINITY_FRAME, player, D3DXVECTOR3(0.0f,0.0f,0.0f), 25.0f, NULL);
  AddHitListSphere(BODY, 0, INFINITY_FRAME, player, D3DXVECTOR3(0.0f,37.5f,0.0f), 25.0f, NULL);
  AddHitListSphere(BODY, 0, INFINITY_FRAME, player, D3DXVECTOR3(0.0f,70.0f,0.0f), 25.0f, NULL);
  AddHitListSphere(BODY, 0, INFINITY_FRAME, player, D3DXVECTOR3(0.0f,112.5f,0.0f), 25.0f, NULL);
  AddHitListSphere(BODY, 0, INFINITY_FRAME, player, D3DXVECTOR3(0.0f,140.0f,0.0f), 25.0f, NULL);
  //ステータス初期化
  playerOp->healthMax_    = 100;
  playerOp->health_       = 100;
  playerOp->magicPoint_   = 3;
  playerOp->staminaMax_   = 100;
  playerOp->stamina_      = 100;
  playerOp->healthEnergy_ = 0;
  //初期モーションの設定
  ChangeFbxAnimation(&player->animationData_, g_animNameTable[ANIM_PLAYER_IDLE], player->pMeshHandle_);
}

/// <summary>
/// プレイヤーの更新
/// </summary>
/// <param name="pPlayer">更新したいプレイヤーオブジェクト</param>
void UpdatePlayer(OBJ_DATA* pPlayer)
{
  PLAYER_OPTION* pPlayerOp = (PLAYER_OPTION*)pPlayer->option_;
  int gameMode = GetGameMode();

  //時間の更新
  pPlayerOp->invincibleTime_ -= GetSpeedRate();              //無敵時間
  pPlayerOp->staminaInfinityTime_ -= GetSpeedRate();         //スタミナ減少無効時間
  pPlayerOp->staminaRegenerationCoolTime_ -= GetSpeedRate(); //スタミナリジェネーションのクールダウン時間

  //アニメーションの更新
  UpdateAnimation(&pPlayer->animationData_);
  switch(pPlayerOp->state_)
  {
  case PL_STATE_MOVE:
    if(dashFlag)
      UpdateFbxAnimation(&pPlayer->animationData_, pPlayer->pMeshHandle_, 1.5f);
    else
      UpdateFbxAnimation(&pPlayer->animationData_, pPlayer->pMeshHandle_, 1.0f);
    break;
  case PL_STATE_KNOCK_BACK:
    UpdateFbxAnimation(&pPlayer->animationData_, pPlayer->pMeshHandle_, KNOCK_BACK_TIME_RATE);
    break;
  case PL_STATE_DEATH:
    //特定の時間内だけ更新する
    if(pPlayerOp->deathAdvanceTime_ < DEATH_ANIMATION_TIME)
      UpdateFbxAnimation(&pPlayer->animationData_, pPlayer->pMeshHandle_, 1.0f);
    break;
  default:
    UpdateFbxAnimation(&pPlayer->animationData_, pPlayer->pMeshHandle_, 1.0f);
    break;
  }
  
  //一部ゲームモードで死なないようにする
  switch(gameMode)
  {
    case GAMEMODE_HEALTH_INFINITY:
    case GAMEMODE_DEBUG:
    case GAMEMODE_DEBUG_HIT_JUDGE:
    case GAMEMODE_MAP_CREATE:
      pPlayerOp->health_ = CLUMP(0, pPlayerOp->health_ + 1, pPlayerOp->healthMax_);
      if(pPlayerOp->health_ <= 0)
        pPlayerOp->health_ = 1;
      break;
    case GAMEMODE_NORMAL:
      break;
  }

  //前フレームの状態を記録
  pPlayerOp->prevState_ = pPlayerOp->state_;

  //死亡時状態切り替え
  if (pPlayerOp->health_ <= 0)
  {
    if(pPlayerOp->state_ != PL_STATE_DEATH)
    {
      //プレイヤーを死亡状態へ
      pPlayerOp->state_ = PL_STATE_DEATH;
      pPlayerOp->deathAdvanceTime_ = 0;
      //死亡時SEを鳴らす
      DSoundPlay(g_soundHandles[PLAYER_DAMAGE_SE], false);
      //歩行音を止める
      DSoundStop(g_soundHandles[PLAYER_WALK_SE]);
    }
    pPlayerOp->deathAdvanceTime_ += GetSpeedRate();
    //死亡から一定時間経過でゲームオーバーへ移行
    if(pPlayerOp->deathAdvanceTime_ >= DEATH_TIME)
      ChangeGameState(GAME_STATE_GAMEOVER);
  }

  //重力とジャンプの処理
  if(gameMode == GAMEMODE_NORMAL || gameMode == GAMEMODE_HEALTH_INFINITY)
  {
    pPlayerOp->gravityVecY_ -= GRAVITY_ACCELE;
    if(pPlayerOp->isGroundHit_) { PlayerJump(pPlayerOp); }
    if(Landing(pPlayer, pPlayerOp->gravityVecY_))
    {
      pPlayerOp->gravityVecY_ = -0.3f;
      pPlayerOp->isGroundHit_ = true;
    }
    else
    {
      pPlayerOp->isGroundHit_ = false;
      if(pPlayerOp->state_ != PL_STATE_DEATH)
        pPlayerOp->state_ = PL_STATE_IDLE;
    }
  }
  else
  {
    pPlayerOp->gravityVecY_ = 0.0f;
    //デバッグ用操作
    KEY_BOARD key = GetKeyBoard();
    //上下移動
    if(key.lShift)
      Landing(pPlayer, -15.0f);
    if(key.space)
      MoveObject(pPlayer, D3DXVECTOR3(0.0f, 15.0f, 0.0f));
    //プレイヤー位置のリセット
    if(key.f12)
      pPlayer->pos_ = D3DXVECTOR3(0.0f, 10.0f, 0.0f);
  }

  //プレイヤーが奈落に落ち無いようにする
  if(pPlayer->pos_.y <= 0.0f)
  {
    pPlayer->pos_ = D3DXVECTOR3(pPlayer->pos_.x, 0.0f, pPlayer->pos_.z);
    pPlayerOp->isGroundHit_ = true;
  }

  if(pPlayerOp->state_ != PL_STATE_KNOCK_BACK && pPlayerOp->state_ != PL_STATE_DEATH)
  {
    //プレイヤーの移動
    if(PlayerMove(pPlayer))
    {
      //オブジェクトの押し出し
      PushObjectCharacter(pPlayer);
      //地面と接地していれば移動状態にする
      if(pPlayerOp->isGroundHit_)
        pPlayerOp->state_ = PL_STATE_MOVE;
    }
    else
    {
      pPlayerOp->state_ = PL_STATE_IDLE;
    }
  }

  KnockBackPlayer(pPlayer);
  PlayerAccessObject(pPlayer);
  PlayerAnimationChange(pPlayer);

  //前フレームより体力が減っているときダメージボイスを鳴らす
  if(pPlayerOp->prevHealth_ > pPlayerOp->health_)
  {
    if(pPlayerOp->health_ <= 0)
    {
      DSoundPlay(g_soundHandles[PLAYER_DEATH_SE], false); //死亡ボイス
    }
    else
    {
      DSoundPlay(g_soundHandles[PLAYER_DAMAGE_SE], false); //ダメージボイス
    }
  }

  //helthの情報保存
  pPlayerOp->prevHealth_ = pPlayerOp->health_;
  //stamina_の回復
  if(pPlayerOp->staminaRegenerationCoolTime_ < 0)
    pPlayerOp->stamina_ = CLUMP(0, pPlayerOp->stamina_ + 0.25f * GetSpeedRate(), pPlayerOp->staminaMax_);
  //鈍足状態異常の回復
  pPlayerOp->isSlow_ = false;

  //光源をセットする
  //PushLightSource(D3DXVECTOR3(pPlayer->pos_.x, pPlayer->pos_.y + 100, pPlayer->pos_.z), D3DXVECTOR4(1, 1, 0.9f, 1), PLAYER_LIGHT_RANGE, 1);
}

/// <summary>
/// プレイヤーの情報の取得
/// </summary>
/// <returns>プレイヤーの情報</returns>
OBJ_DATA GetPlayerData()
{
  OBJ_DATA* nowPointer = GetHeadObjData();
  while(nowPointer)
  {
    if(nowPointer->kind_ == OBJ_KIND_PLAYER)
      return *nowPointer;

    nowPointer = nowPointer->pNext_;
  }
  OBJ_DATA result;
  memset(&result, 0, sizeof(OBJ_DATA));
  return result;
}

/// <summary>
/// プレイヤーのデータアドレスの取得
/// </summary>
/// <returns>プレイヤーのアドレス</returns>
OBJ_DATA* GetPlayerAddress()
{
  OBJ_DATA* nowPointer = GetHeadObjData();
  while(nowPointer)
  {
    if(nowPointer->kind_ == OBJ_KIND_PLAYER)
      return nowPointer;

    nowPointer = nowPointer->pNext_;
  }
  return NULL;
}

/// <summary>
/// プレイヤーがジャンプする処理
/// </summary>
/// <param name="playerOption">プレイヤーのオプションアドレス</param>
void PlayerJump(PLAYER_OPTION* playerOption)
{
  //ジャンプ機能廃止
  //KEY_BOARD key = GetKeyBoard();
  //JOY_STICK joy = GetJoyStickButton();
  //
  //float backFront = 0;//back成分が- front成分が+
  //float leftRight = 0;//left成分が- right成分が+
  //if(key.space || joy.button[0])
  //{
  //  DSoundPlay(g_soundHandles[JUMP_SE], FALSE);
  //  playerOption->gravityVecY_ = (GetStatus()->playerJumpHeight_ / GetStatus()->playerJumpTime_) + (GRAVITY_ACCELE * GetStatus()->playerJumpTime_ / 2.0f);
  //  playerOption->isGroundHit_ = false;
  //}
}

/// <summary>
/// プレイヤーの移動向きと移動量を決める
/// </summary>
/// <param name="stamina_">現在のスタミナ量</param>
/// <param name="direction">(out)移動向き</param>
/// <param name="velocity_">(out)移動速度</param>
void DecideMoveDirection(float stamina, float* direction, float* velocity)
{
  CAMERA cam = GetCamera();
  
  if(direction == NULL || velocity == NULL)
  {
    return;
  }
  KEY_BOARD key = GetKeyBoard();
  JOY_STICK joy = GetJoyStickButton();

  float backFront = 0;//back成分が- front成分が+
  float leftRight = 0;//left成分が- right成分が+
  dashFlag = false;

  //前後左右の成分をキー入力から決定する
  if(key.w || joy.leftStickY)
  {
    backFront = max(key.w, joy.leftStickY);
  }
  if(key.s || joy.leftStickY)
  {
    backFront += min(-key.s, joy.leftStickY);
  }
  if(key.d || joy.leftStickX)
  {
    leftRight = max(key.d, joy.leftStickX);
  }
  if(key.a || joy.leftStickX)
  {
    leftRight += min(-key.a, joy.leftStickX);
  }
  if (key.lShift || joy.button[0] || joy.z < -0.3)
  {
    dashFlag = true;
    if(stamina <= 0)
      dashFlag = false;
  }

  if(backFront == 0 && leftRight == 0)
  {
    *velocity = 0.0f;
    *direction = ERROR_DIRECTION;
    return;
  }

  *velocity = CLUMP(0, GetScalar2D(backFront, leftRight) * GetStatus()->playerVelocity_, GetStatus()->playerVelocity_);
  if(dashFlag) { *velocity *= GetStatus()->playerDashRate_; }
  *direction = atan2f(backFront, leftRight) + cam.phi_;
}

/// <summary>
/// プレイヤーの振り向き処理
/// </summary>
/// <param name="nowAngle">現在の向き</param>
/// <param name="nextAngle">振り向き後の目標向き</param>
/// <returns>新しいプレイヤーの向き</returns>
float TurningPlayer(float nowAngle, float nextAngle)
{
  float result = nowAngle;
  float leftOrRight = sinf(nextAngle - nowAngle);//今の向きを基準として次の向きが右か左かを求める　左 0~PI 右0~-PI
  if(nowAngle == nextAngle)
  {
    return result;
  }
  if(leftOrRight > 0)
  {
    //左振り向き
    result += GetStatus()->playerTurnSpeed_;
    //振り向きが目標向きを超えた時結果を目標と一致させる
    if(sinf(nextAngle - result) <= 0.0f)
      result = nextAngle;
  }
  else
  {
    //右振り向き
    result -= GetStatus()->playerTurnSpeed_;
    //振り向きが目標向きを超えた時結果を目標と一致させる
    if(sinf(nextAngle - result) >= 0.0f)
      result = nextAngle;
  }
  return result;
}

/// <summary>
/// プレイヤーの移動 
/// </summary>
/// <param name="pPlayer">移動させたいプレイヤーオブジェクト</param>
/// <returns>移動時true</returns>
bool PlayerMove(OBJ_DATA* pPlayer)
{
  PLAYER_OPTION* pPlayerOp = (PLAYER_OPTION*)pPlayer->option_;
  CAMERA camera = GetCamera();
  //移動したい向きの決定
  float tmpAngle;
  DecideMoveDirection(pPlayerOp->stamina_, &tmpAngle, &pPlayerOp->velocity_);
  if (tmpAngle != ERROR_DIRECTION)
    pPlayerOp->nextYawAngle_ = tmpAngle - PI / 2.0f;

  if (pPlayerOp->velocity_)
  {
    //移動時
    pPlayer->yawAngle_ = TurningPlayer(pPlayer->yawAngle_, pPlayerOp->nextYawAngle_);
    float vecX;
    float vecZ;
    //移動ベクトルを決める
    if(pPlayerOp->isSlow_ == true)
    {
      //鈍足時
      vecX = pPlayerOp->velocity_ * cosf(tmpAngle) * GetSpeedRate() * GetStatus()->spiderwebSlowRate_;
      vecZ = pPlayerOp->velocity_ * sinf(tmpAngle) * GetSpeedRate() * GetStatus()->spiderwebSlowRate_;
    }
    else
    {
      //非鈍足時
      vecX = pPlayerOp->velocity_ * cosf(tmpAngle) * GetSpeedRate();
      vecZ = pPlayerOp->velocity_ * sinf(tmpAngle) * GetSpeedRate();
    }
    MoveObject(pPlayer, D3DXVECTOR3(vecX, 0, vecZ));
    if(dashFlag == true)
    {
      if(pPlayerOp->staminaInfinityTime_ <= 0)
      {
        pPlayerOp->stamina_ -= GetStatus()->playerSpLost_ * GetSpeedRate();
      }
      pPlayerOp->staminaRegenerationCoolTime_ = STAMINA_REGEN_CT;
      //走る音を鳴らす
      DSoundStop(g_soundHandles[PLAYER_WALK_SE]);
      DSoundPlay(g_soundHandles[PLAYER_RUN_SE], true);
    }
    else
    {
      //歩く音を鳴らす
      DSoundStop(g_soundHandles[PLAYER_RUN_SE]);
      DSoundPlay(g_soundHandles[PLAYER_WALK_SE], true);
    }
    return true;
  }
  //歩き&走り の音をとめる
  DSoundStop(g_soundHandles[PLAYER_RUN_SE]);
  DSoundStop(g_soundHandles[PLAYER_WALK_SE]);
  return false;
}

//他オブジェクトにアクセスする処理
void PlayerAccessObject(OBJ_DATA* player)
{
  KEY_BOARD key = GetTrgInKeyBoard();
  JOY_STICK joy = GetTrgInJoyStickButton();
  MOUSE   mouse = GetTrgInMouseButton();

  //アクセスボタンのチェック
  if(key.e || mouse.button[1] || joy.button[1])
  {
    D3DXVECTOR3 hitCenterPos = D3DXVECTOR3(cos(player->yawAngle_ + PI / 2.0f), 1.0f, sin(player->yawAngle_ + PI / 2.0f));
    AddHitListCapsule(ACCESS, OBJ_KIND_MIST_COMPRESSOR, 5, player, hitCenterPos, 20.0f, 200.0f, AccessMistCompressor);
    AddHitListCapsule(ACCESS, OBJ_KIND_ALTER, 5, player, hitCenterPos, 20.0f, 50.0f, AccessAlter);
  }
}

//プレイヤーのアニメーション切り替え
void PlayerAnimationChange(OBJ_DATA* pPlayer)
{
  if (!pPlayer) { return; }
  if (!pPlayer->option_) { return; }
  PLAYER_OPTION* playerOp = (PLAYER_OPTION*)pPlayer->option_;
  if (playerOp->state_ == playerOp->prevState_)
  {
    return;
  }
  switch (playerOp->state_)
  {
  case PL_STATE_IDLE:
    ChangeFbxAnimation(&pPlayer->animationData_, g_animNameTable[ANIM_PLAYER_IDLE], pPlayer->pMeshHandle_);
    break;
  case PL_STATE_MOVE:
    ChangeFbxAnimation(&pPlayer->animationData_, g_animNameTable[ANIM_PLAYER_MOVE], pPlayer->pMeshHandle_);
    break;
  case PL_STATE_KNOCK_BACK:
    ChangeFbxAnimation(&pPlayer->animationData_, g_animNameTable[ANIM_PLAYER_KNOCKBACK], pPlayer->pMeshHandle_);
    break;
  case PL_STATE_DEATH:
    ChangeFbxAnimation(&pPlayer->animationData_, g_animNameTable[ANIM_PLAYER_DEAD], pPlayer->pMeshHandle_);
    break;
  }
}

/// <summary>
/// プレイヤーのノックバック処理
/// </summary>
/// <param name="player">プレイヤーオブジェクト</param>
void KnockBackPlayer(OBJ_DATA* player)
{
  if(!player) { return; }
  if(!player->option_) { return; }
  PLAYER_OPTION* playerOp = (PLAYER_OPTION*)player->option_;

  //死亡状態だった場合ノックバックしない
  if(playerOp->state_ == PL_STATE_DEATH)
    return;

  //ノックバックの有効時間だった場合ノックバックさせる
  if(playerOp->knockBackAdvanceTime_ < KNOCK_BACK_MAX_TIME && playerOp->knockBackAdvanceTime_ > 0)
  {
    MoveObject(player, playerOp->knockBackDir_ * KNOCK_BACK_DISTANCE / KNOCK_BACK_MAX_TIME);
    //playerOp->prevState_ = playerOp->state_;
    playerOp->state_ = PL_STATE_KNOCK_BACK;
  }
  else if(playerOp->state_ == PL_STATE_KNOCK_BACK)
  {
    playerOp->state_ = PL_STATE_IDLE;
  }
  playerOp->knockBackAdvanceTime_ -= GetSpeedRate();
}

/// <summary>
/// ミストコンプレッサーへのアクセス
/// </summary>
/// <param name="player">プレイヤーオブジェクト</param>
/// <param name="target">ミストコンプレッサーオブジェクト</param>
void AccessMistCompressor(OBJ_DATA* player, OBJ_DATA* target)
{
  MIST_COMPRESSOR_OPTION* mistCompOp = (MIST_COMPRESSOR_OPTION*)target->option_;

  //アクセス禁止時間なら何もしない
  if(mistCompOp->accessBlockTime_ > 0) { return; }

  //霧の濃度操作
  if(mistCompOp->isSaveUp_ == true)
  {
    //霧の濃度を1段階下げる
    DownFogLevel();
    mistCompOp->isSaveUp_ = false;
    mistCompOp->accessBlockTime_ = 30;
  }
  else
  {
    //霧の濃度を1段階上げる
    UpFogLevel();
    mistCompOp->isSaveUp_ = true;
    mistCompOp->accessBlockTime_ = 30;
  }
}

/// <summary>
/// 祭壇へのアクセス
/// </summary>
/// <param name="player">プレイヤーオブジェクト</param>
/// <param name="target">祭壇オブジェクト</param>
void AccessAlter(OBJ_DATA* player, OBJ_DATA* target)
{
  ALTER_OPTION* alterOp = (ALTER_OPTION*)target->option_;
  if(alterOp->isMove_ == false)
  {
    alterOp->isMove_ = true;
    alterOp->moveAdvanceTime_ = 0;
  }
}