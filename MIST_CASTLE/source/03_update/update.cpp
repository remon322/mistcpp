
#include "../00_application/app/app.h"
#include "../01_data/model/loadModel.h"
#include "../01_data/sound/dslib.h"
#include "../01_data/sound/loadSound.h"
#include "../02_input/input.h"
#include "../03_update/update.h"
#include "../05_ingame/camera/camera.h"
#include "../05_ingame/fog/fog.h"
#include "../05_ingame/object/object.h"
#include "../05_ingame/hitJudge/hitJudge.h"
#include "../05_ingame/stageManager/stageManager.h"
#include "../05_ingame/userInterface/userInterface.h"
#include "../05_ingame/debug/debug.h"
#include "../06_outGame/title/title.h"
#include "../06_outGame/opening/opening.h"
#include "../06_outGame/gameover/gameover.h"
#include "../06_outGame/clear/clear.h"
#include "../06_outGame/staffroll/staffroll.h"

//インゲームの経過時間
int g_inGameTime = 0;

/// <summary>
/// インゲームの初期化
/// </summary>
void InitInGame()
{
  ReleaseIngame();
  g_inGameTime = 0;
  InitCamera();
  InitFog();
  InitObject();
  InitStage();
  DSoundPlay(g_soundHandles[INGAME_BGM], TRUE);
}

/// <summary>
/// インゲームの更新
/// </summary>
void Update()
{
  //fpsの取得
  CheckFPS();

  //ゲームの更新
  switch(GetGameState())
  {
  case GAME_STATE_TITLE:
    UpdateTitle();
    break;
  case GAME_STATE_OPENING:
    UpdateOpening();
    break;
  case GAME_STATE_INGAME:
    StageManagement();
    UpdateObject();
    UpdateCamera();
    UpdateFog();
    HitJudge();
    UpdateUserInterface();
#ifdef DEBUG_MODE
    UpdateDebug();
#endif
    break;
  case GAME_STATE_GAMEOVER:
    UpdateGameover();
    break;
  case GAME_STATE_CLEAR:
    UpdateClear();
    break;
  case GAME_STATE_STAFFROLL:
    UpdateStaffroll();
    break;
  case GAME_STATE_END:
    g_isContinueGame = false;
    break;
  }
  g_inGameTime++;

  //escが押されたらゲーム終了
  if(GetTrgInKeyBoard().esc || GetTrgInJoyStickButton().button[7])
    ChangeGameState(GAME_STATE_END);
}

/// <summary>
/// インゲームの経過時間を取得する
/// </summary>
/// <returns>インゲームの経過時間</returns>
int GetInGameTime()
{
  return g_inGameTime;
}

/// <summary>
/// インゲームの情報をすべて開放する
/// </summary>
void ReleaseIngame()
{
  ReleaseObject();
}