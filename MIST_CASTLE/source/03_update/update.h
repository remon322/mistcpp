#pragma once

//インゲームの初期化
void InitInGame();

//インゲームの更新
void Update();

//インゲームの経過時間を取得する
int GetInGameTime();

//インゲームの情報をすべて開放する
void ReleaseIngame();